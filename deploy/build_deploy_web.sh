#!/bin/bash
docker stop web
docker rm web
docker build -t glibr-web .
docker run -d --network=multi-host-network --network-alias web -p 49500:3000 --name web glibr-web
