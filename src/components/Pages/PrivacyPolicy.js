import React, { Component } from 'react';

//import Main from '../Main';

import './style.css';

export default class PrivacyPolicy extends Component {
  render() {
    return (
      <div className="container">
      <div className="container-green-border text-align-left">
        <h1>Privacy Policy.</h1>

      <p> Last updated: May 1, 2017 </p>


      <p>Protecting your privacy is important to us. With this in mind, we&apos;re providing this Privacy Policy to explain our practices regarding the collection, use and disclosure of information that we receive through our Services. This Privacy Policy does not apply to any third-party websites, services or applications, even if they are accessible through our Services. Also, please note that, unless we define a term in this Privacy Policy, all capitalized terms used in this Privacy Policy have the same meanings as in our Terms of Service. So, please make sure that you have read and understand our Terms of Service.</p>

    <p>Any information that is collected via our Services is covered by the Privacy Policy in effect at the time such information is collected. We may revise this Privacy Policy from time to time. If we make any material changes to this Privacy Policy, we&apos;ll notify you of those changes by posting them on the Services or by sending you an email or other notification, and we&apos;ll update the &quot;Last Updated Date&quot; above to indicate when those changes will become effective.</p>

    <p><b><u>Collection and Use of Information</u></b></p>

    <p><b>Information Collected or Received from You</b></p>

    <p>Our primary goals in collecting information are to provide and improve our Services, to administer your use of the Services (including your Account, if you are an Account holder), and to enable you to enjoy and easily navigate our Services. Account Information. If you create an Account, we will collect certain information that can be used to identify you, such as your email address and, if you opt to add it, a photograph or picture of you (&quot;PII&quot;). Creating an Account also requires a username, which may or may not be PII depending on your username. You have the option to choose to provide other information about yourself, which will be made part of your Account&apos;s profile (&quot;Profile&quot;).</p>

    <p><b>Information Collected Using Technologies.</b></p>

    <p>Like many app operators, we use automated data collection tools to collect certain information. Some third party services providers that we engage may also track and report information about how and when you interact with our App and information about your mobile device (such as device hardware, operating system, and location). We may use that information to customize and improve our Services. Note that this Privacy Policy covers only our use of technologies and does not include use of technologies by third parties.</p>

    <p><b>Information Related to Use of the Services.</b></p>

    <p>Our servers automatically record certain information about how a person uses our Services (we refer to this information as &quot;Log Data&quot;), including both Account holders and non-Account holders (either, a &quot;User&quot;). Log Data may include information such as a User&apos;s Internet Protocol (IP) address, operating system, the features of our Services to which a User browsed and the time spent on those features, search terms, the links on our Services that a User clicked on and other statistics. We use Log Data to administer the Services and we analyze (and may engage third parties to analyze) Log Data to improve, customize and enhance our Services by expanding their features and functionality and tailoring them to our Users&apos; needs and preferences. We may use a person&apos;s IP address to generate aggregate, non-identifying information about how our Services are used.</p>

    <p><b>Information Sent by Your Mobile Device.</b></p>

    <p>We collect certain information that your mobile device sends when you use our Services, like a device identifier, user settings, and the operating system of your device, as well as information about your use of our Services. Location Information. When you use our App and have enabled location services on your mobile device, we may collect and store information about your location by converting your IP address into a rough geolocation or by accessing your mobile device&apos;s GPS coordinates or course location. With your permission, we may use location information to improve, personalize and offer certain features of our Services to you, such as letting you check-in to locations, offering lists of nearby venues and calculating the distances and number of footsteps that you walk in a given day. If you do not want us to collect location information, you may disable that feature on your mobile device.</p>

    <p><b>Information that We Share with Third Parties</b></p>

    <p>We will not share any PII that we have collected from or regarding you except as described below: Information Shared with Other Account Holders. Your User Content and Profile, which includes your username, display name and, if you choose to include it, your photograph or picture, will be visible to other Account holders whom you have approved as friends to view your Profile and User Content (&quot;Friends&quot;). In addition, unless you opt out of the default setting, your User Content and Profile will be visible to other Account holders who were approved by your Friends to view their User Content. In other words, friends of your Friends can see your Profile and User Content unless you change the setting in your App. The email address you provide to create your Account is not shared by us with other Account holders (but may appear in your Friends&apos; address books if you provided it to them).</p>

    <p><b>Information Shared with Our Services Providers.</b></p>

    <p>We may engage third party services providers to work with us to administer and provide the Services. These third party services providers will have access to your PII only for the purpose of performing services on our behalf and will be expressly obligated not to disclose or use your PII for any other purpose.</p>

    <p><b>Information Shared with Third Parties.</b></p>

    <p>We may share aggregated information and non-identifying information with third parties for industry research and analysis, demographic profiling and other similar purposes. Information Disclosed in Connection with Business Transactions. Information that we collect from our users, including PII, is considered to be a business asset. Thus, if we are acquired by a third party as a result of a transaction such as a merger, acquisition or asset sale or if our assets are acquired by a third party in the event we go out of business or enter bankruptcy, some or all of our assets, including your PII, may be disclosed or transferred to a third party acquirer in connection with the transaction. Information Disclosed for Our Protection and the Protection of Others.</p>

    <p>We cooperate with government and law enforcement officials or private parties to enforce and comply with the law. We may disclose any information about you to government or law enforcement officials or private parties as we, in our sole discretion, believe necessary or appropriate: (i) to respond to claims, legal process (including subpoenas); (ii) to protect our property, rights and safety and the property, rights and safety of a third party or the public in general; and (iii) to stop any activity that we consider illegal or legally actionable activity.</p>

    <p><b>Your Choices</b></p>

    <p>We offer you choices regarding the collection, use and sharing of your PII and we&apos;ll respect the choices you make. Please note that if you decide not to provide us with the PII that we request, you may not be able to access all of the features of the Services.</p>

    <p><b>Opt-Out.</b></p>

    <p>We may periodically send you free emails that directly promote our Services. When you receive such promotional communications from us, you will have the opportunity to &quot;opt-out&quot; (either through your Account or by following the unsubscribe instructions provided in the email you receive). We do need to send you certain communications regarding the Services (e.g., account management and updates to our Terms of Service or this Privacy Policy) and you will not be able to opt-out of those communications.</p>

    <p><b>Modifying Your Information.</b></p>

    <p>You can access and modify the information contained in your Profile through the Site. If you want us to delete your PII and your Account, please contact us at hello@glibr.net with your request. We&apos;ll take steps to delete your information as soon we can, but some information may remain in archived/backup copies for our records or as otherwise required by law.</p>

    <p><b>The Security of Your Information</b></p>

    <p>We take reasonable administrative, physical and electronic measures designed to protect the information that we collect from or about you (including your PII) from unauthorized access, use or disclosure. Please be aware, however, that no method of transmitting information over the Internet or storing information is completely secure. Accordingly, we cannot guarantee the absolute security of any information.</p>

    <p><b>Links to Other Sites</b></p>

    <p>Our Services may contain links to websites and services that are owned or operated by third parties (each, a &quot;Third Party Service&quot;). Any information that you provide on or to a Third Party Service or that is collected by a Third Party Service is provided directly to the owner or operator of the Third Party Service and is subject to the owner&apos;s or operator&apos;s privacy policy. We&apos;re not responsible for the content, privacy or security practices and policies of any Third Party Service. To protect your information we recommend that you carefully review the privacy policies of all Third Party Services that you access.</p>

    <p><b>International Transfer</b></p>

    <p>Your PII may be transferred to, and maintained on, computers located outside of your state, province, country or other governmental jurisdiction where the privacy laws may not be as protective as those in your jurisdiction. If you&apos;re located outside the United States and choose to provide your PII to us, we may transfer your PII to the United States and process it there.</p>

    <p><b>Our Policy Toward Children</b></p>

    <p>Our Services are not directed to children under the age of 13 and we do not knowingly collect PII from children under age 13. If we learn that we have collected PII from a child under 13 years of age, we will take steps to delete such information from our files as soon as possible.</p>

    <p><b>Questions?</b></p>

    <p>Please contact us at admin@glibr.net if you have any questions about our Privacy Policy.</p>
      </div>
      </div>
    );
  }
}
