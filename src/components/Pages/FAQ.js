import React, { Component } from 'react';

//import Main from '../Main';

import './style.css';

export default class FAQ extends Component {
  render() {
    return (
      <div className="container">
      <div className="container-green-border text-align-left">

          <h1>FAQ</h1>
          <p> Last updated: March 5th, 2019</p>

        
          <ul>
<li>
	<b>What is glibr?</b><br />
	Glibr is a social network where you can post anything you want as long as it is shorter than 150 characters long.
<br /><br /></li>

<li>
	<b>Yeah but why would I use gilbr instead of the tired old Twitter?</b><br />
	Because glibr doesn’t want to harvest your data and sell your profile to marketers like Twitter and so many other social networks do.
<br /><br /></li>

<li>
<b>So how does glibr make money then?</b><br />
	We charge 2 pennies per glib post.
<br /><br /></li>

<li>
	<b>Wait...what, you charge per post?</b><br />
	Yup.  We’re honest here, not like Twitter or those other social networks, where you trade your personal data for them to use and sell.
<br /><br /></li>

<li>
	<b>So I get billed 2 pennies for every glib I make?</b><br />
	No, we accrue the pennies, and when you reach $3.00 we bill you for that.
<br /><br /></li>

<li>
	<b>How does glibr bill me if it doesn’t collect any of my data?</b><br />
	We use a third party company called STRIPE.  They hold all the credit card information, all we have from them is a customer token that looks like this: cus_EIgHNFno6w5EPm.  That’s it.  They actually bill you, and they pay us.  They of course make a profit off of the whole cycle (yay! Capitalism!) but it’s worth it, because we don’t store any of your data.	
<br /><br /></li>

<li>
	<b>So who is STRIPE?  Why should I trust them then?</b><br />
	Go check them out: <a href="https://stripe.com/" target="_stripe">https://stripe.com/</a> They are really cool.  Professional, big company that will be going out to IPO very soon.  The latest round of investment for them came in at $245 MILLION dollars, and brings the value of the company up to $20 BILLION.  <a href="https://techcrunch.com/2018/09/26/stripe-is-now-valued-at-20b-after-raising-another-245m-led-by-tiger-global/" target="_tcrunch">https://techcrunch.com/2018/09/26/stripe-is-now-valued-at-20b-after-raising-another-245m-led-by-tiger-global/</a>
	<br /><br />
	<i>“She’s fast enough for you old man.” - H. Solo</i>
<br /><br /></li>

<li>
	<b>OK.  So maybe it isn’t so crazy, what’s with this charity thing then?</b><br />
	glibr really wants to encourage goodness.  That’s why glibr is the goodest social network.  Part of what that means is that we’ll be donating 20% of our profits to the charity of your choice.  As you post, we’ll keep track of the pennies and when we settle up for the quarter, we’ll cut 20% of our profit and send it off to the charities.
<br /><br />
	And when we get more mature as a company, we’ll be increasing that percentage.  Promise.
<br /><br /></li>

<li>
	<b>So wait, I just post and part of my posted pennies goes to charity?</b><br />
	Yup.
<br /><br /></li>

<li>
	<b>That’s cool.</b><br />
	Yup.
<br /><br /></li>

<li>
	<b>OK, so are you guys going to be censoring stuff?</b><br />
	Yes we are.
<br /><br /></li>

<li>
	<b>Wait what?  Then you are just as bad as Twitter.</b><br />
	No we’re not.  We’re gonna censor like the FCC.  No dirty words, no nudey pictures.
<br /><br /></li>

<li>
	<b>So I can’t say ****.</b><br />
	<b>Wait!  **** you ************!  What did you do to me?!</b><br />
	You’re on glibr, you can’t say the <a href="https://en.wikipedia.org/wiki/Seven_dirty_words" target="_blank" rel="noopener noreferrer">https://en.wikipedia.org/wiki/Seven_dirty_words</a>
<br /><br /></li>

<li>
	<b>No nudey Pictures either?  Bummer.</b><br />
	Yeah well, your selfies from the beach may get blocked as well…  Sorry.  But so be it.
<br /><br /></li>

<li>
	<b>What about offensive speech?</b><br />
	We are not going to police that.
<br /><br /></li>

<li>
	<b>What about hate speech?</b><br />
	We believe in the poem we all learned at the age of four.  Sticks and stones may break my bones, but words will never harm me.  If you don’t like the words of someone, don’t follow them.
<br /><br /></li>

<li>
	<b>How is that possible?</b><br />
	We expect everyone to act in a good manner.  Everyone should.
<br /><br /></li>

<li>
	<b>You are a bunch of naive fools.</b><br />
	Yup.   But anything else we try to do, descends into madness.
<br /><br /></li>

<li>
	<b>OK then, I’ll give a spin.</b><br />
	Welcome aboard!  Keep glib.
<br /><br /></li>
</ul>


- the management
        

      </div>
    </div>
    );
  }
}
