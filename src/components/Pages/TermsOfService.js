import React, { Component } from 'react';

//import Main from '../Main';

import './style.css';

export default class TermsOfService extends Component {
  render() {
    return (
      <div className="container">
            <div className="container-green-border text-align-left">
                  <h1>Terms of Service.</h1>

                  <p>  Last updated: May 1, 2017</p>
                  <p>Welcome to glibr, the penny powered social network (the Site). Please read these Terms of Service (the "Terms") and our Privacy Policy carefully because they govern your use of our services and Site. To make these Terms easier to read, our services and Site are collectively called the "Services."</p>

                  <p><b>Agreement to Terms</b></p>

                  <p>  By using our Services, you agree to be bound by these Terms and by our Privacy Policy. If you don&apos;t agree to these Terms and our Privacy Policy, do not use the Services. If you are accessing and using the Services on behalf of a company (such as your employer) or other legal entity, you represent and warrant that you have the authority to bind that company or other legal entity to these Terms. In that case, "you" and "your" will refer to that company or other legal entity.</p>

                  <p><b>Changes to Terms or Services</b></p>

                  <p>We may modify the Terms at any time, in our sole discretion. If we do so, we&apos;ll let you know by posting the modified Terms on the Site. It&apos;s important that you review the Terms whenever we modify them because if you continue to use the Services after we have posted modified Terms, you are indicating to us that you agree to be bound by the modified Terms. If you don&apos;t agree to be bound by the modified Terms, then you may not use the Services anymore. Because our Services are evolving over time we may change or discontinue all or any part of the Services, at any time and without notice, at our sole discretion.</p>

                  <p><b>Who May Use the Services?</b></p>

                  <p><b>Eligibility</b></p>

                  <p>You may use the Services only if you are 13 years or older and are not barred from using the Services under applicable law.</p>

                  <p><b>Registration and Your Information</b></p>

                  <p>If you want to use certain features of the Services you&apos;ll have to create an account ("Account"). You can do this via the Site. It&apos;s important that you provide us with accurate, complete and up-to-date information for your Account and you agree to update such information, as needed, to keep it accurate, complete and up-to-date. If you don&apos;t, we might have to suspend or terminate your Account. You agree that you&apos;ll notify us immediately of any unauthorized use of your Account. You&apos;re responsible for all activities that occur under your Account, whether or not you know about them.</p>

                  <p><b>Feedback</b></p>

                  <p>We welcome feedback, comments and suggestions for improvements to the Services ("Feedback"). You can submit Feedback by emailing us at legal@glibr.net. You grant us a perpetual, free, and transferable license, with a right to sublicense, to use, copy, modify, create derivative works and otherwise exploit the Feedback in any form and media and for any purpose.</p>

                  <p><b>Privacy Policy</b></p>

                  <p>Please refer to our Privacy Policy for information on how we collect, use and disclose information from our users.</p>

                  <p><b>Content and Content Rights</b></p>

                  <p>For purposes of these Terms: (i) "Content" means text, graphics, images, music, software, audio, video, works of authorship of any kind, and information or other materials that are posted, generated, provided or otherwise made available through the Services; and (ii) "User Content" means any Content that Account holders (including you) provide to be made available through the Services. Content includes without limitation User Content.</p>

                  <p><b>Content Ownership, Responsibility and Removal</b></p>

                  <p>glibr does not claim any ownership rights in any User Content and nothing in these Terms will be deemed to restrict any rights that you may have to use and exploit your User Content. Subject to the foregoing, glibr and its licensors exclusively own all right, title and interest in and to the Services and Content, including all associated intellectual property rights. You acknowledge that the Services and Content are protected by copyright, trademark, and other laws of the United States and foreign countries. You agree not to remove, alter or obscure any copyright, trademark, service mark or other proprietary rights notices incorporated in or accompanying the Services or Content.</p>

                  <p><b>Rights in User Content Granted by You</b></p>

                  <p>By making any User Content available through Services you grant to glibr a worldwide, royalty-free, transferable license, with the right to sublicense, to use, copy, modify, create derivative works, display, perform and distribute your User Content in order to operate and provide the Services and Content to you and other Account holders.</p>

                  <p>TL:DR: we will use your User Content to run the Site; we won&apos;t take your User Content and sell it to others.</p>

                  <p>You are solely responsible for all of your User Content. You represent and warrant that you own all of your User Content or you have all rights that are necessary to grant us the license rights in your User Content under these Terms. You also represent and warrant that your User Content, your use and provision of your User Content to be made available through the Services, and any use of your User Content by glibr on or through the Services will not infringe, misSiteropriate or violate a third party&apos;s intellectual property rights, or rights of publicity or privacy, or result in the violation of any applicable law or regulation.</p>

                  <p>You can remove your User Content by specifically deleting it. However, in certain instances, some of your User Content (such as comments or messages you make) may not be completely removed and copies of your User Content may continue to exist on the Services. We are not responsible or liable for the removal or deletion of (or the failure to remove or delete) any of your User Content.</p>

                  <p><b>Rights in Content Granted by glibr</b></p>

                  <p>Subject to your compliance with these Terms, glibr grants you a limited, non-exclusive, non-transferable, non-sublicensable license to view, copy, display and perform the Content solely in connection with your permitted use of the Services and solely for your personal and non-commercial purposes.</p>

                  <p><b>Rights in Site</b></p>

                  <p>Subject to your compliance with these Terms, glibr grants you a limited non-exclusive, non-transferable, non-sublicenseable license to download and install a copy of the Site on a mobile device or computer that you own or control and to run such copy of the Site solely for your own personal, non-commercial purposes. You may not copy the Site, except for making a reasonable number of copies for backup or archival purposes. Except as expressly permitted in these Terms, you may not: (i) copy, modify or create derivative works based on the Site; (ii) distribute, transfer, sublicense, lease, lend or rent the Site to any third party; (iii) reverse engineer, decompile or disassemble the Site; or (iv) make the functionality of the Site available to multiple users through any means. glibr reserves all rights in and to the Site not expressly granted to you under these Terms.</p>

                  <p><b>glibr Enforcement</b></p>

                  <p>Please refer to our Community Guidelines for information about the types of User Content that may not be posted and the kinds of conduct that are prohibited while using the Site. Although we&apos;re not obligated to monitor access to or use of the Services or Content or to review or edit any Content, we have the right to do so for the purpose of operating the Services, to ensure compliance with our Community Guidelines, and to comply with applicable law or other legal requirements. We reserve the right, but are not obligated, to remove or disable access to any Content, at any time and without notice, including, but not limited to, if we, at our sole discretion, consider any Content to be objectionable or in violation of the Community Guidelines. We have the right to investigate violations of the Community Guidelines or conduct that affects the Services. We may also consult and cooperate with law enforcement authorities to prosecute users who violate the law.</p>

                  <p><b>DMCA/Copyright Policy</b></p>

                  <p>glibr respects copyright law and expects its users to do the same. It is glibr&apos;s policy to terminate in Siteropriate circumstances Account holders who repeatedly infringe or are believed to be repeatedly infringing the rights of copyright holders. Please see glibr&apos;s Copyright Policy for further information.</p>

                  <p><b>Links to Third Party Websites or Resources</b></p>

                  <p>The Services and Site may contain links to third-party websites or resources. We provide these links only as a convenience and are not responsible for the content, products or services on or available from those websites or resources or links displayed on such websites. You acknowledge sole responsibility for and assume all risk arising from, your use of any third-party websites or resources.</p>

                  <p><b>Termination</b></p>

                  <p>We may terminate your access to and use of the Services, at our sole discretion, at any time and without notice to you. You may cancel your Account at any time from within the Site. Upon any termination, discontinuation or cancellation of Services or your Account, the following provisions will survive: Feedback; Content Ownership, Responsibility and Removal; Warranty Disclaimers; Limitation of Liability; General Terms; and this sentence of Termination.</p>

                  <p><b>Warranty Disclaimers</b></p>

                  <p>The services and content are provided "AS IS," without warranty of any kind. Without limiting the foregoing, we explicitly disclaim any warranties of merchantability, fitness for a particular purpose, quiet enjoyment or non-infringement, and any warranties arising out of course of dealing or usage of trade. We make no warranty that the services will meet your requirements or be available on an uninterrupted, secure, or error-free basis. We make no warranty regarding the quality, accuracy, timeliness, truthfulness, completeness or reliability of any content.</p>

                  <p><b>Limitation of liability</b></p>

                  <p>Neither glibr nor any other party involved in creating, producing, or delivering the services or content will be liable for any incidental, special, exemplary or consequential damages, including lost profits, loss of data or goodwill, service interruption, computer damage or system failure or the cost of substitute services arising out of or in connection with these terms or from the use of or inability to use the services or content, whether based on warranty, contract, tort (including negligence), product liability or any other legal theory, and whether or not glibr has been informed of the possibility of such damage, even if a limited remedy set forth herein is found to have failed of its essential purpose. Some jurisdictions do not allow the exclusion or limitation of liability for consequential or incidental damages, so the above limitation may not sitely to you.</p>

                  <p>In no event will glibr&apos;s total liability arising out of or in connection with these terms or from the use of or inability to use the services or content exceed the amounts you have paid to glibr for use of the services or content or one hundred dollars ($100), if you have not had any payment obligations to glibr, as applicable. The exclusions and limitations of damages set forth above are fundamental elements of the basis of the bargain between glibr and you.</p>

                  <p><b>General Terms</b></p>

                  <p>These Terms constitute the entire and exclusive understanding and agreement between glibr and you regarding the Services and Content, and these Terms supersede and replace any and all prior oral or written understandings or agreements between glibr and you regarding the Services and Content. If for any reason a court of competent jurisdiction finds any provision of these Terms invalid or unenforceable, that provision will be enforced to the maximum extent permissible and the other provisions of these Terms will remain in full force and effect. You may not assign or transfer these Terms, by operation of law or otherwise, without glibr&apos;s prior written consent. Any attempt by you to assign or transfer these Terms, without such consent, will be null. glibr may freely assign or transfer these Terms without restriction. Subject to the foregoing, these Terms will bind and inure to the benefit of the parties, their successors and permitted assigns. Any notices or other communications provided by glibr under these Terms, including those regarding modifications to these Terms, will be given: (i) via email; or (ii) by posting to the Services. For notices made by e-mail, the date of receipt will be deemed the date on which such notice is transmitted. glibr&apos;s failure to enforce any right or provision of these Terms will not be considered a waiver of such right or provision. The waiver of any such right or provision will be effective only if in writing and signed by a duly authorized representative of glibr. Except as expressly set forth in these Terms, the exercise by either party of any of its remedies under these Terms will be without prejudice to its other remedies under these Terms or otherwise.</p>

                  <p><b>Contact Information</b></p>

                  <p>If you have any questions about these Terms or the Services, please contact glibr at legal@glibr.net</p>

                  <p><b>glib on</b></p>

            </div>
      </div>
    );
  }
}
