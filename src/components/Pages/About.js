import React, { Component } from 'react';

//import Main from '../Main';
import '../../global.css'
import './style.css';

export default class TermsOfService extends Component {
  render() {
    return (
      <div className="container">
      <div className="container-green-border text-align-left container-padding-adder25">

          <h1>About <span className="glibr-font font-bold font-spacing-add">glibr</span></h1>
          <p> Last updated: April 13, 2020</p>

<p><span className="glibr-font font-bold font-spacing-add font-spacing-add">glibr</span> is the new social network built on three pillars to make it the <span className="glibr-font font-bold font-spacing-add">goodest</span> social network</p>

<ol>
  <li>Pennies fund <span className="glibr-font font-bold font-spacing-add">glibr</span>; no tracking; no selling personal data; no targeted advertising. Nothing.</li>
  <li>Charity partnerships are a part of our founding.  <span className="glibr-font font-bold font-spacing-add">glibr</span> will start with 6 charity partners. Users will select which charity will partially benefit from their posts.</li>
  <li><span className="glibr-font font-bold font-spacing-add">glibr</span> keeps the good of social media by building the community in the right way.  Standard FCC regulations are embedded in <span className="glibr-font font-bold font-spacing-add">glibr</span> so things you that get bleeped on public broadcast media are not posted in <span className="glibr-font font-bold font-spacing-add">glibr</span>.  
</li>
</ol>

<p>
<b>Pennies:</b><br />
<span className="glibr-font font-bold font-spacing-add">glibr</span> charges for every post made on the site – 2 pennies per post.  Users post 150 times before they get a $3 bill charged to their visa, held by Stripe.  <span className="glibr-font font-bold font-spacing-add">glibr</span> does not even hold your visa card.
</p>

<p>
<b>Charities:</b><br />
When you sign up with <span className="glibr-font font-bold font-spacing-add">glibr</span> you choose a charity to pair with.  Every post made includes the chosen charity mention. Every post made adds money to a charitable donation to the chosen charity. Quarterly, <span className="glibr-font font-bold font-spacing-add">glibr</span> adds all those pennies up, and donates 20% of our profits to all the charities, divvied by the percentage of pennies from the posts made for that charity.
</p>

<p>
<b>Community:</b><br />
People are hungry for a gooder social media. The positive culture around <span className="glibr-font font-bold font-spacing-add">glibr</span> will grow, from the support for charities, the freedom from marketing, data collection and tracking, and from the common sense FCC standards we are all used to in broadcast media. <span className="glibr-font font-bold font-spacing-add">glibr</span> -  will self-regulate itself as the users stay for the positive culture.
</p>


<p>The goodest social network.</p>

<p>Come try it out at <span className="glibr-font font-bold font-spacing-add">glibr</span>.com</p>

<hr className="style-two" />

<p>
<b>Details:</b>
<br />

<ul>
  <li><span className="glibr-font font-bold font-spacing-add">glibr</span> does not collect personal information.  The only piece of personal information that <span className="glibr-font font-bold font-spacing-add">glibr</span> needs is an email address.  Everything else is optional by the user.
</li>
  <li>When a user signs up <span className="glibr-font font-bold font-spacing-add">glibr</span> does require a Credit Card from the user but <span className="glibr-font font-bold font-spacing-add">glibr</span> doesn’t store the CC number.  We partner with Stripe (www.stripe.com) to handle all our credit card processing.  Stripe handles all that and <span className="glibr-font font-bold font-spacing-add">glibr</span> stores a single unique identifier for our interactions with Stripe.  <span className="glibr-font font-bold font-spacing-add">glibr</span> never sees, saves, or tracks the Credit Cards of users.  Never.
</li>
  <li><span className="glibr-font font-bold font-spacing-add">glibr</span> will provide a report to the Charities on a quarterly basis and a donation which will be the 20% portion of the profits apportioned by the percentage of all pennies posted to <span className="glibr-font font-bold font-spacing-add">glibr</span> in that quarter.
</li>
<li><span className="glibr-font font-bold font-spacing-add">glibr</span> has built in protections against bad content.  We’ve built in controls against bad words, adhering to the FCC rules against not allowed words and filtering them out.  <span className="glibr-font font-bold font-spacing-add">glibr</span> also has integrated with an API the parses and removes porn pictures.  The site stays clean.
</li>
</ul>
</p>












</div>
    </div>
    );
  }
}
