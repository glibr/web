import React, { Component } from 'react';
import { Link } from 'react-router';
import './style.css';

import EnvironmentSettings from "../../common/environmentsettings.js"

let envSettings = new EnvironmentSettings();


export default class ProfileWidgetSmall extends Component {
  constructor(props) {
    super(props);

    this.state = { 
      numFollows:0,
      numFollowers:0,
      numDoubleFollows:0,
      numGlibs:0,
      profilePicSrc: '/images/logo_NO_profilePic.png',

        };
  }

  componentDidMount() {

    if(this.props.numberOfGlibs !=='undefined'){
      this.setState({
        numGlibs: this.props.numberOfGlibs
      })      
    }

    if(this.props.follows !== 'undefined'){
      this.setState({
        numFollows: this.props.follows
      })     
    }

    if(this.props.followers !=='undefined'){
      this.setState({
        numFollowers: this.props.followers
      })
     
    }

    if(this.props.doubledFollows !=='undefined'){
      this.setState({
        numDoubleFollows: this.props.doubledFollows
      }) 
    }

    if(this.props.profilePicture !=='undefined'){
      this.setState({
        profilePicSrc: envSettings.GLIBR_IMAGE_ROOT_URI()+  this.props.profilePicture
      }) 
    }

}//end componentDidMount    


  render() {

    return (

      <div className="profileWidget-border text-align-left " >


              <table  width="100%" cellPadding="2">
                  <tbody>
                      <tr >
                        <td  rowSpan="3" >

                        <div className="profile-image-circle-cropper">
                                    <img src={this.state.profilePicSrc} alt={this.props.handle + "'s Avatar"}  className="profile-profile-picture" />
                              </div>

                        </td>
                        <td colSpan="2"  className="glibr-font">
                          
                          {/* implement forceUpdate here because we are changing users and the ROUTER doesn't know to update.
                                it still thinks its on the same page - which is stupid.
                                also throws an enqueueForceUpdate error
                                */}
                 <Link onClick={this.forceUpdate}  to={this.props.handle}>{this.props.handle}</Link>


                          
                        </td>
                    </tr>
              
                    <tr>
                    <td  className="tiny-text div-no-wrap">
glibs: {this.state.numGlibs}
                          </td>

                     
                          <td  className="tiny-text div-no-wrap">
follows: {this.state.numFollows}
                          </td>

                    </tr> 
                    {/* row 4 */}
                    <tr>
                    <td className="tiny-text div-no-wrap">
followers: {this.state.numFollowers}

                    </td>
           
                          <td className="tiny-text div-no-wrap">
doubled: {this.state.numDoubleFollows}
                          </td>                          
                     
                    </tr> 
                   </tbody>
              </table>





    </div>

     
   
    );
  }
}
