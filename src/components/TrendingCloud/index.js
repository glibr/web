import React, { Component } from 'react';
import classnames from 'classnames';

import './style.css';

export default class TrendingCloud extends Component {
  render() {
    const { className, ...props } = this.props;
    return (
      <div className={classnames('trending-cloud', className)} {...props}>
        <h1>
          Trending Cloud?
        </h1>
      </div>
    );
  }
}