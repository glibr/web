import React, { Component } from 'react';
import EnvironmentSettings from "../../common/environmentsettings.js";
import GLOBALS from "../../common/globals.js";

import Helper from "../../common/helpers.js";

const Compress = require('compress.js')
const compress = new Compress();


// Libraries and constants
let envSettings = new EnvironmentSettings();

export default class UploadButton extends Component {
      constructor(props) {
            super(props);
      
            this.state = {
                  image64Out: null,
            }

      }        
      uploadImage(e) {
            let uploadResponse = {
                  success: false,
                  message: 'spinner',
                  fileName: null,
                  tempFIleSource: '/images/resources/green-load.gif',
            }
            this.props.uploadFileResponse(uploadResponse);

            this.resizeAndUpload(e);
      }

      async resizeAndUpload(e) {

            let files = null;
            let uploadResponse = {
                  success: false,
                  message: null,
                  fileName: null,
                  tempFIleSource: null,
            }


            try {
                  files = [...e.target.files];
            } catch (err) {
                  uploadResponse = {
                        success: false,
                        message: 'There was a problem finding the file.  Please try again.',
                        fileName: null,
                        tempFIleSource: null,
                  }
                  // prop injector function
                  // <UploadImageControl   uploadFileResponse={this.uploadResponse}   />
                  this.props.uploadFileResponse(uploadResponse);
                  return null;
                  //exit
            }


            let fileToUpload = null;
            let thisBlob = null;

            let supportedFormats = ['image/jpg', 'image/jpeg', 'image/gif', 'image/png'];

            if (files[0] && files[0].type) {
                  if (0 > supportedFormats.indexOf(files[0].type)) {

                        uploadResponse = {
                              success: false,
                              message: 'That is not a supported File Type.  Please only upload a png or jpg or gif file.  Thank you.',
                              fileName: null,
                              tempFIleSource: null,
                        }
                        // prop injector function
                        // <UploadImageControl   uploadFileResponse={this.uploadResponse}   />
                        this.props.uploadFileResponse(uploadResponse);
                        return null;
                        //exit
                  }
            }

            console.log("check 1");

            // compress image before uploading to server.
            let conpressFormats = ['image/jpg', 'image/jpeg', 'image/png'];
            let formData = new FormData();
            let img = null;
            let imgSize = 0;
            let resized = false;
            let  filePrefix = null;
            let filePreview64 = null;

        

            if ( conpressFormats.indexOf(files[0].type) >= 0  ) {

                  const images = await compress.compress(files, {
                        size: 4, // the max size in MB, defaults to 2MB
                        quality: .85, // the quality of the image, max is 1,
                        maxWidth: 800, // the max width of the output image, defaults to 1920px
                        // maxHeight: 1920, // the max height of the output image, defaults to 1920px
                        resize: true, // defaults to true, set false if you do not want to resize the image width and height
                  })

             

                  img = images[0];
                  filePrefix = img.prefix;
                  filePreview64 = img.data;
                  thisBlob = Compress.convertBase64ToFile(img.data, img.ext);
                  fileToUpload = new File([thisBlob], files[0].name, { type: files[0].type, size: thisBlob.size, originalname: files[0].name, lastModified: Date.now() });
                  imgSize = thisBlob.size;
                  resized = true;

            } else {
               // we are a .gif
                  filePrefix = `data:${files[0].type};base64,`               
                  const img64 = await Helper.getBase64File(files[0]);
                  filePreview64 =  img64.base64StringFile;             
                  fileToUpload = files[0];
                  img = files[0]; 
                  imgSize = files[0].size;
            }

      


            formData.append('picToUpload', fileToUpload);
            formData.append('apikey', GLOBALS.APIKEY);
            formData.append('resized', resized);
            formData.append('filename', files[0].name);
            formData.append('mimetype', files[0].type);
            formData.append('size', imgSize);
            formData.append('originalname', files[0].name)




            try {

                  const settings = {
                        credentials: 'include',
                        method: 'POST',
                        body: formData,
                  };

                  const response = await fetch(envSettings.GLIB_API_URI() + '/utils/imageupload', settings)

                  const responseJSON = await response.json();
                  if (response.status === 200) {

                        if (responseJSON.success) {

                              uploadResponse = {
                                    success: true,
                                    message: null,
                                    fileName: responseJSON.fileName,
                                    tempFIleSource: `${filePrefix}${filePreview64}`
                              }
                        } else {
                              // failure branch
                              uploadResponse = {
                                    success: false,
                                    message: responseJSON.message,
                                    fileName: null,
                                    tempFIleSource: null,
                              }
                        }
                  } else { // probably 400 and probably our main failure branch
                        uploadResponse = {
                              success: false,
                              message: responseJSON.message,
                              fileName: null,
                              tempFIleSource: null,
                        }
                  }
            } catch (err) {
                  console.log(err)
                  uploadResponse = {
                        success: false,
                        message: `Something went wrong: ${err}`,
                        fileName: null,
                        tempFIleSource: null,
                  }
            }

            // prop injector function
            // <UploadImageControl   uploadFileResponse={this.uploadResponse}   />
            this.props.uploadFileResponse(uploadResponse);

      }

        

      render() {

            return (

                  <label className="glibPictureUploadLabel">
                        <input type="file" onChange={this.uploadImage.bind(this)} id="uploadResizer" />
                        <span className="glibr-font-white">upload picture</span>

                        {this.state.image64Out}

                  </label>

            )

      }

} // end class



