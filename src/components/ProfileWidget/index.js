import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import './style.css';
import EnvironmentSettings from "../../common/environmentsettings.js";

import RESTCLIENT from "../../common/restClient.js";
import GLOBALS from "../../common/globals.js";

import FollowButton from '../FollowButton';

//setCurrentUserFollowsList
// Import Actions
import {
    setCurrentUserFollowsList
} from '../../actions/userActions';

let envSettings = new EnvironmentSettings();

// Short hand for mapDispatch
// removes the need for the mapdispatch method and the long list of setters
// "actionCreators" is passed as the second parameter in the connect call - see bottom of this module
const actionCreators = {
    //user actions
    setCurrentUserFollowsList
}
let RestClient = new RESTCLIENT();

export class ProfileWidget extends Component {

    constructor(props) {
        super(props);

        this.state = {
            profilePictureSrcUrl: '/images/logo_NO_profilePic.png',
            userFollowList: this.props.user.currentUserFollowList,
            showButton: "hide"
        }

    }


    async componentDidMount() {
   
        // user in props is our logged in user
        // user not in props is the user for this page
       await this.getUserLink(this.props.user.currentUserId, this.props.currentUserId);

        //set avatar
        if (this.props.profilePicture) {
            this.setState({
                profilePictureSrcUrl: this.props.profilePicture
            })
        }
    }


    async getUserLink(primeId, followId) {
        // get user type by group id and handle
        // console.log("user ids: " + primeId + " " + followId);

        try {
            // logged in user is following the current profile page user
            const mQuery = `/users/userlink/${primeId}/${followId}`;

            // console.log(mQuery);
            const settings = {
                credentials: 'include',
                method: 'GET',
            };
            const apikey = `?apikey=${GLOBALS.APIKEY}`

            const response = await fetch(`${envSettings.GLIB_API_URI()}${mQuery}${apikey}`, settings);

            const responseJSON = await response.json();    

                if (responseJSON.success === true) {
                    this.setState({
                        showButton: "hide"
                    })
                    return{
                        success: true,
                        response: responseJSON,
                    }

                } else {
                    
                    this.setState({
                        showButton: "show"
                    })
                    return{
                        success: false,
                        response: responseJSON,
                    }
            }
        } catch (err) {
            this.setState({
                showButton: "show"
            })
            return{
                success: false,
            }
        }
    }


    // Execute the follow user metthod.
    _followUser(event) {

        event.preventDefault();

        // console.log("clicked link user.");

        let followData = {
            handle: this.props.handle
        };

        RestClient.fetchPost('/linkuser', followData, response => {
            if (response.success) {
                this.props.setCurrentUserFollowsList(response.followList);
                this.setState({
                    userFollowList: response.followList,
                    showButton: "hide"
                });
            }
        }, err => {
            console.log("link user error: " + err);
        });
    }


    // Execute the unlink  user method.
    _unFollowUser(event) {

        event.preventDefault();

        let followData = {
            handle: this.props.handle
        };

        RestClient.fetchPost('/unlinkuser', followData, response => {
            if (response.success) {
                this.props.setCurrentUserFollowsList(response.followList);
                this.setState({
                    userFollowList: this.props.user.currentUserFollowList,
                    showButton: "show"
                });
            }
        });
    }








    render() {


        let welcomeMessage = GLOBALS.STRINGS.PROFILE_SALUTATION_USER;
        if (!this.props.currentUser) {
            welcomeMessage = GLOBALS.STRINGS.PROFILE_SALUTATION_ANON;
        }

        return (
            <div className="profile-widget-wrapper">

                <div className="profile-widget-top">
                    <span className="image-circle-cropper">
                        <div className="inner">
                            <img
                                alt="User profile"
                                className="profile-picture"
                                src={this.state.profilePictureSrcUrl}
                            />
                        </div>

                    </span>

                </div>


                <h3 className="d-none d-md-block" >{welcomeMessage} {this.props.firstName}!</h3>
                <span>
                    <Link to={'/' + this.props.handle}  >@{this.props.handle}</Link></span>
                    <br />
              

                <FollowButton
                    thisUser={this.props.handle}
                    userFollowList={this.props.userFollowList}
                    showButton={this.state.showButton}
                    onUnlinkClick={this._unFollowUser.bind(this)}
                    onLinkClick={this._followUser.bind(this)}
                />


                <div className="profile-widget-bottom d-none d-sm-block">

                    <hr className="style-three" />

                    <label className="font-upsize">charity</label><br />
                    <img src={this.props.charityLogoForUser} className='profile-charity-logo' alt='charity logo'/>

                    <hr className="style-three" />

                    <label className="font-upsize">glibs</label>
                    <div className="glibr-font font-upsize">{this.props.numberGlibs}</div>

                    <hr className="style-three" />

                    <label className="font-upsize">following</label>
                    <div className="glibr-font font-upsize">{this.props.numberFollows}</div>

                    <hr className="style-three" />

                    <label className="font-upsize">followers</label>
                    <div className="glibr-font font-upsize">{this.props.numberFollowers}</div>

                    <hr className="style-three" />

                    <label className="font-upsize">doubled</label>
                    <div className="glibr-font font-upsize">{this.props.numberOfDoubled}</div>

                </div>
            </div>
        );
    }
}


//Map the properties of the page to store items
const mapStateToProps = (store) => {
    return {
        //user properties
        user: store.userState,
        //authenticate properties
        authenticate: store.authState
    };
};


//connect is the redux method that connects redux and react
export default connect(mapStateToProps, actionCreators)(ProfileWidget);
