import React, { Component } from 'react';
import './style.css';
import Helper from '../../common/helpers';
let parseMentions = require('parse-mentions');

const GLOBALS = require('../../common/globals.js');

export default class NavBar extends Component {
  constructor(props) {
    super(props);
    this.searchRef = React.createRef();
    this.compactMenuRef = React.createRef();


    this.state = {
      width: null,
      height: null
    }



  }

  componentDidMount() {


    this._updateDimensions();

    window.addEventListener("resize", this._updateDimensions);
  }
  componentWillUnmount() {
    window.removeEventListener("resize", this._updateDimensions);
  }

  render() {

    if (this.state.width > 600 || this.state.width === null) {

      return (


        <nav className="navbar navbar-expand-md navbar-light ">

          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#glibrMainNavbar" aria-controls="glibrMainNavbar" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon text-align-right"></span>
          </button>

          <div className="collapse navbar-collapse  container-green-border text-align-center" id="glibrMainNavbar">
            <ul className="navbar-nav mr-auto ul-nav">

              <li className="nav-item li-nav">
                <button className="see-follower-requests btn btn-default" onClick={() => { this.props.resetGlibFeed("full") }}>
                  All glibs
                </button>
              </li>

              <li className="nav-item li-nav">
                <button className="see-follower-requests btn btn-default" onClick={() => { this.props.resetGlibFeed("mostExpensiveAllTime") }}>
                  most pennies ALL TIME
                </button>
              </li>

              <li className="nav-item li-nav">
                <button className="see-follower-requests btn btn-default" onClick={() => { this.props.resetGlibFeed("mostExpensiveToday") }}>
                  most pennies TODAY
                </button>
              </li>

              <li className="nav-item li-nav">
                <button className="see-follower-requests btn btn-default" onClick={() => { this.props.resetGlibFeed("glibsOfFollows") }}>
                  people you follow
                </button>
              </li>

              <li className="nav-item li-nav">
                <button className="see-follower-requests btn btn-default" onClick={() => { this.props.resetGlibFeed("glibsOfFollowers") }}>
                  people following you
                </button>
              </li>

              <li className="nav-item li-nav">
                <button className="see-follower-requests btn btn-default" onClick={() => { this.props.resetGlibFeed("glibsOfDoubled") }}>
                  people you've doubled
                </button>
              </li>


              <li className="nav-item li-nav-nowrap">
                <input type="text" placeholder="Search..." id="glibrSearch" ref={this.searchRef} />
                <input type="button" id="glibrSearchButton" value="go" onClick={() => { this._processSearch() }} />
              </li>

            </ul>

          </div>
        </nav>

      )


    } else {
      return (
        <navbar>
        <div>
          <div className='navbar-compact cursor-hand' onClick={() => { this._toggleCompactMenu() }} > &#9660; change feed &#9660;
        </div>

          <div className='navbar-compact-menu' id='navbar-compact-menu' ref={this.compactMenuRef}>

            <table width="100%">
             
              <tbody>
                <tr>
                  <td colSpan="2" className="text-align-center">
                    <input type="text" placeholder="Search..." id="glibrSearch" ref={this.searchRef} />
                    <input type="button" id="glibrSearchButton" value="go" onClick={() => { this._processSearch() }} />
                  </td>
                </tr>
                <tr>
                  <td className="text-align-center container-padding-adder"><div className="container-purple-border container-full-width glibr-font font-upsize cursor-hand font-line-height-double" onClick={() => { this.props.resetGlibFeed("full") }}>
                    All glibs
  </div></td>
                  <td className="text-align-center container-padding-adder"><div className="container-purple-border container-full-width glibr-font font-upsize cursor-hand" onClick={() => { this.props.resetGlibFeed("glibsOfDoubled") }}>
                    people you've doubled
  </div></td>
                </tr>
                <tr>
                  <td className="text-align-center container-padding-adder"><div className="container-purple-border container-full-width glibr-font font-upsize cursor-hand" onClick={() => { this.props.resetGlibFeed("glibsOfFollows") }}>
                    people you follow
  </div>   </td>
                  <td className="text-align-center container-padding-adder"><div className="container-purple-border container-full-width glibr-font font-upsize cursor-hand" onClick={() => { this.props.resetGlibFeed("glibsOfFollowers") }}>
                    people following you
  </div></td>
                </tr>
                <tr>
                  <td className="text-align-center container-padding-adder"><div className="container-purple-border container-full-width glibr-font font-upsize cursor-hand" onClick={() => { this.props.resetGlibFeed("mostExpensiveAllTime") }}>
                    most &cent;&cent;  ALL TIME
  </div></td>
                  <td className="text-align-center container-padding-adder"><div className="container-purple-border container-full-width glibr-font font-upsize cursor-hand" onClick={() => { this.props.resetGlibFeed("mostExpensiveToday") }}>
                    most &cent;&cent;  TODAY
  </div></td>
                </tr>
                </tbody>
            </table>



          </div>


          </div>
          </navbar>
          )
        }
    
    
      }
    
  async _processSearch() {
    var node = this.searchRef.current;
      
          // check for  mentions
    if (node.value.length > 0) {
      //short curcuit search if there is an @mention in the search box
      //regardless of length of search
      var mentions = parseMentions(node.value);
      if (mentions.matches.length > 0) {
            // go to the first person.
            window.location.href = '/' + mentions.matches[0].name;
        }
  
        // shorten string
        let searchString = escape(node.value.substring(0, GLOBALS.CONSTANTS.SEARCH_STRING_LENGTH));
  
        // clean and check for bad words
        const checkSearchString = await Helper.checkForBadWords(searchString);
  
        // success? go to search results page
      if (checkSearchString.success) {
            window.location.href = '/search?q=' + escape(checkSearchString.cleanedString);
      } else {
            console.log(checkSearchString.foundBadText);
        }
  
        console.log(escape(node.value));
  
      }
    }
  
  
  _updateDimensions() {
    try {

            console.log("height: " + window.innerWidth);
    
      this.setState({
            width: window.innerWidth,
          height: window.innerHeight
        });
    } catch (err) {
            console.log(err);
        }
    
      }
    
  _toggleCompactMenu() {  // function that will toggle active/false


    var navRef = this.compactMenuRef.current;
      
          var display = navRef.style.display;
      
    if (display === "block") {
            navRef.style.display = "none";
    } else {
            navRef.style.display = "block";
        }
      }
    
    }//end of class
    
