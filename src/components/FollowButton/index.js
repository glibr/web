import React, { Component } from 'react';
import { connect } from 'react-redux';


export class FollowButton extends Component {
  

  render() {

    //we only want to render the button if we are not the logged in user and we are not following this dude.
    //are we the logged in user?
    if (this.props.thisUser === "" || this.props.thisUser === this.props.user.currentUserHandle) {
      return null;
    } else {
      // we are some other dude.
      
      if (this.props.showButton === "show") {
        //show the button
        return (
          < section className="follow-this-user-container" >
            <button onClick={this.props.onLinkClick} className="follow-this-user btn-primary btn" alt={this.props.userFollowList}>Follow @{this.props.thisUser}</button>
          </section >
        );

      } else {
        return (
          <section>
            <div className="already-following" alt={this.props.userFollowList}>&#10004; Following</div>
           <span className="tiny-text"  onClick={this.props.onUnlinkClick}>unfollow</span>
          </section>
        );
      }
    }
  }

} // end class


//Map the properties of the page to store items
const mapStateToProps = (store) => {
  return {
    //user properties
    user: store.userState
  };
};


//connect is the redux method that connects redux and react
export default connect(mapStateToProps)(FollowButton);
