import React, { Component } from "react";
import { Fragment } from "react";

import Header from "../../containers/Header";
import Footer from "../Footer";

import "./style.css";

class App extends Component {
  render() {
  
    if (
      this.props.location.pathname === "/shareglib" ||
      this.props.location.pathname === "/zombocom/"
    ) {
      return <Fragment>{this.props.children}</Fragment>;
    } else {
      return (
        <div className="app container">
          <Header />
          {this.props.children}
          <Footer />
        </div>
      );
    }
  }
}

export default App;
