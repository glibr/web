import React, { Component } from 'react';
import HelperMethods from "../../common/helpers.js";
import GLOBALS from "../../common/globals.js";


// Props to pass in:
// function prop to receive this.state.textElementDirty
// function prop - onSetGroupname -  to receive value back
// ElementPlaceHolder for placeholder text in text field
// elementType : text || textArea (changes form elment render style)
// optional textAreaRows : defaults to three
// optional checkForGroupDupe: default false - do we check for group dupe in this validation pass
// optional textlength: default 255 - 55|150|255|1000
export class ValidText extends Component {
      constructor(props) {
            super(props);

            this.handleChange = this.handleChange.bind(this);
            this.handleOnBlur = this.handleOnBlur.bind(this);

            this.state = {
                  textElementValue: this.props.defaultValue,
                  textElementErrorMessage: null,
                  textElementErrorClassName: 'hide',
                  textElementDirty: false,
                  requiredTextClassName:'hide',
            }
      }



      handleChange(e) {
            this.setState({ textElementValue: e.target.value });
      }


      async handleOnBlur(e) {

            const textToCheck = e.target.value;
            let foundErrorThisPass = false;

            if(this.props.checkForAlphaNumeric){
                  const resAlphaCheck = await HelperMethods.validateThis('text', 'alphanumeric-only', textToCheck);
                  if (!resAlphaCheck) {
                        foundErrorThisPass = true;
                        this.setState({
                              textElementErrorMessage: GLOBALS.STRINGS.TEXT_INPUT_ALPHA_NUMERIC_CHECK,
                              textElementErrorClassName: 'response-container-error',
                        });
                  }
            }
 

            if (this.props.textLength === '55') {
                  const resLength55Check = await HelperMethods.validateThis('text', 'text-length-55', textToCheck);
                  if (!resLength55Check) {
                        foundErrorThisPass = true;
                        this.setState({
                              textElementErrorMessage:GLOBALS.STRINGS.TEXT_INPUT_LENGTH_55,
                              textElementErrorClassName: 'response-container-error',
                        });
                  }
            }

            if (this.props.textLength === '150') {
                  const resLength55Check = await HelperMethods.validateThis('text', 'text-length-150', textToCheck);
                  if (!resLength55Check) {
                        foundErrorThisPass = true;
                        this.setState({
                              textElementErrorMessage: GLOBALS.STRINGS.TEXT_INPUT_LENGTH_150,
                              textElementErrorClassName: 'response-container-error',
                        });
                  }
            }

            if (this.props.textLength === '255') {
                  const resLength55Check = await HelperMethods.validateThis('text', 'text-length-255', textToCheck);
                  if (!resLength55Check) {
                        foundErrorThisPass = true;
                        this.setState({
                              textElementErrorMessage: GLOBALS.STRINGS.TEXT_INPUT_LENGTH_255,
                              textElementErrorClassName: 'response-container-error',
                        });
                  }
            }

            if (this.props.textLength === '1000') {
                  const resLength55Check = await HelperMethods.validateThis('text', 'text-length-1000', textToCheck);
                  if (!resLength55Check) {
                        foundErrorThisPass = true;
                        this.setState({
                              textElementErrorMessage: GLOBALS.STRINGS.TEXT_INPUT_LENGTH_1000,
                              textElementErrorClassName: 'response-container-error',
                        });
                  }
            }

            const resClean = await HelperMethods.checkForBadWords(textToCheck);

            if (resClean.success) {
                  // check for found bad text
                  if (resClean.foundBadText) {
                        foundErrorThisPass = true;
                        this.setState({
                              textElementValue: resClean.cleanedString,
                              textElementErrorMessage: GLOBALS.STRINGS.TEXT_INPUT_CONTAINS_BAD_WORDS,
                              textElementErrorClassName: 'response-container-error',
                        });
                  } else if (resClean.foundHTML) {
                        foundErrorThisPass = true;
                        this.setState({
                              textElementValue: resClean.cleanedString,
                              textElementErrorMessage: GLOBALS.STRINGS.TEXT_INPUT_CONTAINS_HTML,
                              textElementErrorClassName: 'response-container-error',
                        });
                  } else if (!foundErrorThisPass) {
                        this.setState({
                              textElementErrorMessage: null,
                              textElementErrorClassName: 'hide',
                        });
                  }
            } else if (!foundErrorThisPass) {
                  // no error message - we will be validating on the server as well
                  this.setState({
                        textElementErrorMessage: null,
                        textElementErrorClassName: 'hide',
                  });
            }

            //  if this is a check fpr dupe of a group
            if (this.props.checkForGroupDupe) {
                  const resDupe = await HelperMethods.checkFoGroupDupe(textToCheck);
                  //and check for dupe
                  if (resDupe.success) {
                        foundErrorThisPass = true;
                        this.setState({
                              textElementErrorMessage: GLOBALS.STRINGS.GROUPS_DUPE_NAME,
                              textElementErrorClassName: 'response-container-error',
                        });
                  } else if (!foundErrorThisPass) {
                        this.setState({
                              textElementErrorMessage: null,
                              textElementErrorClassName: 'hide',
                        });
                  }
            }


            this.props.onSetTextValue(this.state.textElementValue);

            // is there text in the text box
            // and did we find an error
            if(this.state.textElementValue.length<=0 ){
                  this.props.onSetElementDirty(true);
            } else if (foundErrorThisPass){
                  this.props.onSetElementDirty(true);
            }else {
                  this.props.onSetElementDirty(false);
            }
            
      }

      componentDidMount(){
                        // check for required
                        if(this.props.required){
                              this.setState({
                                    requiredTextClassName: 'tiny-text-red text-align-right',
                              })
                        }
      }


      render() {

            if (this.props.elementType === 'textarea') {

                  return (
                        <div className="form-group row">
                              <div className="col-12">
                                    <span className={this.state.textElementErrorClassName}>{this.state.textElementErrorMessage}</span>
                                    <textarea className="form-control md-textarea"
                                          rows={this.props.textAreaRows}
                                          placeholder={this.props.ElementPlaceHolder}
                                          value={this.state.textElementValue}
                                          onChange={this.handleChange}
                                          onBlur={this.handleOnBlur}
                                    />
                                    <div className={this.state.requiredTextClassName}>*required</div> 
                              </div>
                        </div>
                  )



            } else {
                  return (

                        <div className="form-group row">
                              <div className="col-12">
                                    <span className={this.state.textElementErrorClassName}>{this.state.textElementErrorMessage}</span>
                                    <input type="text"
                                          className="form-control"
                                          placeholder={this.props.ElementPlaceHolder}
                                          value={this.state.textElementValue}
                                          onChange={this.handleChange}
                                          onBlur={this.handleOnBlur} />                                          
                                    <div className={this.state.requiredTextClassName}>*required</div> 
                              </div>
                        </div>


                  );
            }

      }
}

//-----------------------------------------------
//  Set Default props values
ValidText.defaultProps = {

      checkForGroupDupe: false,
      checkForAlphaNumeric: false,
      elementType: 'text',
      textAreaRows: '3',
      testLength:'255',
      required:false,

};

export default ValidText;