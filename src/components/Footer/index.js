import React, { Component } from 'react';
import { Link } from 'react-router';

export default class Footer extends Component {
  render() {
    return (
      <div className="form-group row">
        <div  className="col-3 text-align-center glib-font-light-green">
          <Link to="/about">about</Link>
        </div>
        
        <div  className="col-3 text-align-center glib-font-light-green">
          <Link to="/tos">terms of service</Link>
        </div>
        
        <div  className="col-3 text-align-center glib-font-light-green">
          <Link to="/privacy">privacy policy</Link>
        </div>
        
        <div  className="col-3 text-align-center glibr-font">
           &copy; {new Date().getFullYear()} glibr
        </div>
      </div>
    );
  }
}