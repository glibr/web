import React from 'react';
import { Link } from 'react-router';



const headerUI = (props) => {

  var adminLink = null;

  // Add administrator control
  if (props.currentUserRole === 'admin') {
    adminLink = (
      <span> | <Link to="admin" className="glibr-font-highlight">admin</Link> </span>
    )

  }


  //set default - Not logged in.
  var loginLinks = (
    <div className="pull-right align-text-top auth-container glibr-font">
      <Link to="login" className="glibr-font-highlight">log in</Link> | <Link to="register" className="glibr-font-highlight">register</Link>
    </div>

  );


  if (props.isAuthenticated) {

    var profileThumbnailclassName = props.profilePicture ? 'profile-thumbnail' : 'profile-thumbnail hide';

    // overwrite if authenticated
    loginLinks = (

      <div className="pull-right align-text-top auth-container glibr-font-highlight">

        <a href="#logout" className="glibr-font-highlight" onClick={props.onClick}>logout</a>
        {adminLink}
        &nbsp;|&nbsp;
        <Link to="profile" className="glibr-font-highlight">
            profile   &nbsp;
            <img alt="Pofile thumbnail" className={profileThumbnailclassName} src={props.profilePicture} />&nbsp;
        </Link>

      </div>

    );
  }

  return (


    <div className="container-full-width text-align-center ">

      <div className="form-group row">
        <div className="col-8 logo-container text-align-left">
          <Link to="/" >
            <img className="logo" src={process.env.PUBLIC_URL + '/images/logo.png'}  alt="glibr.com" />
            <h1 className="style-one" id="logoName">glibr</h1>
          </Link>
        </div>
        <div className="col-4 header-container">{loginLinks}</div>
      </div>

      <div className="form-group row text-align-center glibr-tagline">      
      <div className="col-12 text-align-center">
          <h1 className="glib-font-light-green style-three font-spacing-add">The goodest social network.</h1>
      </div>
      </div>

    </div>

  );

}


export default headerUI;
