import React, { Component } from 'react';
import { connect } from 'react-redux';

import PaymentAccrued from "./Payments/penniesAccrued.js";

class Admin extends Component {
      constructor(props) {
            super(props);
            this.state = {
                  adminView: 'empty',

                  userAdminClass: "glibr-font",
                  glibsAdminClass: "glibr-font",
                  groupsAdminClass: "glibr-font",
                  paymentsAdminClass: "glibr-font",
                  charitiesAdminClass: "glibr-font",
            }

      }


      loadAdminView(adminType) {

            this.setAdminViewClass(adminType);

            switch (adminType) {
                  case 'users':
                        this.setState({
                              adminView: `user managment`,
                        });
                        break;
                  case 'glibs':
                        this.setState({
                              adminView: `glib managment`,
                        });
                        break;
                  case 'groups':
                        this.setState({
                              adminView: `groups managment`,
                        });
                        break;
                  case 'payments':
                        this.setState({
                              adminView: <PaymentAccrued />
                        });
                        break;
                  case 'charities':
                        this.setState({
                              adminView: `charities managment`,
                        });
                        break;

                  default:
                        this.setState({
                              adminView: `no  managment shown`,
                        });
                        break;
            }


      }



      setAdminViewClass(adminType) {
            console.log(adminType)
            switch (adminType) {
                  case 'users':
                        console.log("in user class set");
                        this.setState({
                              userAdminClass: "glibr-font-purple font-upsize",
                              glibsAdminClass: "glibr-font",
                              groupsAdminClass: "glibr-font",
                              paymentsAdminClass: "glibr-font",
                              charitiesAdminClass: "glibr-font",
                        })
                        break;
                  case 'glibs':
                        this.setState({
                              userAdminClass: "glibr-font",
                              glibsAdminClass: "glibr-font-purple font-upsize",
                              groupsAdminClass: "glibr-font",
                              paymentsAdminClass: "glibr-font",
                              charitiesAdminClass: "glibr-font",
                        })
                        break;
                  case 'groups':
                        this.setState({
                              userAdminClass: "glibr-font",
                              glibsAdminClass: "glibr-font",
                              groupsAdminClass: "glibr-font-purple font-upsize",
                              paymentsAdminClass: "glibr-font",
                              charitiesAdminClass: "glibr-font",
                        })
                        break;
                  case 'payments':
                        this.setState({
                              userAdminClass: "glibr-font",
                              glibsAdminClass: "glibr-font",
                              groupsAdminClass: "glibr-font",
                              paymentsAdminClass: "glibr-font-purple font-upsize",
                              charitiesAdminClass: "glibr-font",
                        })
                        break;
                  case 'charities':
                        this.setState({
                              userAdminClass: "glibr-font",
                              glibsAdminClass: "glibr-font",
                              groupsAdminClass: "glibr-font",
                              paymentsAdminClass: "glibr-font",
                              charitiesAdminClass: "glibr-font-purple font-upsize",
                        })
                        break;

                  default:
                        return null;
            }
      }





      render() {

            // check that user state is set
            if (!this.props.authenticate.userSetStateDone)
            // This value gets set in the header to note when the other redux user state values are set.
            {
                  return (< div > Patience mortal, the little demons of paradise are conjuring the djinns of the singing garden dimension forth, once done they together they will render the page. </div>);

            }
            else {
                  // User redux state set
                  // double protection (we also check in routes.js) for admin role and active state
                  if (this.props.authenticate.currentUserStatus !== "active") {

                        window.location = '/userProblem';
                        return null;

                  } else if (this.props.authenticate.currentUserRole !== 'admin') {

                        window.location = '/';
                        return null;
                  } else {
                        return (


                              <div className="container-green-border">
                                    <div className="text-align-center  div-full-width">
                                          <h3>Administration</h3>


                              <span className={this.state.userAdminClass} onClick={this.loadAdminView.bind(this, 'users')}>users</span> |
                              <span className={this.state.glibsAdminClass} onClick={this.loadAdminView.bind(this, 'glibs')}>glibs</span> |
                              <span className={this.state.groupsAdminClass} onClick={this.loadAdminView.bind(this, 'groups')}>groups</span> |
                              <span className={this.state.paymentsAdminClass} onClick={this.loadAdminView.bind(this, 'payments')}>payments</span> |
                              <span className={this.state.charitiesAdminClass} onClick={this.loadAdminView.bind(this, 'charities')}>charities</span>

                                          <hr className="style-two" />


                                          {this.state.adminView}



                                          <br />

                                    </div>
                              </div>



                        )
                  }
            }
      }

} //end class


const mapStateToProps = function (store) {
      return {
            //glib properties
            glibs: store.glibsState,

            //user properties
            user: store.userState,

            //authenticate properties
            authenticate: store.authState
      };
};

export default connect(mapStateToProps)(Admin);