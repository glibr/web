import React, { Component } from 'react';

import RESTCLIENT from "../../../common/restClient.js";
let RestClient = new RESTCLIENT();
/**
 * Payment List 
 *  Returns a list of glib penny payment lines as a list.
 * 
 *  Can be a lsit based on a handle, charity, or peryaps other attribute TBD
 *  Props: showList |  handle | paymentStatus
 */
export default class PaymentList extends Component {

      constructor(props) {
            super(props);


            this.state = {
                  renderGlibPennyCharges: null
            };

      }

      componentDidMount() {

            this._showPennyChargeList("gonzo");

      }

      _showPennyChargeList(handle) {

            var feedQuery = "";

            feedQuery = "/payments/paymentslist/" + this.props.handle;

            RestClient.fetchGet(feedQuery, (response) => {
                  if (response.success) {
                        this.setState({
                              renderGlibPennyCharges: response.glibChargeLines
                        })


                  } else {
                        // no list
                        this.setState({
                              renderGlibPennyCharges: "no penny lines for this user"
                        })

                  }
            },
                  (err) => {
                        console.log(err);
                  });



      }



      render() {


            var userGibPenniesAccrued = 0;

            if (this.state.renderGlibPennyCharges === null) {
                  return (
                        <div>Stand by, stand by.....</div>
                  )
            } else {

                  if (this.props.showList && (this.props.handle === this.props.showForThisHandle)) {
                   
                        return(

                              <div className="text-align-center  div-full-width">
                             <span className="glibr-font-purple"> list of glibs</span>
                         <table border="1" className="table-center-fix" ><tbody>
                                    <tr className="text-align-center glibr-font-black">
                                       
                                          <td>Bill Status</td>
                                          <td>&cent;&cent;</td>
                                          <td>&cent; accrued</td>
                                          <td>Handle</td>
                                          <td>userId</td>
                                          <td>glibId</td>
                                          <td>glib link</td>
                                          <td>Charity Id</td>
                                          <td>Charity Name</td>
                                    </tr>
                                    {this.state.renderGlibPennyCharges.map(paymentLine => (

                                          <tr key={paymentLine.glibId} className="tiny-text-black">

                                         
                                                <td>{paymentLine.paymentStatus}</td>
                                                <td>{paymentLine.pennies}</td>
                                                <td> {userGibPenniesAccrued += paymentLine.pennies}</td>
                                                <td>{paymentLine.handle}</td>
                                                <td>{paymentLine.userId}</td>
                                                <td>{paymentLine.glibId}</td>
                                                <td className="text-align-center"><a href={"/glib?glibId=" + paymentLine.glibId} target="singleGlibView"><img src="/images/glib-link-icon.png" alt='glib link' /></a></td>
                                                <td>{paymentLine.charityId}</td>
                                                <td>{paymentLine.charityName}</td>
                                          </tr>

                                    ))}
                              </tbody></table>
                              {userGibPenniesAccrued} pennies
                        </div>

                        )


                  } else {



                        return null;
                  }


            }





      }
}