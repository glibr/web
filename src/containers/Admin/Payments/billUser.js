import React, { Component } from 'react';

import GLOBALS from '../../../common/globals.js';
import RESTCLIENT from "../../../common/restClient.js";

let RestClient = new RESTCLIENT();

/**
 * Payment List 
 *  Returns a list of glib penny payment lines as a list.
 * 
 *  Can be a lsit based on a handle, charity, or peryaps other attribute TBD
 *  Props: showList |  handle | paymentStatus
 */
export default class BillUser extends Component {

      constructor(props) {
            super(props);

            this.state = {
                  paidState: false
            }

      }

      _sendBillToCustomer(e) {
            e.preventDefault();

            let billData = {
                  handle: this.props.handle
            };

            var feedQuery = '/payments/chargeuser';

            RestClient.fetchPost(feedQuery, billData, response => {
                  if (response.success) {

                        this.setState({
                              paidState: true
                        });

                  } else {
                         console.log('Something bad happened while trying to bill the user');
                  }
            }, err => {
                  console.log('error in trying to bill user: ' + err);
            });




      }


      render() {

            if (this.props.handle === this.props.showForThisHandle) {
                  //  the bill user form is expanded

                  // check penny total
                  // check current paid state
                  if (this.props.amount > GLOBALS.CONSTANTS.GLIB_BILLING_THRESHOLD && !this.state.paidState) {

                        return (
                              <div>Bill user:

                        <button onClick={this._sendBillToCustomer.bind(this)} className="glib-button" >bill user</button>

                                    <span>{this.props.amount} pennies to bill</span>


                              </div>
                        );

                  } else {
                        return null;
                  }
            } else {
                  return null;
            }

      }
}//end class
