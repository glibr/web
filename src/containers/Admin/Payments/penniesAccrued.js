import React, { Component } from 'react';
import { Fragment } from 'react';

import PaymentList from "./paymentList.js";
import BillUser from "./billUser.js";

import RESTCLIENT from "../../../common/restClient.js";
let RestClient = new RESTCLIENT();

/**
 * Payment List 
 *  Returns a list of glib penny payment lines as a list.
 * 
 *  Can be a lsit based on a handle, charity, or peryaps other attribute TBD
 */
export default class PaymentAccrued extends Component {

      constructor(props) {
            super(props);


            this.state = {
                  renderAccruedPenniesByUser: null,
                  showPaymentList: false,
                  showForThisHandle: null,
                  showBillPaneForThisHandle: null
            };

      }

      componentDidMount() {

            this._getPenniesAccruedByUser();

      }



      //'/payments/userpennyaccruelist/:paymentstate', 
      _getPenniesAccruedByUser() {

            var feedQuery = "";

            feedQuery = "/payments/userpennyaccruelist/new";

            RestClient.fetchGet(feedQuery, (response) => {
                  if (response.success) {
                        this.setState({
                              renderAccruedPenniesByUser: response.userPenniesAccrued
                        })

                  } else {
                        // no list
                        this.setState({
                              renderAccruedPenniesByUser: "no penny lines for this user"
                        })
                  }
            },
                  (err) => {
                        console.log(err);
                  });

      }

      _showPaymentList( showThisHandle){

            this.setState({
                  showPaymentList: true,
                  showForThisHandle: showThisHandle
            })      
      }
      _hidePaymentList( showThisHandle){
    
                this.setState({
                      showPaymentList: false,
                      showForThisHandle: null
                })          
          }
          _showBillPane(showThisHandle){
            this.setState({
                  showBillPaneForThisHandle: showThisHandle
            })          
          }
          _hideBillPane(){
            this.setState({
                  showBillPaneForThisHandle: null
            })          
          }

      render() {
           if (this.state.renderAccruedPenniesByUser === null) {
                  return (
                        <div>Stand by, stand by.....</div>
                  )
            } else {
                  return (
                        <div className="text-align-center">

                     <table border="1" className="table-center-fix container-background-white"><tbody>
                                    <tr className="text-align-center glibr-font-black">
                                          <td>
                                         
                                          </td>
                                          <td>&cent; accrued</td>
                                          <td># of glibs</td>
                                          <td>Handle</td>
                                          <td>userId</td>
                                          <td>Charity Id</td>
                                          <td>Charity Name</td>
                                    </tr>
                                    {this.state.renderAccruedPenniesByUser.map(accruedPennies => (
                                          //      {
                                          //       "_id" : {
                                          //           "handle" : "glibrsmith",
                                          //           "userId" : "5b030211d381584fd05d3b95",
                                          //           "charityName" : "USO",
                                          //           "charityId" : "5bae2ee0f38d4d70cb82fa7b"
                                          //       },
                                          //       "totalAccruedNewPennies" : 312,
                                          //       "count" : 6.0
                                          //   }

                                          <Fragment key={accruedPennies._id.userId}>

                                                <tr>

                                                      <td>
                                                      <button onClick={this._showBillPane.bind(this, accruedPennies._id.handle)}  className="tiny-text"> bill</button>
                                                | 
                                                 <button onClick={this._hideBillPane.bind(this, accruedPennies._id.handle)}  className="tiny-text"> cancel</button>
                                           
                                                

                                                      </td>
                                                      <td> {accruedPennies.totalAccruedNewPennies}</td>
                                                      <td>{accruedPennies.count} 
                                                            <button onClick={this._showPaymentList.bind(this, accruedPennies._id.handle)}  className="tiny-text"> show</button>
                                                             | 
                                                             <button onClick={this._hidePaymentList.bind(this, accruedPennies._id.handle)}  className="tiny-text"> hide</button>                                                            
                                                      </td>
                                                      <td>{accruedPennies._id.handle}</td>
                                                      <td>{accruedPennies._id.userId}</td>
                                                      <td>{accruedPennies._id.charityId}</td>
                                                      <td>{accruedPennies._id.charityName}</td>



                                                </tr>

                                                    <tr >
                                                      <td colSpan="7">
                                                      <BillUser key={accruedPennies._id.userId} handle={accruedPennies._id.handle}  amount={accruedPennies.totalAccruedNewPennies} showForThisHandle={this.state.showBillPaneForThisHandle}  />
                                                      </td>
                                                      </tr>

                                                <tr >
                                                      <td colSpan="7">
                                                      <PaymentList key={accruedPennies._id.userId} showList={this.state.showPaymentList} handle={accruedPennies._id.handle} showForThisHandle={this.state.showForThisHandle}/>
                                                      </td>
                                                      </tr>

                                          </Fragment>

                                    ))}
                              </tbody></table>
                        </div>


                  )

            }

      }
}