import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Fragment } from 'react';

//resources
import './style.css';


//components
import ProfileWidget from '../../components/ProfileWidget';
import ProfileList from '../../containers/Account/ProfileList';
import GlibFeed from '../../containers/GlibFeed/GlibFeed';
import Login from '../../containers/Auth/Login';
import GlibEditControl from '../../containers/Glib/glibEditControl.js';
import GroupsWidget from '../../containers/Groups/groupsWidget.js';

export class UserHome extends Component {
  constructor(props) {
    super(props);

     
    this.state = {
      userFollowList: this.props.user.currentUserFollowList,
      reloadBubble: false,
      usersLinked: false
    }
  }


  shouldComponentUpdate(nextProps, nextState) {

    return nextProps.user.currentUserFollowList !== this.props.user.currentUserFollowList;

  }

  render() {

    if (this.props.authenticate.userLoggedIn) {

      return (

        <div>
          {/* <NavBar /> */}

          <div className="row">

            <div className="col-xs-12 col-sm-3">

              <ProfileWidget             
                currentUserId={this.props.currentUserId}
                profilePicture={this.props.profilePicture}
                handle={this.props.handle}
                firstName={this.props.firstName}
                numberGlibs={this.props.numberGlibs}
                numberFollows={this.props.numberFollows}
                numberFollowers={this.props.numberFollowers}
                numberOfDoubled={this.props.numberOfDoubled}
                userFollowList={this.props.followList}
                userFollowersList={this.props.followersList}
                userDoublesList={this.props.doublesList}
                charityLogoForUser={this.props.charityLogoForUser}
              />
              {this.props.currentUser}

              <GroupsWidget
                currentUser={this.props.currentUser}
                handle={this.props.handle}
              />

            </div>

            <div className="col-xs-12 col-sm-6">

              <div className="container-purple-border">
                <h3> @{this.props.handle} says:</h3>
                <hr className="style-four-purple" />

                <span className="glibr-font-black">{this.props.tagline}</span>

              </div>
              <br />

             {this.renderEditControl()}

              <div className="container-purple-border">
                <span className="glibr-font">{this.props.handle}'s glibs</span>

                <hr className="style-two" />

                <GlibFeed feedType="profileUserFeed" feedReload={this.state.reloadFeedBubble} feedLocation="secondaryFeed" numGlibs="12" thisUserFeedHandle={this.props.handle} />
              </div>




            </div>


            <div className="col-xs-12 col-sm-3">
              {this._renderPMList()}


              <div className="container-purple-border">
                <img src={process.env.PUBLIC_URL + '/images/arrow_joined.png'} alt='arrow joined' />
                <span className="glibr-font font-upsize" > People doubled with {this.props.handle}</span>
                <hr className="style-four-purple" />

                <div className="row">

                  <ProfileList
                    idList={this.props.doublesList}
                    profileListType="doubled"
                    handle={this.props.handle}
                  />

                </div>
              </div>

              <br />

              <div className="container-purple-border">
                <img src={process.env.PUBLIC_URL + '/images/arrow_right.png'} alt='arrow listening' />
                <span className="glibr-font font-upsize" > People {this.props.handle} is following.</span>
                <hr className="style-four-purple" />

                <div className="row">

                  <ProfileList
                    idList={this.props.followList}
                    profileListType="follows"
                    handle={this.props.handle}
                  />

                </div>
              </div>
              <br />
              <div className="container-purple-border">
                <img src={process.env.PUBLIC_URL + '/images/arrow_left.png'} alt='arrow listeners' />
                <span className="glibr-font font-upsize" > People following {this.props.handle}</span>
                <hr className="style-four-purple" />

                <div className="row">

                  <ProfileList
                    idList={this.props.followersList}
                    profileListType="followers"
                    handle={this.props.handle}
                  />

                </div>
              </div>

            </div>
          </div>
        </div>

      );
    } else {
      return (
        <div className="row">
          <div className="col-xs-12">
            <br />
            <Login />
          </div>
        </div>
      );
    }

  }



  renderEditControl(){
    
    if(this.props.usersLinked){
      return(
        <Fragment><div className="container-purple-border"> <span className="glibr-font font-upsize"> Leave a private message for {this.props.handle}</span><GlibEditControl glibPrivate={true} userToPM={this.props.handle} glibPrivateUserId={this.props.currentUserId} reloadFeed={this._reloadGlibFeed.bind(this)} /></div> <br /></Fragment>
      );
    } else {
      return null;
    }
   
  }

  // bubble up a reglib
  _reloadGlibFeed() {
    this.setState({
      reloadFeedBubble: true,
    });
  }


  _renderPMList() {
    let pmListHeadText = `Private Messages with ${this.props.handle}`;
    if (this.props.user.currentUserHandle === this.props.handle) {
      pmListHeadText = 'Your private messages';
    }
    return (
      <Fragment>
        <div className="container-purple-border">
          <span className="glibr-font">{pmListHeadText}</span>
          <hr className="style-two" />
          <GlibFeed feedType="privateMessagesWithUser" feedLocation="secondaryFeed" numGlibs="12" thisUserFeedHandle={this.props.handle} autoRefresh={false} />
        </div>
        <br />
      </Fragment>


    )
  }

}//end class


const mapStateToProps = function (store) {
  return {
    //glib properties
    glibs: store.glibsState,

    //user properties
    user: store.userState,

    //authenticate properties
    authenticate: store.authState
  };
};

//connect is the redux method that connects redux and react
export default connect(mapStateToProps)(UserHome);

