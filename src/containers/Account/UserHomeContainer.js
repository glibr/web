import React, { Component } from 'react';
import { connect } from 'react-redux';
import './style.css';


//components
import UserHome from '../Account/UserHome';
import EnvironmentSettings from "../../common/environmentsettings.js";
import RESTCLIENT from "../../common/restClient.js";

let envSettings = new EnvironmentSettings();
let RestClient = new RESTCLIENT();

/* Entry Point for Public Profile Page
*  We check for who we have?  the current user or a different user.
*  We capture the correct user context and pass it down to userHome
* 
*   UserHomeContainer
*         |
*         |_ UserHome
*                |
*                |_ProfileList - Several distinct instances of this control - builds the appropriate list of profiles
*                          |
*                          |_ProfileWidgetSmall
*/
class UserHomeContainer extends Component {
  constructor(props) {
    super(props);

    this.state = {
      userFound: false,
      currentUser: false,
      usersLinked: false,
      handle: '',
      user: null,
      userLinkCheckDone: false,
    };

    this.handle = this.props.location.pathname.replace('/', '');

  }//end constructor




  async  componentDidMount() {
   
    const loggedInHandle = localStorage.getItem('currentUserHandle');
    const pageHandle = this.handle;    

    // Set user booleans
    // The passed in handle matches the current user
    if (this.props.user.currentUserHandle === this.handle) {
      this.setState({
        currentUser: true,
        userLinkCheckDone: true,
      });
      await this.loadCurrentUser(loggedInHandle);

    } else {
      await this.loadCurrentUser(pageHandle);
      //check for userlinks
      await this.checkForUserLink(loggedInHandle, pageHandle);
    }

    




  }//end ComponentDidMount


  async loadCurrentUser(handle){

    var mQuery = '/getuserbyhandleverbose/' + handle;
     //   RestClient.fetchGet('/getcurrentuser', (response) => {
    //look for a user with the handle    
    RestClient.fetchGet(mQuery, (response) => {
      if (response.success) {
        // We found someone at that handle       

        this.setState({
          userFound: true,
          user: response.user
        });      

      } else {
        // user does not exist, so redirect to 404 page
        window.location = '/404';
      }
    }, err => {
      console.log(err);
    });


  }


  async  checkForUserLink(thisHandle, thatHandle) {
    
    // userRouter.get('/checkuseridlinked/:thisid/:thatid'
    var mQuery = `/userlinks/checkhandleslinked/${thisHandle}/${thatHandle}`;

    RestClient.fetchGet(mQuery, (response) => {
      if (response.success) {
        // We found a link    

        this.setState({
          usersLinked: true,
          userLinkCheckDone: true
        });

      } else {
        this.setState({
          userLinkCheckDone: true
        });
      }
    }, err => {
      console.log(err);
      this.setState({
        userLinkCheckDone: true
      });
    });

  }



  render() {

    // we should be with a valid user
    // check that user state is set
    if (this.state.userFound && this.props.authenticate.userSetStateDone && this.state.userLinkCheckDone) {

      // good branch
      //load user from handle passed in querystring
      return (

        // Populate UserHome with data from response
        <UserHome         
          currentUserId={this.state.user._id}
          usersLinked={this.state.usersLinked}
          handle={this.state.user.handle}
          firstName={this.state.user.firstName}
          profilePicture={envSettings.GLIBR_IMAGE_ROOT_URI() + this.state.user.profilePicture}
          tagline={this.state.user.tagline}
          numberGlibs={this.state.user.numberOfGlibs}
          numberFollows={this.state.user.followCount}
          numberFollowers={this.state.user.followerCount}
          numberOfDoubled={this.state.user.doubleFollowCount}
          followList={this.state.user.follows}
          followersList={this.state.user.followers}
          doublesList={this.state.user.doubles}
          charityLogoForUser={this.state.user.userCharityLogo}

        />

      );

    } else {

      return (
        <div>patience grasshopper
        true or false:         {this.state.userFound}
        </div>);

    }
  }
}//close the class



const mapStateToProps = function (store) {
  return {
    //glib properties
    glibs: store.glibsState,

    //user properties
    user: store.userState,

    //authenticate properties
    authenticate: store.authState
  };
};


export default connect(mapStateToProps)(UserHomeContainer);
