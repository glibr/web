import React, { Component} from 'react';
import GLOBALS from "../../common/globals.js";
import EnvironmentSettings from "../../common/environmentsettings.js";
import ProfileWidgetSmall from '../../components/ProfileWidgetSmall';

let envSettings = new EnvironmentSettings();

export default class ProfileList extends Component {
  constructor(props) {
    super(props);

    this.state = {
      followListRendered: null,
    };

  } //end constructor


  async componentDidMount() {

    await this.renderFollowsProfileSmall();
  } //end componentDidMount    


  render() {

    if (this.state.followListRendered) {
      return ( <div className = "boxFluid" > {this.state.followListRendered} </div>
      );
    }

    return ( < span > . </span>);
    }
    // end render

    // get the list of ids for this profile list
    async getFollowsIdList() {
      try {
        const settings = {
          credentials: 'include',
          method: 'GET',
        };
        const apikey = `?apikey=${GLOBALS.APIKEY}`
        // /user/getFollows/:handle/:followtype'
        const response = await fetch(`${envSettings.GLIB_API_URI()}/user/getfollows/${this.props.handle}/${this.props.profileListType}${apikey}`, settings);

        if (response.status === 200) {
          const responseJSON = await response.json();

          if (responseJSON.success) {
           return{
              success: true,
              usersIdMap: responseJSON.userlink,
            }
          } else {
          return{
              success: false,
            }
          }
        }
      } catch (err) {
        console.log(err);
     return{
          success: false,
        }
      }
    }

    // render the profile lists to the state
    async renderFollowsProfileSmall() {

      try{

        const followsResponse = await this.getFollowsIdList();

        if (followsResponse.success && followsResponse.usersIdMap!=='empty') {
          const settings = {
            credentials: 'include',
            method: 'GET',
          };

          const apikey = `?apikey=${GLOBALS.APIKEY}`
          let mongoQ = await `${envSettings.GLIB_API_URI()}/getusersbyidlist/${followsResponse.usersIdMap}${apikey}`;
        
          // /user/getFollows/:handle/:followtype'
          const response =await fetch(mongoQ, settings);

          if(response.status === 200){
            const responseJSON = await response.json();
  
              let fListRendered = [];
              let iter = 1;
    
              responseJSON.userList.forEach(user => {
                fListRendered.push( <ProfileWidgetSmall 
                  profileID = {user._id}
                  profilePicture = {user.profilePicture}
                  firstName = {user.firstName}
                  lastName = {user.lastName}
                  handle = {user.handle}
                  emailAddress = {user.emailAddress}
                  tagline = {user.tagline }
                  follows = {user.followCount}
                  followers = {user.followerCount}
                  doubledFollows = {user.doubleFollowCount}
                  numberOfGlibs = {user.numberOfGlibs }
                  key = {iter}
                  />
                );
                iter++;
              });
    
              this.setState({
                followListRendered: fListRendered
              });

          }      
  
        }
        
      } catch (error) {

        console.log(error);
        
      }
   
    }


  } //end class

