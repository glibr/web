import React, { Component } from 'react';
import { connect } from 'react-redux';
import { RIEInput } from 'riek';
import EnvironmentSettings from "../../common/environmentsettings.js";
import Utilities from "../../common/utils.js";
import RESTCLIENT from "../../common/restClient.js";
import GLOBALS from "../../common/globals.js";
import HelperMethods from "../../common/helpers.js";
import UploadImageControl from '../../components/UploadButton/';

import './style.css';

// Import Actions
import {
  setCurrentUserProfilePicture
} from '../../actions/userActions';

// Short hand for mapDispatch
// removes the need for the mapdispatch method and the long list of setters
// "actionCreators" is passed as the second parameter in the connect call - see bottom of this module
const actionCreators = {
  setCurrentUserProfilePicture
};

let envSettings = new EnvironmentSettings();
let RestClient = new RESTCLIENT();

export class Profile extends Component {
  constructor(props) {
    super(props);

    this._uploadProfilePicture = this._uploadProfilePicture.bind(this);
    this.uploadButtonResponse = this.uploadButtonResponse.bind(this);

    this.state = {
      validationMessage: [],
      isAuthenticated: false,
      currentUser: null,
      profilePictureSrcUrl: '/images/logo_NO_profilePic.png',
      profileEditError: null,
      tempImageSource: null,
    }

    RestClient.fetchGet('/getcurrentuser', (response) => {
      if (response.success) {

        //set avatar
        if (response.currentUser.profilePicture) {
          this.setState({
            profilePictureSrcUrl: envSettings.GLIBR_IMAGE_ROOT_URI() + response.currentUser.profilePicture
          })
        }

        if (response.currentUser.address_line1 !== 'undefined') {
          this.setState({ address_line1: response.currentUser.address_line1 })
        } else {
          this.setState({ address_line1: "---------------" })
        }

        if (response.currentUser.address_line2 !== 'undefined') {
          this.setState({ address_line2: response.currentUser.address_line2 })
        } else {
          this.setState({ address_line2: "---------------" })
        }

        if (response.currentUser.city !== 'undefined') {
          this.setState({ city: response.currentUser.city })
        } else {
          this.setState({ city: "---------------" })
        }

        if (response.currentUser.state !== 'undefined') {
          this.setState({ state: response.currentUser.state })
        } else {
          this.setState({ state: "---------------" })
        }

        if (response.currentUser.zipcode !== 'undefined') {
          this.setState({ zipcode: response.currentUser.zipcode })
        } else {
          this.setState({ zipcode: "---------------" })
        }

        if (response.currentUser.country !== 'undefined') {
          this.setState({ country: response.currentUser.country })
        } else {
          this.setState({ country: "---------------" })
        }

        this.setState({
          isAuthenticated: response.isAuthenticated,
          currentUser: response.currentUser,
          tagline: response.currentUser.tagline,
          firstName: response.currentUser.firstName,
          lastName: response.currentUser.lastName,
          phone: response.currentUser.phone,


        });
      } else {

        window.location = '/login';
      }
    },
      (err) => {
        console.log(err);
      });


    
  }


  //method injected into UploadImageControl
async  uploadButtonResponse(response) {

    if(response.success){

      const resSaveFileName = await HelperMethods.saveFileNameToDB('profile', response.fileName, this.props.user.currentUserHandle);
      if (resSaveFileName.success) {
        this.setState({
          tempImageSource: response.tempFIleSource,
          glibPicture: response.fileName,
          profilePictureSrcUrl: envSettings.GLIBR_IMAGE_ROOT_URI() + response.fileName,
          profilePictureFileName:  response.fileName,
        });
        this.props.setCurrentUserProfilePicture(envSettings.GLIBR_IMAGE_ROOT_URI() + response.fileName);
      }
    }   
  }


  


  updateProfileField(data) {
    let userData = { address: {} },
      validationMessages = [],
      key = [];

    // loop on array of values coming in data param
    // should be on item in our mthodology because
    // we catch one change to one field
    for (key in data) {

      // match on 'tagline'
      if (key === 'tagline') {
        if (!Utilities.validation.isTextLessThan55Characters(data[key])) {
          validationMessages.push(Utilities.validation.messages.TAGLINE_LONG55);
        } else {
          //push into userData in preparation for the update
          userData.tagline = data[key];
        }
      }//end tagline check

      // First Name
      else if (key === 'firstName') {
        if (!Utilities.validation.isFirstName(data[key])) {
          validationMessages.push(Utilities.validation.messages.FIRSTNAME_LEN50);
        } else {
          userData.firstName = data[key];
        }
      }
      // Last Name
      else if (key === 'lastName') {
        if (!Utilities.validation.isLastName(data[key])) {
          validationMessages.push(Utilities.validation.messages.LASTNAME_LEN50);
        } else {
          userData.lastName = data[key];
        }
      }
      // Phone
      else if (key === 'phone') {
        if (!Utilities.validation.validPhone(data[key])) {
          validationMessages.push(Utilities.validation.messages.INVALID_PHONE);
        } else {
          userData.phone = data[key];
        }
        // Address line 1
      } else if (key === 'address_line1') {
        userData.address_line1 = data[key];
      }
      // Address Line 2
      else if (key === 'address_line2') {
        console.log(key);
        userData.address_line2 = data[key];
      }
      // City 
      else if (key === 'city') {
        console.log(key);
        userData.city = data[key];
      }
      // State 
      else if (key === 'state') {
        console.log(key);
        userData.state = data[key];
      }
      // Zipcode 
      else if (key === 'zipcode') {
        console.log(key);
        userData.zipcode = data[key];
      }
      // Country 
      else if (key === 'country') {
        console.log(key);
        userData.country = data[key];
      }

    }//end key check


    // Execute update against mongo db.
    RestClient.fetchPost('/updateuserprofile', userData, response => {
      console.log(response);
    }, err => {
      console.log(err);
    });


  }




  /* ************************************
   * Upload a profiile picture
  **************************************/
  async  _uploadProfilePicture(e) {

    this.setState({
      profileEditError: null
    })

    try {
      // upload profile picture.
      const pictureUploadResponse = await HelperMethods.uploadPicture(e);

      if (pictureUploadResponse.success) {

        // save profile picture URL path to db
        let collectionType = 'profile';
        const resSaveFileName = await HelperMethods.saveFileNameToDB(collectionType, pictureUploadResponse.picture, this.props.user.currentUserHandle);

        if (resSaveFileName.success) {
          this.setState({
            profilePictureSrcUrl: pictureUploadResponse.pictureFullPath,
            profilePictureFileName: pictureUploadResponse.picture,
          });
          this.props.setCurrentUserProfilePicture(envSettings.GLIBR_IMAGE_ROOT_URI() + pictureUploadResponse.picture);
        } else {
          this.setState({
            profileEditError: GLOBALS.STRINGS.IMAGE_UPLOAD_ERROR,
          });

        }

      } else {
        this.setState({
          profileEditError: pictureUploadResponse.message
        });
      }
    } catch (err) {
      // error 
      this.setState({
        profileEditError: GLOBALS.STRINGS.IMAGE_UPLOAD_ERROR + " " + err,
      })
    }
  }




  _displayValidationMessages() {
    let msgStr = '';
    this.state.validationMessage.forEach(msg => {
      msgStr += msg;
    });

    // we have an error, show the error message
    return (<span className="response-container-error">{msgStr}</span>);
  }



  render() {
    return (
      <div className="container">
        {this._renderProfile()}
      </div>
    );
  }


  _renderProfile() {


    // check that user state is set
    if (this.state.currentUser === null || this.state.currentUser.handle === 'undefined' || this.state.currentUser.handle === null)
    // This value gets set in the Header.js file to note when the other redux user state values are set.
    {
      return (<div>patience, the squirrels are working as fast as they can.</div>);

    } else {
     if (this.props.authenticate.currentUserStatus !== "active" && this.props.authenticate.currentUserStatus !== 'active-provisional') {

        window.location = '/userProblem';

      } else {

        let errorMessageClassName = this.state.profileEditError ? 'profile-error' : 'profile-error hide';

        return (
          <div>
            <div className="row">

            <div className="col-xs-12 col-sm-3 text-align-center">

              

                <div className="profile-upload-circle-cropper">
                    <div className="profile-upload-picture-inner" >
                      <img alt="User profile" src={this.state.profilePictureSrcUrl}   className="profile-upload-picture" />
                    </div>
                  </div>

                <br />

                <UploadImageControl   uploadFileResponse={this.uploadButtonResponse}   />
                    <span className={errorMessageClassName}>{this.state.profileEditError}</span>
              
              <br />
              
              </div>







              
            <div className="col-xs-12 col-sm-9">
                <form className="profile-form">
                  <div className="container-green-border text-align-center">


                    <span className="response-container">{this._displayValidationMessages()}</span>


                    <h1 className="text-align-center handle">@{this.state.currentUser.handle}</h1>

                    <RIEInput
                      value={this.state.tagline}
                      change={this.updateProfileField}
                      propName='tagline'
                    />


                  </div>
                  <br />

                  <div className="container-green-border">
                    <h4 className="text-align-center">Who is @{this.state.currentUser.handle}?</h4>
                    <hr className="style-two" />

                    <div className="text-align-center tiny-text">just click on the thing you want to edit</div>

                    <div className="form-group row">
                      <div className="col-3 text-right" >
                        <h4 className="push-vAlign-down10">email:</h4>
                      </div>
                      <div className="col-9">
                        <span className="form-inActive-text email">{this.state.currentUser.emailAddress}</span>
                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col-3 text-right" >
                        <h4 className="push-vAlign-down10">first name:</h4>
                      </div>
                      <div className="col-9">

                        <RIEInput
                          value={this.state.firstName || '----------'}
                          change={this.updateProfileField}
                          propName="firstName"
                        />

                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col-3 text-right" >
                        <h4 className="push-vAlign-down10">last name:</h4>
                      </div>
                      <div className="col-9">

                        <RIEInput
                          value={this.state.lastName || '----------'}
                          change={this.updateProfileField}
                          propName="lastName"
                        />

                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col-3 text-right">
                        <h4 className="push-vAlign-down10">phone:</h4>
                      </div>
                      <div className="col-9">

                        <RIEInput
                          value={this.state.phone || '----------'}
                          change={this.updateProfileField}
                          propName="phone"
                        />

                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col-3 text-right">
                        <h4 >reset password:</h4>
                      </div>
                      <div className="col-9">
                        <a href="/change-password" id="changePasswordLink" className="">click to change your password</a>
                      </div>
                    </div>

                  </div>

                  <br />

                  <div className="container-green-border">
                    <h4 className="text-align-center">Where is @{this.state.currentUser.handle}?</h4>
                    <hr className="style-two" />

                    <div className="text-align-center tiny-text">just click on the thing you want to edit</div>

                    <div className="form-group row">
                      <div className="col-3 text-right">
                        <h4 className="push-vAlign-down10">address line 1:</h4>
                      </div>
                      <div className="col-9">

                        <RIEInput
                          value={this.state.address_line1 || '----------'}
                          change={this.updateProfileField}
                          propName="address_line1"
                        />


                        {/* <InlineEdit
                         className="form-inActive-text address-line-1"
                         activeClassName="form-control address-line-1"
                         text={this.state.currentUser.address.address_line1 || '----------'}
                         paramName="addressLine1"
                         change={this.dataChanged}
                       /> */}
                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col-3 text-right">
                        <h4 className="push-vAlign-down10">address line 2:</h4>
                      </div>
                      <div className="col-9">

                        <RIEInput
                          value={this.state.address_line2 || '----------'}
                          change={this.updateProfileField}
                          propName="address_line2"
                        />

                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col-3 text-right">
                        <h4 className="push-vAlign-down10">city:</h4>
                      </div>
                      <div className="col-9">

                        <RIEInput
                          value={this.state.city || '----------'}
                          change={this.updateProfileField}
                          propName="city"
                        />

                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col-3 text-right">
                        <h4 className="push-vAlign-down10">state:</h4>
                      </div>
                      <div className="col-9">

                        <RIEInput
                          value={this.state.state || '----------'}
                          change={this.updateProfileField}
                          propName="state"
                        />

                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col-3 text-right">
                        <h4 className="push-vAlign-down10">zip code:</h4>
                      </div>
                      <div className="col-9">

                        <RIEInput
                          value={this.state.zipcode || '----------'}
                          change={this.updateProfileField}
                          propName="zipcode"
                        />

                      </div>
                    </div>

                    <div className="form-group row">
                      <div className="col-3 text-right">
                        <h4 className="push-vAlign-down10">country:</h4>
                      </div>
                      <div className="col-9">

                        <RIEInput
                          value={this.state.country || '----------'}
                          change={this.updateProfileField}
                          propName="country"
                        />

                      </div>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        );
      }
    }
  }
}// end class


//Map the properties of the page to store items
const mapStateToProps = (store) => {
  return {
    //glib properties
    glibs: store.glibsState,

    //user properties
    user: store.userState,

    //authenticate properties
    authenticate: store.authState
  };
};


//connect is the redux method that connects redux and react
export default connect(mapStateToProps, actionCreators)(Profile);