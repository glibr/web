import React, { Component, Fragment } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import './style.css';
import Utilities from "../../common/utils.js";
import RESTCLIENT from "../../common/restClient.js";
import GLOBALS from "../../common/globals.js";

// Controls
import Charities from "../../containers/Charities/charities.js"
import PasswordControl from "./passwordcontrol.js"
import RegistrationPayment from "./RegistrationPayment"
import RegistrationProvisionalToActive from './RegistrationProvisionalToActive.js'

// Actions
// Import Actions
import {
  setCurrentUserCharity,
  setCurrentUserHandle
} from '../../actions/userActions';


// Short hand for mapDispatch
// removes the need for the mapdispatch method and the long list of setters
// "actionCreators" is passed as the second parameter in the connect call - see bottom of this module
const actionCreators = {
  setCurrentUserCharity,
  setCurrentUserHandle
};


let RestClient = new RESTCLIENT();


// >>>>>

// Solution for the switch case of our registration pages.
// Beautiful - >> Strategy Pattern

// https://glcheetham.name/2016/05/20/killing-switch-statements-in-react-with-the-strategy-pattern/

// >>>>>>
export class Registration extends Component {
  constructor(props) {
    super(props);

    this.state = {
      validationMessage: [],
      userRegistrationState: "new",
      // stepOneComplete: false,
      charityChosen: '',
      stripeCardToken: null,
      stripeTokenReceived: false,
      stripeCardTokenFingerprint: null,
      stripeFingerprintReceived: false,
      emailAddress: "",
      firstName: "",
      lastName: "",
      currentUser: null,
      isAuthenticated: false,
      stripeKey: null,
      isUserActivated: false,
    };


    this._updateUserRegistrationState = this._updateUserRegistrationState.bind(this);




  } // end constructor


  componentDidMount() {
    RestClient.fetchGet('/getcurrentuser', (response) => {
      if (response.success) {

        this.setState({
          isAuthenticated: true,
          currentUser: response.currentUser,
          userRegistrationState: response.currentUser.userStatusName,
        })
      }
    });

    // Call into the API layer to retrieve the stripe public key so that it is stored in only one place.
    RestClient.fetchGet('/payments/getStripePublicKey/' + GLOBALS.ENVIRONMENT, (response) => {
      if (response.success) {
        this.setState({ stripeKey: response.stripeKey })
      }
    });


    //http://api.glibr:5000/activateaccount?token=LTx63gyxwOXvkuaX4FUSSBrUlPJce9kdDtiyBVikohiFOfjVEra5EbxQCwP9RahZ6GObesZrV9JAbNSd5kS93Q%3D%3D
    var urlParams = new URLSearchParams(window.location.search);

    if (urlParams.has("activate")) {

      if (urlParams.has("token")) {

        RestClient.fetchGet("/register/activate/" + encodeURIComponent(urlParams.get("token")), (response) => {

          if (response.success) {
            this._updateUserRegistrationState("active");
          } else {
            // todo - build out the activation failure in userproblem
            window.location("/userproblem?activation=failed&msg=" + response.message);
          }
        });

      } else {
        this.setState({
          userRegistrationState: "not-activated"
        })

      }

    } // end activation email respons handling



  }





  render() {
    if (this.state.userRegistrationState === "new") {
      // registration step one

      return (
        <div className="register-container container">
          <div className="container-green-border">
            <h1 className="text-align-center">Join glibr today.</h1>
            <p className="text-align-center  glibr-tagline"><b><span className="glibr-font">glibr</span> is the goodest. <span className="glibr-font cursor-hand" onClick={this._toggleElement.bind(this, 'el_goodest_message')}>more...</span></b></p>

            <div id="el_goodest_message" className="hide">
              <p><span className="glibr-font">glibr</span> is a system built on your pennies. There&apos; no marketing.  There&apos; no machine learning or AI or scraping of your data.
  We don't collect your data, we don't sell it, we don't <i>use you</i> at all.   Just your posts and the posts of other glibr users.   The glibr system will never need to jam any crappy advertising into your
  feed.  You only see what you want to see, never anything else.</p>

              <p>Each little two penny post contributes to the <span className="glibr-font">glibr</span> system and keeps the whole machine running smoothly.  And <b>FREE</b> of corporate data gathering.</p>
            </div>
          </div>


          <br />

          <div className="container-green-border">
            <form className="join-glibr">
              <div className="form-group row">
                <div className="regLabelDisplay col-md-3 hidden-xs" >
                  <h5 className="push-vAlign-down10">handle:</h5>
                </div>
                <div className="col-md-9 col-xs-12">
                  <input type="text" className="form-control" id="handle" placeholder="your glibr handle" />
                </div>
              </div>


              <div className="form-group row">
                <div className="regLabelDisplay col-md-3 hidden-xs " >
                  <h5 className="push-vAlign-down10">email:</h5>
                </div>
                <div className="col-md-9 col-xs-12">
                  <input type="text" className="form-control " id="email" placeholder="email" />
                </div>
              </div>

              <div className="form-group row">
                <div className="regLabelDisplay col-md-3 hidden-xs " >
                  <h5 className="push-vAlign-down10">password:</h5>
                </div>
                <div className="col-md-9 col-xs-12">
                  <PasswordControl passwordFieldType="registration" />
                </div>
              </div>

              <div className="form-group row">
                <div className="col-12 regButtonDisplay">
                  <button onClick={this._registerUser.bind(this)} className="btn btn-primary" >next step: choose a charity</button>
                </div>
              </div>

              <div className="form-group row">
                <div className="col-12">
                  <span className="response-container">{this._displayValidationMessages()}</span>
                </div>
              </div>

              <div className="form-group row">
                <div className="col-12 text-center">
                  <small>By signing up, you agree to the <Link to="/tos">Terms of Service</Link> and <Link to="/privacy">Privacy Policy</Link>. Others will be able to find you by email when provided.</small>
                </div>
              </div>
            </form>
          </div>
        </div>
      );


    } else if (this.state.userRegistrationState === "registration-two-charity") {
      // registration step two - charity
      return (
        <div className="register-container container">

          <div className="container-green-border">
            <h1 className="text-align-center">Join glibr today.</h1>
            <p className="text-align-center"><b>Post for Good! <span className="glibr-font cursor-hand" onClick={this._toggleElement.bind(this, 'el_good')}>more...</span></b></p>

              <div id="el_good" className="hide">
                <p>When you sign up, <span className="glibr-font">glibr</span> partners you with a charity of your choice and a portion of all your (and everyone else&apos;s) pennies from your posts will be donated to them.
                  Your social posts will go in part to a good cause!</p>
                <p><span className="glibr-font">glibr</span> will donate 20% of our profits to the charity of your choice, based on your penny allocation.  The more you glib, the higher penny amount you put on your glibs, the more your charity will
                  receive.</p>
              </div>
          </div>        

          <br />

          <div className="container-green-border">

          <div className="form-group row">
              <div className="col-12 regButtonDisplay">
                <button onClick={this._registerCharity.bind(this)} className="btn btn-primary" disabled={!this.state.charityChosen} >next step: payment</button>
              </div>
            </div>


            <Charities
              enableCharityButton={this._enableCharityButton.bind(this)}
            />

            <hr className="style-two" />

            <div className="form-group row">
              <div className="col-12 regButtonDisplay">
                <button onClick={this._registerCharity.bind(this)} className="btn btn-primary" disabled={!this.state.charityChosen} >next step: payment</button>
              </div>
            </div>

          </div>

        </div>

      );
      // replace new with  registration-three-payment
      // check to see if we have a stripekey returned.
    } else  if (this.state.userRegistrationState === "registration-three-payment" && this.state.stripeKey !== null) {

      return (

        <Fragment>
          <RegistrationPayment
            emailAddress={this.state.emailAddress}
            handle={this.props.user.currentUserHandle}
            updateUserRegistrationState={this._updateUserRegistrationState}
          />
        </Fragment>

      );

      // final credit card capture for Expired Provisional Users
    } else if (this.state.userRegistrationState === "provisional-expired" && this.state.stripeKey !== null) {

      return (

        <Fragment>
          <RegistrationProvisionalToActive
            firstName={this.props.user.currentUserFirstName}
            lastName={this.props.user.currentUserLastName}
            emailAddress={this.state.emailAddress}
            handle={this.props.user.currentUserHandle}
            updateUserRegistrationState={this._updateUserRegistrationState}
          />
        </Fragment>

      );

    } else if (this.state.userRegistrationState === "registered-inactive" || this.state.userRegistrationState === "registered-provisional-inactive") {
      // registration step four - activation email
      return (


        <div className="register-container container">

          <div className="register-container container">
            <div className="container-green-border">
              <h1 className="text-align-center">Join glibr today.</h1>
              <h5 className="text-align-center">{GLOBALS.STRINGS.GLIBR_TAGLINE}</h5>
            </div>
          </div>

          <br />

          <div className="container-green-border">
            <p className="text-align-center">
              You should be getting an email from the glibr team with an activation email.  Click on the link in that email to complete your registration.
  </p>

            <p className="text-align-center">
              <b> *Note:</b> please check your junk mail or spam folder in your email just in case our message  happens to get dropped in there.
  </p>


            <hr className="style-two" />

            <table width="100%">
              <tbody>
                <tr>
                  <td>
                    <span className="glibr-font-black">Thanks</span>
                  </td>
                  <td className="text-align-right">
                    <Link to="/login">login</Link>
                  </td>
                </tr>
              </tbody>
            </table>


          </div>

        </div>

      );


      // active
    } else if (this.state.userRegistrationState === "active") {
      return (


        <div className="register-container container">

          <div className="register-container container">
            <div className="container-green-border">
              <h1 className="text-align-center">Join glibr today.</h1>
              <h5 className="text-align-center">{GLOBALS.STRINGS.GLIBR_TAGLINE}</h5>
            </div>
          </div>

          <br />

          <div className="container-green-border">
            <p className="text-align-center">
              You're account is activated and ready to go!   Get to glibbin!
              </p>

            <hr className="style-two" />

            <table width="100%">
              <tbody>
                <tr>
                  <td>
                    <span className="glibr-font-black">Thanks</span>
                  </td>
                  <td className="text-align-right">
                    <Link to='/'>login</Link>
                  </td>
                </tr>
              </tbody>
            </table>


          </div>

        </div>

      );



    } else {

      return (

        <div className="register-container container">

          <div className="register-container container">
            <div className="container-green-border">
              <h1 className="text-align-center">Join glibr today.</h1>
              <h5 className="text-align-center">{GLOBALS.STRINGS.GLIBR_TAGLINE}</h5>
            </div>
          </div>

          <br />

          <div className="container-green-border">
            <p className="text-align-center">
              Something ain't kosher in Kansas...  Please contact glibr.help@gmail.com for assistance.
              {this.state.userRegistrationState}
            </p>




          </div>


        </div>
      );

    }

  }

  _toggleElement(el) {
    let oEl = document.getElementById(el);

    if (typeof (oEl) !== 'undefined' && oEl !== null) {

      if (oEl.className === "show") {
        oEl.classList.remove('show');
        oEl.classList.add('hide');
      } else if (oEl.className === "hide") {
        oEl.classList.remove('hide');
        oEl.classList.add('show');
      }
    }
  }

  _enableCharityButton(charity) {

    this.setState({
      charityChosen: charity
    })


  }



  _displayValidationMessages() {
    let msgStr = '';
    this.state.validationMessage.forEach(msg => {
      msgStr += msg;
    });

    if (this.state.errorMsg) {
      //we have an error, show the error message
      return (<span className="response-container-error">{msgStr}</span>);
    } else if (this.state.successMsg) {
      return (<span className="response-container-success">{msgStr}</span>);
    } else {
      //      we have no message return nothing - they are both false
      return (null);
    }

  }



  /**
   *  Step 1 - Initial account information.
   *  handle | email | phone
   */
  _registerUser(e) {

    e.preventDefault();

    let validationMessages = [];

    let userData = {
      handle: document.getElementById('handle').value,
      emailAddress: document.getElementById('email').value.toString().toLowerCase().trim(),
      password: document.getElementById('password').value
    };


    if (!userData.emailAddress || !userData.handle || !userData.password) {
      validationMessages.push(Utilities.validation.messages.MISSING_FIELDS);
    } else if (!Utilities.validation.validHandle(userData.handle)) {
      validationMessages.push(Utilities.validation.messages.INVALID_HANDLE);
    } else if (!Utilities.validation.validHandleLength(userData.handle)) {
      validationMessages.push(Utilities.validation.messages.INVALID_HANDLE_LENGTH);
    } else if (!Utilities.validation.validEmail(userData.emailAddress)) {
      validationMessages.push(Utilities.validation.messages.INVALID_EMAIL);
    } else if (Utilities.validation.weakPassword(userData.password)) {
      validationMessages.push(Utilities.validation.messages.IS_WEAK_PASSWORD);
    }

    if (validationMessages.length === 0) {

      this.setState({
        emailAddress: userData.emailAddress
      })

      RestClient.fetchPost('/register', userData, response => {
        if (response.success) {

          //validationMessages.push('Success! We\'ve sent you an email to the address you provided. Please confirm your account by clicking the link in the mail. Thanks!');

          this.props.setCurrentUserHandle(response.handle);

          this.setState({
            validationMessage: validationMessages,
            userRegistrationState: "registration-two-charity",
            errorMsg: false,
            successMsg: true
          });

        } else {
          validationMessages.push(response.message);

          this.setState({
            validationMessage: validationMessages,
            errorMsg: true
          });
        }
      },
        (err) => {
          console.log(err);
          validationMessages.push("error: " + err);

          this.setState({
            // message for non-unique card.        
            validationMessage: validationMessages,
            errorMsg: true,
            successMsg: false
          })


        });
    } else {
      this.setState({
        validationMessage: validationMessages,
        errorMsg: true
      });
    }
  }


  /**
   *  Step 2 - Register Charity
   *  This is where the user sets their charity.
   */
  _registerCharity(e) {

    e.preventDefault();
    let validationMessages = [];

    let userData = {
      handle: this.props.user.currentUserHandle

    };

    RestClient.fetchPost('/registercharity', userData, response => {
      if (response.success) {

        this.setState({
          userRegistrationState: "registration-three-payment"
        });

      } else {
        validationMessages.push(response.message);

        this.setState({
          // message for non-unique card.        
          validationMessage: validationMessages,
          errorMsg: true,
          successMsg: false
        })
      }
    }, (err) => {
      console.log(err);
    });

  }



  _updateUserRegistrationState(userRegistrationState) {
    console.log('got the response: ' + userRegistrationState);

    this.setState({
      userRegistrationState: userRegistrationState,
    });
  }


} // end class

//Map the properties of the page to store items
const mapStateToProps = (store) => {
  return {
    //glib properties
    glibs: store.glibsState,

    //user properties
    user: store.userState,

    //authenticate properties
    authenticate: store.authState
  };
};


//connect is the redux method that connects redux and react
export default connect(mapStateToProps, actionCreators)(Registration);