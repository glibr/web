
import React, { Component } from 'react';
// Dumb name for a password strength checker library, but there you go
import './style.css';
import { Fragment } from 'react';

var zxcvbn = require('zxcvbn');

class PasswordControl extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: 'password',
      score: 'null'
    }
    this.showHide = this.showHide.bind(this);
    this.passwordStrength = this.passwordStrength.bind(this);
  }

  showHide(e) {
    e.preventDefault();
    e.stopPropagation();
    this.setState({
      type: this.state.type === 'input' ? 'password' : 'input'
    })
  }

  passwordStrength(e) {
    if (e.target.value === '') {
      this.setState({
        score: 'null'
      })
    }
    else {
      var pw = zxcvbn(e.target.value);
      this.setState({
        score: pw.score
      });
    }

  }

  render() {
    return (
      <Fragment>
        <input type={this.state.type} className="form-control" id="password" onChange={this.passwordStrength}  placeholder="password" />
        <span className="password__show" onClick={this.showHide}>{this.state.type === 'input' ? 'Hide' : 'Show'}</span>
        
        {this._passwordStrength()}
      </Fragment>
    )
  }

_passwordStrength(){
  if(this.props.passwordFieldType==="registration"){
    return(
      <span className="password__strength" data-score={this.state.score} />
    )
  }
}


}



//connect is the redux method that connects redux and react
export default (PasswordControl);
