
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Elements, StripeProvider } from 'react-stripe-elements';
import Helper from "../../common/helpers.js";

import './style.css';
import RESTCLIENT from "../../common/restClient.js";
import GLOBALS from "../../common/globals.js";

// Controls
import StripeUserSignUp from "./stripeUserSignUp"

let RestClient = new RESTCLIENT();

export class RegistrationPayment extends Component {
  constructor(props) {
    super(props);

    this.state = {
      currentUser: null,
      isAuthenticated: false,
      isUserActivated: false,

      firstName: "",
      lastName: "",
      emailAddress: "",

      validationMsg: [],
      userRegistrationState: "provisional-expired",
      charityChosen: '',
      stripeKey: null,
      stripeCardToken: null,
      stripeTokenReceived: false,
      stripeCardTokenFingerprint: null,
      stripeFingerprintReceived: false,

    };


    this.handleInputChange = this.handleInputChange.bind(this);  
    this.handleSubmitComplete = this.handleSubmitComplete.bind(this);
    this.setErrorMessageList = this.setErrorMessageList.bind(this);



  } // end constructor

  componentDidMount() {
    RestClient.fetchGet('/getcurrentuser', (response) => {
      if (response.success) {

        this.setState({
          isAuthenticated: true,
          currentUser: response.currentUser,
          userRegistrationState: response.currentUser.userStatusName,
          firstName: response.currentUser.firstName,
          lastName: response.currentUser.lastName,
          emailAddress: response.currentUser.emailAddress,
        })
      }
    });

    // Call into the API layer to retrieve the stripe public key so that it is stored in only one place.
    RestClient.fetchGet('/payments/getStripePublicKey/' + GLOBALS.ENVIRONMENT, (response) => {
      if (response.success) {
        this.setState({ stripeKey: response.stripeKey })
      }
    });
  }



  handleInputChange(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;
    const inputClassName = `${name}Class`;

    this.setState({
      [name]: value,
      [inputClassName]: 'form-control',
    });
  }

  async handleSubmitComplete(event) {
    event.preventDefault();
    let validationMessages = [];

   //  const target = event.target;

    // const value = target.type === 'checkbox' ? target.checked : target.value;
    // const name = target.name;

    let msgList = await this.cleanTextInput('firstName', 'first name');
    let msgListLast = await this.cleanTextInput('lastName', 'last name');

    msgList = msgList.concat(msgListLast);

    this.setErrorMessageList(msgList);


    console.log('fingerprint: ' + this.state.stripeCardTokenFingerprint);

    if (msgList.length <= 0) {

      let userData = {
        handle: this.props.handle,
        stripeCardToken: this.state.stripeCardToken,
        stripeCardTokenFingerprint: this.state.stripeCardTokenFingerprint,
      };

      RestClient.fetchPost('/register/updateprovisionaltoactive', userData, response => {
        if (response.success) {

          this.props.updateUserRegistrationState('active');

        } else {
          validationMessages.push(response.message);
          this.setState({
            validationMessage: validationMessages,
            errorMsg: true,
            successMsg: false
          })
        }
      }, (err) => {
        console.log(err);
        this.setState({
          validationMessage: "error: " + err,
          errorMsg: true,
          successMsg: false
        })
      });

    }



  }



  render() {

    if (this.state.stripeKey !== null) {
      return (
        <div className="container">

          <div className="container-green-border">

            <h1 className="text-align-center">Join glibr today.</h1>
            <h5 className="text-align-center">{GLOBALS.STRINGS.GLIBR_TAGLINE}</h5>
            <hr className="style-two" />
            <br />

            <div className="row ">
              <div className="d-none d-sm-block col-sm-3 pr-2">
                <div>


                  <h3 className="glibr-font font-bold font-spacing-add text-align-center">glibr</h3>

                  <div className="glibr-font-purple font-bold font-spacing-add text-align-center">This is how it works. </div>
                  <br />

                  <ul>
                    <li className="check-heart"><span className='li-valign-middle font-bold'>No Tracking</span>
                      <br />
                      <span className="glibr-font">glibr</span> doesn't track anything.  Nothing.  Nada. Zilch.
  <br />
                    </li>

                    <li className="check-heart"><span className='li-valign-middle font-bold'>2 pennies per post</span>
                      <br />
                      ...and that is what makes it all work.
  </li>
                  </ul>


                </div>



              </div>

              <div className="d-none d-sm-block col-sm-1 pr-2"><img src="/images/hearts-left.png" alt="hearts" height="50px" /></div>

              <div className="col-xs-12 col-sm-7 container-full-width" >
                
              <div className="text-align-center" >

              <div className="container-80pct ">
                    <b>Hello {this.props.handle}</b><br /><br />
                    Please enter a vaild credit card<br /> in the form below.  
                    <br /><br />
                  </div>

                <div className="container-box-shadow container-background-light-green container-80pct">
      

                  <div className="text-align-center">

                  <StripeProvider apiKey={this.state.stripeKey}>
                    <Elements>
                      <StripeUserSignUp
                        firstName={this.state.firstName}
                        lastName={this.state.lastName}
                        setErrorMessages={this.setErrorMessageList}
                        captureStripeSignIn={this._captureStripeToken.bind(this)}
                      />
                    </Elements>
                  </StripeProvider>

                  </div>

                  <hr className="style-two" />



                  <div className="text-align-right">
                    <button
                      onClick={this.handleSubmitComplete.bind(this)}
                      className="btn btn-primary"
                      disabled={!this.state.stripeFingerprintReceived}
                    >complete registration</button>
                  </div>




                  </div>

                  <br />

                  <div className="container-80pct ">
                  This is what makes it all work.  Your 2 pennies per post.
                  </div>

                </div>

              </div>

              <div className="d-none d-sm-block col-sm-1 pr-2 "><img className="valign-div-bottom" alt="hearts" src="/images/hearts-right.png" height="50px" /></div>

            </div>

            <br />

            <div className="response-container-error text-align-center" dangerouslySetInnerHTML={{ __html: this.state.validationMsg }} />

          </div>

          <br />

          <div className="container-green-border">

            <p className="text-align-center"><b><span className="glibr-font">glibr</span> we don't track anything!  <u>Especially</u> your Credit Card.
            We use <a href="https://stripe.com/" target="new-stripe">STRIPE</a>  for all our transactions.
            <br />
              <span className="glibr-font cursor-hand" onClick={this._toggleElement.bind(this, 'el_post_pennies')}>more information...</span></b></p>


            <div id="el_post_pennies" className="hide">
              <p><span className="glibr-font">glibr</span> doesn't  track any of your personal data.  We don't need to.  We don't want to.  Because you don't want us to.  <span className="glibr-font">glibr</span> uses the online payment industry leader -  <a href="https://stripe.com/" target="new-stripe">STRIPE</a> - to do that for us.  Leaving you to be free from any hint of any possiblity of any inkling of any tracking.  </p>

              <p>In short you have freedom from data mining and slimy tracking and selling.  All of that for two pennies a post.</p>



              This is how you keep your privacy.<br />
              <b>You pay for your freedom with 2 pennies per post.</b> <br />
              <span className="glibr-font-black font-upsize">glibr</span> will <b>NOT</b> charge you now. <br />
              <ul className="text-align-left">
                <li>We will <b>only</b> charge you<b> after</b> you have  accrued  $3.00 in glibs.</li>
                <li> This control is from our payment provider:   <a href="http://www.stripe.com" target="_stripenew">STRIPE</a>. </li>
                <li> We use the token we get back from STRIPE to verify you and to charge you that $3 when you get there.</li>
              </ul>


            </div>

          </div>
          <br />



        </div>

      )
    } else {
      return (
        <span>The bubble machine is bubblin' as fast as it can bubble.</span>
      )
    }

  }



  _captureStripeToken(e, userData) {

    let validationMessages = [];

    // Check for unique token    
    RestClient.fetchGet("/payments/checkuniquestripecardtoken/" + userData.stripeCardToken, (response) => {

      // return will be  successful if the token is NOT already in the db
      if (response.success) {

        this.setState({
          stripeTokenReceived: true,
          stripeFingerprintReceived: true,
          stripeCardToken: userData.stripeCardToken,
          stripeCardTokenFingerprint: response.stripeCardTokenFingerprint,
          firstName: userData.firstName,
          lastName: userData.lastName,
          errorMsg: false,
          successMsg: false
        })

      } else {
        // there is an existing card.  Set error message
        console.log("in failed branch");
        validationMessages.push(response.message);

        this.setState({
          stripeTokenReceived: false,
          stripeFingerprintReceived: false,
          // message for non-unique card.        
          validationMsg: validationMessages,
          errorMsg: true,
          successMsg: false
        })

      }

    })

  }


  
  _displayValidationMessages() {
    let msgStr = '';
    this.state.validationMessage.forEach(msg => {
      msgStr += msg;
    });

    if (this.state.errorMsg) {
      //we have an error, show the error message
      return (<span className="response-container-error">{msgStr}</span>);
    } else if (this.state.successMsg) {
      return (<span className="response-container-success">{msgStr}</span>);
    } else {
      //      we have no message return nothing - they are both false
      return (null);
    }

  }



  _toggleElement(el) {
    let oEl = document.getElementById(el);

    if (typeof (oEl) !== 'undefined' && oEl !== null) {

      if (oEl.className === "show") {
        oEl.classList.remove('show');
        oEl.classList.add('hide');
      } else if (oEl.className === "hide") {
        oEl.classList.remove('hide');
        oEl.classList.add('show');
      }
    }
  }


  async  cleanTextInput(inputName, entityName) {

    let msgList = [];
    let inputClassName = `${inputName}Class`;

    // run through the validation for First Name
    if (!this.state[inputName]) {
      msgList.push(`You need to enter a ${entityName}`);
      this.setState({
        [inputClassName]: 'form-control form-input-error ',
      })
    } else {
      const cleanText = await Helper.checkForBadWords(this.state[inputName]);

      if (cleanText.foundBadText) {
        this.setState({
          [inputName]: cleanText.cleanedString,
          [inputClassName]: 'form-control form-input-error ',
        })
        msgList.push(`You had some bad words in your ${entityName}, you can't do that.  We fixed it for ya..`);
      }

      if (this.state[inputName].length > GLOBALS.CONSTANTS.NAME_FIELD_LENGTH) {
        this.setState({
          [inputClassName]: 'form-control form-input-error ',
        })
        msgList.push(`Your${entityName} can only be ${GLOBALS.CONSTANTS.NAME_FIELD_LENGTH} long.  Please shorten it.`);
      }

      this.setState({
        [inputName]: Helper.stripTagsFromText(this.state[inputName]),
      })

    }

    if (msgList.length === 0) {
      this.setState({
        [inputClassName]: 'form-control',
      })
    }

    return msgList;

  }


  setErrorMessageList(msgList) {
    let messageList = '';
    msgList.forEach(msg => {
      messageList += `${msg}  <br />`;
    });
    this.setState({
      validationMsg: messageList,
    })
  }




} // end class


//Map the properties of the page to store items
const mapStateToProps = (store) => {
  return {
    //glib properties
    glibs: store.glibsState,

    //user properties
    user: store.userState,

    //authenticate properties
    authenticate: store.authState
  };
};


//connect is the redux method that connects redux and react
export default connect(mapStateToProps)(RegistrationPayment);

