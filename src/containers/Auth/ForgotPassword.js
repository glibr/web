import React, { Component } from "react";

import "./style.css";
import Utilities from "../../common/utils.js";
import RESTCLIENT from "../../common/restClient.js";

let RestClient = new RESTCLIENT();

export default class ForgotPassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      validationMessage: [],
      forgotPageTitle: "Forgot your password?"
    };
  }

  render() {
    return (
      <div className="reset-password container text-align-center">
        <div className="container-green-border containter-full-width text-align-center">
          <h1 className="text-align-center">{this.state.forgotPageTitle}</h1>

          <form id="forgot-password-input-row" className="reset-password">
            <table
              id="forgot-password-form-control-table"
              className="table-center-fix"
            >
              <tbody>
                <tr>
                  <td>
                    <h4 className="push-vAlign-down10">
                      email address for account
                    </h4>
                  </td>
                  <td>
                    <input
                      type="text"
                      className="form-control email"
                      placeholder="Email"
                    />
                  </td>
                </tr>
                <tr>
                  <td colspan="2">
                    <hr className="style-two" />
                  </td>
                </tr>
                <tr>
                  <td></td>
                  <td>
                    <a href="/profile">cancel</a> &nbsp;
                    <button
                      onClick={this._forgotPassword.bind(this)}
                      className="btn btn-primary"
                    >
                      Submit
                    </button>
                  </td>
                </tr>
              </tbody>
            </table>

            <span id="spinnerLoadingImage" className="hide ">
              <img src="../images/resources/green-load.gif" alt="Loading" />
            </span>
            <span className="response-container ">
              {this._displayValidationMessages()}
            </span>
          </form>
        </div>
      </div>
    );
  }

  _displayValidationMessages() {
    let msgStr = "";

    try {
      if (this.state.validationMessage.length > 0) {
        this.state.validationMessage.forEach(msg => {
          msgStr += msg;
        });
      }
    } catch (err) {
      console.log("can't load validation messages: " + err);
    }

    if (this.state.errorMsg) {
      return <span className="response-container-error">{msgStr}</span>;
    } else if (this.state.successMsg) {
      document.getElementById("forgot-password-form-control-table").style.display =
        "none";
      Utilities.common.hideLoadingSpinner(true);
      return <span className="response-container-success">{msgStr}</span>;
    }
  }

  _forgotPassword(e) {
    e.preventDefault();

    let validationMessages = [],
      userData = {
        emailAddress: document
          .getElementsByClassName("email")[0]
          .value.toString()
          .toLowerCase()
      };

    if (!userData.emailAddress) {
      validationMessages.push(
        "Hey, we need an email address if we are gonna, you know, send an email to it."
      );
    } else if (!Utilities.validation.validEmail(userData.emailAddress)) {
      validationMessages.push(
        "Oops, looks like the email you entered may have a typo or something. Please check that guy for errors and try again."
      );
    }

    if (validationMessages.length === 0) {
      this.setState({
        errorMsg: false,
        successMsg: false
      });
      Utilities.common.hideLoadingSpinner(false);

      RestClient.fetchPost(
        "/forgotpassword",
        userData,
        response => {
          if (response.success) {
            validationMessages.push(
              "Thanks! If the e-mail address you entered matches a user in our system, then we have just sent an e-mail out with a link to reset your password."
            );

            this.setState({
              validationMessage: validationMessages,
              errorMsg: false,
              successMsg: true,
              forgotPageTitle: "glibr found that account."
            });
          } else {
            // failed server side validation
            this.setState({
              validationMessage: response.message,
              errorMsg: true
            });
          }
        },
        err => {
          Utilities.common.hideLoadingSpinner(true);
          validationMessages.push(
            "There was an error trying to send an email to that account.  Please try again or perhaps a different email address.  If you need more help contact glibr support at support@glibr.net"
          );

          this.setState({
            validationMessage: validationMessages,
            errorMsg: true
          });
        }
      );
    } else {
      // failed client side validation
      this.setState({
        validationMessage: validationMessages,
        errorMsg: true
      });
    }
  }
}
