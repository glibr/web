import React, { Component } from 'react';
//import { Link } from 'react-router';

import './style.css';
import Utilities from "../../common/utils.js";
import RESTCLIENT from "../../common/restClient.js";
let RestClient = new RESTCLIENT();

export default class ResetPassword extends Component {
  constructor(props) {
    super(props);

    this.state = {
      //resetPasswordToken: Utilities.common.getUrlParameterByName('token', window.location.href),
      validationMessage: [],
      resetPageTitle: 'Please enter a new password'
     };
  }

  render() {
    return (

    


<div className="reset-password container text-align-center">
<div className="container-green-border containter-full-width text-align-center"> 

<h1 className="text-align-center">{this.state.resetPageTitle}</h1>


<form id="forgot-password-input-row" className="reset-password">

<table id='forgot-password-form-control-table' className='table-center-fix'>
<tbody>
     
      <tr>
        <td>
          <h4 className="push-vAlign-down10">new password:</h4>
        </td>
        <td>
          <input type="password" id="password1" className="form-control" placeholder="Enter New Password" />
        </td>
      </tr>
      <tr>
        <td>
            <h4 className="push-vAlign-down10">retype new password:</h4>
        </td>
        <td>
            <input type="password" id="password2" className="form-control" placeholder="Retype New Password" />
       </td>
      </tr>
      <tr>
        <td colspan="2">
          <hr className="style-two" />
        </td>
      </tr>
      <tr>
        <td></td>
        <td>
          <a href="/profile">cancel</a> &nbsp;
          <button onClick={this._resetPassword.bind(this)} className="btn btn-primary">Reset my Password</button>
              </td>
      </tr>

     
     

</tbody>
</table>

<span id="spinnerLoadingImage" className="hide "><img src="../images/resources/green-load.gif" alt="Loading" /></span>
 <span className="response-container ">
          {this._displayValidationMessages()}
</span>
   

</form>
</div>
</div>










    );
  }

  _displayValidationMessages() {
    let msgStr = '';
    this.state.validationMessage.forEach(msg => {
      msgStr += msg;
    });

    if (this.state.errorMsg) {
      Utilities.common.hideLoadingSpinner(true);
      return (<span className="response-container-error">{msgStr}</span>);
    } else if (this.state.successMsg) {
      document.getElementById('forgot-password-form-control-table').style.display="none";
   
      return (<span className="response-container-success">{msgStr}</span>);
    }
  }

  _resetPassword(e) {
    e.preventDefault();

    let validationMessages = [],
      userData = {
        token: Utilities.common.getUrlParameterByName('token', window.location.href), // get the token from the URL so we know what user this is
        password: document.getElementById('password1').value,
        repeatPassword: document.getElementById('password2').value
      };

    if (!userData.password || !userData.repeatPassword) {
      validationMessages.push('Hey, please enter your new one and retype it just to make sure.');
    } else if (userData.password !== userData.repeatPassword) {
        validationMessages.push('Ok, so good try, but your two new passwords don\'t match');
    } else if (Utilities.validation.weakPassword(userData.password)) {
        validationMessages.push('You gotta make a harder password.  Make sure you have lowercase, uppercase, number and special character.  And it has to be at least 8 characters long.');
    }

    if (validationMessages.length === 0) {
      Utilities.common.hideLoadingSpinner(false);

      RestClient.fetchPost('/forgotpasswordreset', userData, response => {
        if (typeof response !== 'undefined') {
          if (response.success) {
              validationMessages.push('Thanks! Your password was reset successfully, don\'t forget it! Remember, P@ssword1 is NOT a good password, just so you know, and if yours is something like that, we\'d be pretty disappointed.');

              this.setState({
                validationMessage: validationMessages,
                errorMsg: false,
                successMsg: true,
                resetPageTitle: 'Your password has been reset'
              });
          } else {
            // failed server side validation
              validationMessages.push(response.message)
            this.setState({
              validationMessage: validationMessages,
              errorMsg: true
            });
          }
        }
      }, err => {
        Utilities.common.hideLoadingSpinner(true);

        let serverResponseMessage = '';

        if (typeof this.response !== 'undefined') {
          if (this.response.message.length > 0) {
            serverResponseMessage = this.response.message;
          }
        }

        validationMessages.push('There was an error trying to set your new password. '+ serverResponseMessage +' If you need more help contact glibr support at support@glibr.net');

        this.setState({
          validationMessage: validationMessages,
          errorMsg: true
        });
      });
    } else {
      // failed client side validation
      this.setState({
        validationMessage: validationMessages,
        errorMsg: true
      });
    }
  }
}
