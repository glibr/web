import React, { Component } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
//import store from '../../store';

// Controls
import PasswordControl from "./passwordcontrol.js"
import Utilities from "../../common/utils.js";
import RESTCLIENT from "../../common/restClient.js";

import './style.css';

// Actions
// Import Actions
import {
  setUserLoggedIn,
  setCurrentUserRole,
  setCurrentUserStatus
} from '../../actions/authActions';

// Short hand for mapDispatch
// removes the need for the mapdispatch method and the long list of setters
// "actionCreators" is passed as the second parameter in the connect call - see bottom of this module
const actionCreators = {
      //auth actions
      setUserLoggedIn,
      setCurrentUserRole,
      setCurrentUserStatus
}; 

let RestClient = new RESTCLIENT();

class Login extends Component {
  constructor(props) {
    super(props);

    this.state = {
      accountActivated: Utilities.common.getUrlParameterByName('accountactivated', window.location.href),
      accountActivationMessage: 'Your account has been activated!',
      accountCreated: Utilities.common.getUrlParameterByName('accountregistration', window.location.href),
      accountCreatedMessage: 'Your account was successfully created, check your email to validate.',
      credentialsIncorrectMessage: 'Jimmeny jillickers batman!  Either your username or your password is incorrect!',
      validationMessage: []
    };
  }

  componentDidMount(){

    clearInterval(this.props.glibs.intervalTimer_MainGlibFeed);
    clearInterval(this.props.glibs.intervalTimer_UserGlibFeed);
    clearInterval(this.props.glibs.intervalTimer_SecondaryGlibFeed);


  }

  render() {
    return (
      <div className="login-container container">
        <div className="container-green-border">
          <h1 className="text-align-center">Login below</h1>
        </div>

        <br />

        <div className="container-green-border">
          <form className="login-glibr">
            <div className="form-group row">
              <div className="col-12">
                <span className="response-container">{this._displayLoginMessage()}</span>
              </div>
            </div>

            <div className="form-group row">
              <div className="col-12">
                <input type="text" className="form-control" id="email" placeholder="Email" />
              </div>
            </div>

            <div className="form-group row">
              <div className="col-12">
                  <PasswordControl passwordFieldType="registration" />
              </div>
            </div>

            <div className="form-group row">
              <div className="col-12 text-right">
                <Link to="/forgot-password">Forgot password?</Link><span>&nbsp;&nbsp;</span>
                <button onClick={this._loginUser.bind(this)} className="btn btn-primary">Sign in</button>
              </div>
            </div>

            <div className="form-group row">
              <div className="col-12 text-align-center">
                <span id="spinnerLoadingImage" className="hide "><img src="../images/resources/green-load.gif" alt="Loading" /></span>
                <span className="response-container">{this._displayValidationMessages()}</span>
              </div>
            </div>
          </form>
        </div>
      </div>
    );
  }

  _displayLoginMessage() {
    if (this.state.accountCreated) {
      return (<span className="response-container-success">{this.state.accountCreatedMessage}</span>)
    } else if (this.state.accountActivated) {
      return (<span className="response-container-success">{this.state.accountActivationMessage}</span>);
    } else if (this.state.invalidUsernameOrPassword) {
      return (<span className="response-container-error">{this.state.credentialsIncorrectMessage}</span>);
    }
  }

  _displayValidationMessages() {
    let msgStr = '';
    this.state.validationMessage.forEach(msg => {
      msgStr += msg;
    });

    if (this.state.errorMsg) {
      Utilities.common.hideLoadingSpinner(true);
      return (<span className="response-container-error">{msgStr}</span>);
    } else if (this.state.successMsg) {
      //navigate to login on success
      window.location = "/";
      Utilities.common.hideLoadingSpinner(true);
      return (<span className="response-container-success">{msgStr}</span>);
    }
  }

  _loginUser(e) {
    e.preventDefault();

    let validationMessages = [],
      userData = {
        username: document.getElementById('email').value.toString().toLowerCase().trim(),
        password: document.getElementById('password').value
      };

    if (!userData.username || !userData.password) {
      validationMessages.push('Oops looks like you forgot to enter some stuff, please fill in all the boxes above.');
    } else if (!Utilities.validation.validEmail(userData.username)) {
      validationMessages.push('Oops, looks like the email you entered may have a typo or something. Please check that guy for errors and try again.');
    } else if (Utilities.validation.weakPassword(userData.password)) {
      validationMessages.push('Whoops, that can\'t be your password, you made it with at least one uppercase letter, one special character, and one number.  Give it another shot.')
    }

    if (validationMessages.length === 0) {
      Utilities.common.hideLoadingSpinner(false);

      RestClient.fetchPost('/login', userData, response => {
        if (response.success) {
           
          console.log(response.message)
              //navigate 
              // No need to save redux state
             window.location = '/';
        } else {
          //set false on login
              this.props.setUserLoggedIn(false);
              validationMessages.push(response.message);

              this.setState({
                invalidUsernameOrPassword: true
                //validationMessage: validationMessages
          });

          Utilities.common.hideLoadingSpinner(true);
        }
      }, err => {
         console.log(err);

          Utilities.common.hideLoadingSpinner(true);

          validationMessages.push('There was an error trying to login to your account.  Please contact glibr support at support@glibr.net');

          this.setState({
            validationMessage: validationMessages,
            errorMsg: true
          });
        });
    } else {
      Utilities.common.hideLoadingSpinner(true);

      this.setState({
        validationMessage: validationMessages,
        errorMsg: true
      });
    }
  }
}


//Map the properties of the page to store items
const mapStateToProps = (store) => {
  return {
    //glib properties
    glibs: store.glibsState,

    //user properties
    user: store.userState,

    //authenticate properties
    authenticate: store.authState
  };
};


//connect is the redux method that connects redux and react
export default connect(mapStateToProps, actionCreators)(Login);