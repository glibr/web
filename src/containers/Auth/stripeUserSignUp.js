import React, { Component } from 'react';
import { CardElement, injectStripe } from 'react-stripe-elements';
import { Fragment } from 'react';

export class StripeUserSignUp extends Component {
      constructor(props) {
            super(props);

            this.state = {}
      }


      // https://stripe.com/docs/saving-cards
      async  _stripeCardVerification(e) {

            let msgList = [];

            console.log(this.props.firstName);

            if (!this.props.firstName) {
                  msgList.push('Please enter a first name.');
                  this.props.setErrorMessages(msgList);
            }


            if (!this.props.lastName) {
                  msgList.push('Please enter a last name.');
                  this.props.setErrorMessages(msgList);
            }

            if (msgList.length <= 0) {

                  console.log("in call to stripe");

                  const result = await this.props.stripe.createToken({ name: "Name" });

                  if (result.error) {
                        // If there's an error, log and handle it
                        console.log(result.error)
                        msgList.push(result.error.message);
                        this.props.setErrorMessages(msgList);
                  }

                  else {
                        let userData = {
                              firstName: this.props.firstName,
                              lastName: this.props.lastName,
                              stripeCardToken: result.token.id,
                        };

                        if (result.token.id) {
                              // valid token?
                              this.props.captureStripeSignIn(null, userData);
                              this.props.setErrorMessages(msgList);
                        } else {
                              // else
                              //present error
                              console.log("failed to get token");
                              msgList.push("failed to get token");
                              this.props.setErrorMessages(msgList);
                        }

                  }

            }


      }

      // Using Stripe Elements in React     
      // https://stripe.com/docs/recipes/elements-react

      render() {


            return (

                  <Fragment >





                        <CardElement className="form-control form-control-stripe " />


                        <br />

                        <div className="text-align-right">
                              <button onClick={this._stripeCardVerification.bind(this)} className="btn btn-primary">verify card with STRIPE</button>
                        </div>

                  </Fragment>

            )
      }

} // End class

export default injectStripe(StripeUserSignUp);