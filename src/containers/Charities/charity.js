import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import './style.css';

// Props passed in
export class Charity extends Component {
  constructor(props) {
    super(props);

    //component inits
    this.glibReplyEditControl = null;

  }


  render() {

    var containerClassName = "container-green-border";
    if (this.props.user.charityForUser  === this.props.name) {
      containerClassName = "container-purple-border container-background-light-green ";
    }

    return (

      <Fragment >

      <div className={containerClassName}>
        <table width="100%">
          <tbody>
            <tr >
              <td>
                <h1 className="style-two"> {this.props.name}</h1>
              </td>
              <td align="right" width="100">
                <img src={this.props.logo} alt={"logo for " + this.props.name} className="charityLogo" />
              </td>
            </tr>
            <tr>
              <td colSpan="2">  {this.props.description}</td>
            </tr>
            <tr>
              <td>
                <hr className="style-two" />
                glibs: {this.props.glibCount} |
                pennies: {this.props.glibPennies} |
                users: {this.props.usersChosen}


              </td>
              <td className="text-align-center">
              
                <button className="btn btn-primary" onClick={this.props.setCharity} charityname={this.props.name}>select</button>

              </td>
            </tr>

          </tbody>

        </table>
        
      </div>
      <br />
     </Fragment>

    )

  }

} // end class



const mapStateToProps = function (store) {
  return {
    //glib properties
    glibs: store.glibsState,

    //user properties
    user: store.userState,

    //authenticate properties
    authenticate: store.authState
  };
};

export default connect(mapStateToProps)(Charity);