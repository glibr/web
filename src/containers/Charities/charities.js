import React, { Component } from 'react';
import { connect } from 'react-redux';
import './style.css';

// Components
import Charity from "./charity.js";

import RESTCLIENT from "../../common/restClient.js";

// Actions
// Import Actions
import {
  setCurrentUserCharity
} from '../../actions/userActions';

// Short hand for mapDispatch
const actionCreators = {
  setCurrentUserCharity
};

let RestClient = new RESTCLIENT();

class Charities extends Component {
  constructor(props) {
    super(props);

    this.state = {
      charitiesRendered: ""
    };

  }//end constructor


  componentDidMount() {

    this._getCharities();
  }

  render() {

    if (!this.props.authenticate.userSetStateDone)
    // This value gets set in the header to note when the other redux user state values are set.
    {
      return (<div>patience, the squirrels are working as fast as they can.</div>);

    } else {

      return (
        <div className="text-align-center">
          <h3>Please choose a Charity to partner with</h3>

          <hr className="style-two" />
          {this.state.charitiesRendered}

        </div>
      )
    }

  }

  _getCharities() {

    var feedQuery = '/charities/getcharitieslist';

    RestClient.fetchGet(feedQuery, response => {
      let charitiesList = [],
        iter = 3000;

      response.charities.forEach(charity => {
        // Check to see if we have a picture as part of the glib.
        // Defensive coding, check for existence first.

        charitiesList.push(
          <Charity charity_id={charity._id}
            name={charity.name}
            description={charity.description}
            logo={charity.logo}
            url={charity.url}
            glibCount={charity.glibCount}
            glibPennies={charity.glibPennies}
            usersChosen={charity.usersChosen}
            setCharity={this._setCharityForUser.bind(this)}
            userSelected={this.props.user.charityForUser}

            key={iter + '-' + charity._id}
          />
         
        );
        iter++;
      });

      this.setState({
        charitiesRendered: charitiesList
      })

    }, err => {
      console.log(err);
    });


  }



  _setCharityForUser(e) {

    e.preventDefault();

    var charity = e.target.attributes.charityname.value;
    var thisUser = this.props.user.currentUserHandle;
    // Get user handle
    // create feed query
    var feedQuery = '/charities/setcharityforuser/' + charity + '/' + thisUser;


    RestClient.fetchGet(feedQuery, response => {

      if (response.success) {
        this.props.setCurrentUserCharity(response.charity);

        this.props.enableCharityButton(response.charity);

      }




    }, err => {
      console.log(err);
    });


  }


}// end class


//Map the properties of the page to store items
const mapStateToProps = (store) => {
  return {
    //glib properties
    glibs: store.glibsState,

    //user properties
    user: store.userState,

    //authenticate properties
    authenticate: store.authState
  };
};


//connect is the redux method that connects redux and react
export default connect(mapStateToProps, actionCreators)(Charities);