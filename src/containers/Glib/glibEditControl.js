import React, { Component } from 'react';
import { connect } from 'react-redux';
import RESTCLIENT from "../../common/restClient.js";
import GLOBALS from "../../common/globals.js";
import HelperMethods from "../../common/helpers.js";

import UploadImageControl from '../../components/UploadButton/';

// Actions
// Will need to update the count for number of glibs
import {
  setCurrentUserNumberGlibs
} from '../../actions/userActions';

//setCurrentUserStatus(currentUserStatus)
import {
  setCurrentUserStatus
} from '../../actions/authActions';
// Short hand for mapDispatch
// removes the need for the mapdispatch method and the long list of setters
// "actionCreators" is passed as the second parameter in the connect call - see bottom of this module
const actionCreators = {
  setCurrentUserNumberGlibs,
  setCurrentUserStatus
}

// Libraries and constants
let RestClient = new RESTCLIENT();
// Props passed in
//  - glibReplyType == Type of glib: new, reply, reglib
//  - parentGlibID == parent glib id - may be null
//  - feedType ==  glib feed type - may be null
export class GlibEditControl extends Component {
  constructor(props) {
    super(props);

    this._handleKeyPress = this._handleKeyPress.bind(this);

    this.state = {
      glibPicture: null,
      glibPictureFullPath: null,
      glibPictureHash: null,
      errorGlibEditMessage: null,
      viewEditControl: true,
      postComplete: false,
      tempImageSource: null,
    }

    this.uploadButtonResponse = this.uploadButtonResponse.bind(this);

  }


  //method injected into UploadImageControl
  uploadButtonResponse(response) {

      if(response.message==='spinner'){
        console.log(window.location.href + this.state.tempImageSource);
        this.setState({
          tempImageSource: HelperMethods.combineURLs(window.location.href, response.tempFIleSource),
          errorGlibEditMessage: null,
        })
      } else {
        if (!response.success){
          this.setState({
            tempImageSource: null,
            errorGlibEditMessage: response.message,
          })
        } else {
        this.setState({
          tempImageSource: response.tempFIleSource,
          glibPicture: response.fileName,
          errorGlibEditMessage: response.message,
        })
      }  
      
    }
   
  }



  render() {

    let glibPhotoClassname = this.state.tempImageSource ? 'glib-picture' : 'glib-picture hide';
    let errorMessageClassName = this.state.errorGlibEditMessage ? 'glib-error' : 'glib-error hide';

    let glibCost = "newGlibCost";
    let gBodyTextId = "glibBodyText";
    let glibLetterCount = "glibLetters";
    let gParentID = null;

    //set glib variables
    if (this.props.glibReplyType === "reply" || this.props.glibReplyType === "reGlib") {
      //this.props.parentID
      glibCost = "newGlibCost_" + this.props.parentID;
      gBodyTextId = "glibBodyText_" + this.props.parentID;
      glibLetterCount = "glibLetters_" + this.props.parentID;
      gParentID = this.props.parentID;
    }

    if (this.state.viewEditControl) {
      return (
        <div className="text-align-center">

          <section className={this.props.glibEditClass}>

            <img alt="glib pic" className={glibPhotoClassname} src={this.state.tempImageSource} />

            {/* <span className="tiny-text">{this.props.parentID} | {this.props.glibReplyType}</span> */}

            <textarea onKeyUp={() => this._handleKeyPress(this)} placeholder="What's on your mind?" className="new-glib-body" parentid={gParentID} id={gBodyTextId}></textarea>

            <div className="glib-message-bar" id="glibMessageBar">letters remaining: <span id={glibLetterCount}>{GLOBALS.CONSTANTS.GLIB_POST_MAX_LENGTH}</span></div>

            <div className="new-glib-toolbar">
              <span className={errorMessageClassName}>{this.state.errorGlibEditMessage}</span>

              <label className="new-glib-cost-label">pennies:</label>
              <input type="number" id={glibCost} className="new-glib-cost" placeholder={GLOBALS.CONSTANTS.GLIB_MIN_PENNIES} defaultValue={GLOBALS.CONSTANTS.GLIB_MIN_PENNIES} />


              {this._renderPhotoUploadButton()}

              {this._renderGlibPostButton()}
            </div>
          </section>

        </div>
      );

    } else {
      return null;
    }

  }




  _renderGlibPostButton() {

    switch (this.props.glibReplyType) {

      case "reply":
        return (
          <span>
            <button className="glib-button" onClick={this._composeGlib.bind(this, 'reply', this.props.parentID)}>
              <span className="glibr-font-white">post glib</span>
            </button>
            <br />
            <span className="tiny-text" onClick={this.props.onCancel}>cancel</span>
          </span>
        )
      // don't need break; because we return

      case "reGlib":
        return (
          <span>
            <button className="glib-button" onClick={this._composeGlib.bind(this, 'reGlib', this.props.parentID)}>
              <span className="glibr-font-white">post glib</span>
            </button>
            <br />
            <span className="tiny-text" onClick={this.props.onCancel}> cancel</span>
          </span>
        )
      // don't need break; because we return

      default:
        return (
          <button className="glib-button" onClick={this._composeGlib.bind(this, 'original', -1)}>
            <span className="glibr-font-white">post glib</span>
          </button>
        )
      // don't need break; because we return

    }

  }



  _renderPhotoUploadButton() {

    switch (this.props.glibReplyType) {

      case "reply":
      case "reGlib":
        //nothing for now
        break;

      default:
        return (
          <UploadImageControl   uploadFileResponse={this.uploadButtonResponse}   />
        )
      // don't need break; because we return

    }

  }


  /*-------------------------------------------------------------------
  * _componseGlib
  *
  * This is where we make and save a glib
  *
  *-------------------------------------------------------------------*/
  _composeGlib(glibReplyType, parentID) {

    this.setState({
      errorGlibEditMessage: null
    })

    console.log(`compose: is this a private message:  ${this.props.glibPrivate} and if so the handle is: ${this.props.glibPrivateUserId}`);

    let validationMessages = [];
    let _parentPost = null;
    let _quotedPost = null;
    let _glibPost = null;
    let _glibCost = null;

    //check to see if we are on a single glib page
    if (this.props.glibType !== 'singleGlib') {
      _glibPost = document.getElementById('glibBodyText').value;
      _glibCost = document.getElementById('newGlibCost').value;
    } else {
      _glibPost = document.getElementById("glibBodyText_" + parentID).value;
      _glibCost = document.getElementById("newGlibCost_" + parentID).value;
    }


    let minPennies = GLOBALS.CONSTANTS.GLIB_MIN_PENNIES;
    // let maxPennies = GLOBALS.CONSTANTS.GLIB_MAX_PENNIES;

    //check for over max pennies
    if (_glibCost > GLOBALS.CONSTANTS.GLIB_MAX_PENNIES) {
      alert("Yo, you can only glib a max of " + GLOBALS.CONSTANTS.GLIB_MAX_PENNIES + " pennies, so mellow out.");
      document.getElementById('newGlibCost').value = GLOBALS.CONSTANTS.GLIB_MAX_PENNIES;
      return;
    }

    //check for under min pennies
    if (_glibCost < minPennies) {
      alert("Yo, your glib has to be at least a " + minPennies + " penny glib, it's going to charity, have a heart.");
      document.getElementById('newGlibCost').value = minPennies;
      return;
    }


    if (glibReplyType === 'reply') {
      _parentPost = parentID;
      _glibPost = document.getElementById("glibBodyText_" + parentID).value;
      _glibCost = document.getElementById("newGlibCost_" + parentID).value;
    } else if (glibReplyType === 'reGlib') {
      _quotedPost = parentID;
      _glibPost = document.getElementById("glibBodyText_" + parentID).value;
      _glibCost = document.getElementById("newGlibCost_" + parentID).value;
    }

    let glibData = {
      glibPost: _glibPost,
      glibCost: _glibCost,
      parentPost: _parentPost,
      quotedPost: _quotedPost,
      glibPicture: this.state.glibPicture,

      //private messages 
      glibPrivate: this.props.glibPrivate,
      glibPrivateUserId: this.props.glibPrivateUserId,

      // groups messages
      groupId: this.props.groupId,
    };

    RestClient.fetchPost('/glibs/create', glibData, response => {
      if (response.success) {

        if (glibReplyType === 'reply' || glibReplyType === 'reGlib') {
          this.setState({
            glibEditControlRendered: null
          })

        } else {

          document.getElementById('glibBodyText').value = '';
          document.getElementById('newGlibCost').value = minPennies;
        }

        this.setState({
          errorMsg: false,
          successMsg: true
        });
        
        if(response.provisionalExpired){
          this.props.setCurrentUserStatus('provisional-expired');
        }

        //return call to reload feed right now.
        this.props.reloadFeed();

  

      } else {
        validationMessages.push(response.message);

        this.setState({
          errorGlibEditMessage: validationMessages,
          errorMsg: true
        });

      }
    }, err => {
      console.log(err);
    });

    var boolViewEditControl = true;
    if (glibReplyType === 'reply' || glibReplyType === 'reGlib') {
      boolViewEditControl = false;
    }
    this.setState({
      //make picture go away from edit box if there was one.
      glibPicture: null,
      tempImageSource:null,
      viewEditControl: boolViewEditControl,
      postComplete: true
    });


    if (this.props.onComplete !== null) {
      this.props.onComplete(true);
    }


  }



 



  /* ************************************
  * _handleKeyPress
  *  This method captures the key press from the control and adjusts the number of remaining letters
  *  With the associated color of the number to denote how close to the end the user is getting.
 **************************************/
  _handleKeyPress(gBodyTextElement) {

    // add on the parent id for the page element id if this is a reply or reGlib
    let pID = "";
    if (this.props.parentID) {
      pID = "_" + this.props.parentID;
    }
    let glibBody = document.getElementById('glibBodyText' + pID);
    let glibLetterCount = document.getElementById('glibLetters' + pID);
    let letterCount = GLOBALS.CONSTANTS.GLIB_POST_MAX_LENGTH - glibBody.value.length;

    if (10 < letterCount && letterCount <= 25) {
      glibLetterCount.innerHTML = letterCount;
      glibLetterCount.classList.remove('red-text');
      glibLetterCount.classList.add('orange-text');
    } else if (0 < letterCount && letterCount <= 10) {
      glibLetterCount.innerHTML = letterCount;
      glibLetterCount.classList.remove('orange-text');
      glibLetterCount.classList.add('red-text');
    } else if (letterCount <= 0) {
      letterCount = 0;
      //reset the textarea to have only the 150 length
      glibBody.value = glibBody.value.substring(0, GLOBALS.CONSTANTS.GLIB_POST_MAX_LENGTH);
      glibLetterCount.innerHTML = letterCount;
      glibLetterCount.classList.remove('orange-text');
      glibLetterCount.classList.add('red-text');
    } else {
      glibLetterCount.innerHTML = letterCount;
      glibLetterCount.classList.remove('orange-text');
      glibLetterCount.classList.remove('red-text');
    }

  }

}//end class

//-----------------------------------------------
//  Set Default props values
GlibEditControl.defaultProps = {
  //  - glibReplyType == Type of glib: new, reply, reglib
  //default == new
  glibReplyType: "new",

  //  - parentGlibID == parent glib id - may be -1
  // default -1 to denote no parent id
  // maps to parentPost in mongo
  parentID: null,

  //  - quotedGlibID == quoted  glib id - may be -1
  // default -1 to denote no parent id
  // maps to quotedPost in mongo
  quotedGlibID: null,

  //  - feedType ==  glib feed type 
  // Default "full"
  feedType: "full",


  glibEditClass: 'new-glib',
  onComplete: null,

  glibPrivate: false,
  userToPM: null,

  groupId: null,

};






const mapStateToProps = function (store) {
  return {
    //glib properties
    glibs: store.glibsState,

    //user properties
    user: store.userState,

    //authenticate properties
    authenticate: store.authState
  };
};


//connect is the redux method that connects redux and react
export default connect(mapStateToProps, actionCreators)(GlibEditControl);

