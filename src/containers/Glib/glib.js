import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router';
import ReactModal from 'react-modal';

import './style.css';

//components
import GlibEditControl from '../Glib/glibEditControl.js';
import ShareGlibDialog from '../Glib/shareGlibDialog';
import GlibSingle from '../GlibFeed/GlibSingle.js';
import EnvironmentSettings from "../../common/environmentsettings.js";
import RESTCLIENT from "../../common/restClient.js";

// Libraries and constants
let envSettings = new EnvironmentSettings();
let RestClient = new RESTCLIENT();

// Props passed in
//  - glibType == Type of glib: new, reply, reglib
//  - parentGlibID == parent glib id - may be null
//  - feedType ==  glib feed type - may be null
export class Glib extends Component {
  constructor(props) {
    super(props);

    //function binds
    this._renderReplyToGlib = this._renderReplyToGlib.bind(this);
    this._likeGlib = this._likeGlib.bind(this);
    this._unLikeGlib = this._unLikeGlib.bind(this);
    this._deleteGlib = this._deleteGlib.bind(this);
    this._cancelReplyPost = this._cancelReplyPost.bind(this);

    //component inits
    this.glibReplyEditControl = null;

    this.state = {
      glibRendered: null,
      glibType: "normal", //normal | private | group | readOnly
      viewGlibReplyControl: false,
      avatarSrc: '/images/logo_NO_profilePic.png',

      //like states
      glibLikeCount: null,
      glibLikeImage: '/images/glib-like-icon.png',
      userLikesGlib: false,

      //unlike states
      glibUnlikeCount: null,
      glibUnlikeImage: '/images/glib-unlike-icon.png',
      userUnlikesGlib: false,

      //glib picture
      glibPictureSrc: "",

      showModal: false,
      modalContentItem: null,
      customStyles: '',
    };


  }

  handleOpenModal(thisModalContentItem) {
    // if we are a smaller screen just navitgate to the single glib page
    if (window.screen.width <= 576 && thisModalContentItem === 'glibSingle') {

      window.location = `/glib?glibId=${this.props.glibId}`;
    } else {

      let scrollYPos = window.pageYOffset;

      const customStyles = {
        content: {
          top: "50px",
        }
      }


      this.setState({
        // if we are a larger screen then open modal
        modalContentItem: thisModalContentItem,
        showModal: true,
        modalTopPosition: scrollYPos,
        customStyles: customStyles,
      });

    }

  }

  getScrollPostion() {
    if (window.pageYOffset !== undefined) {
      return {
        sx: window.pageXOffset,
        sy: window.pageYOffset,
      }
    } else {
      var sx, sy, d = document,
        r = d.documentElement,
        b = d.body;
      sx = r.scrollLeft || b.scrollLeft || 0;
      sy = r.scrollTop || b.scrollTop || 0;
      return {
        sx: sx,
        sy: sy,
      }
    }
  }

  handleCloseModal() {
    this.setState({ showModal: false });
  }


  componentDidMount() {

    //set avatar
    if (this.props.avatar) {
      this.setState({
        avatarSrc: envSettings.GLIBR_IMAGE_ROOT_URI() + this.props.avatar
      })
    }
    //set glib picture

    if (this.props.glibPicture) {
      this.setState({
        glibPictureSrc: envSettings.GLIBR_IMAGE_ROOT_URI() + this.props.glibPicture
      })

    }

    // does the glib have likes?
    if (this.props.glibLikes) {
      //does THIS user like it?
      if (this.props.glibLikes.indexOf(this.props.user.currentUserHandle) !== -1) {
        this.setState({
          glibLikeImage: '/images/glib-like-done-icon.png',
          userLikesGlib: true
        });
      }
      // set number of likes
      this.setState({
        glibLikeCount: this.props.glibLikes.length
      });
    }
    // does the glib have unlikes?
    if (this.props.glibUnlikes) {
      //does THIS user like it?
      if (this.props.glibUnlikes.indexOf(this.props.user.currentUserHandle) !== -1) {
        this.setState({
          glibUnlikeImage: '/images/glib-unlike-done-icon.png',
          userUnlikesGlib: true
        });
      }
      // set number of likes
      this.setState({
        glibUnlikeCount: this.props.glibUnlikes.length
      });
    }

  }



  render() {

    //set show hide for the uploaded glib picture
    let glibPhotoClassname = this.props.glibPicture ? 'glib-picture' : 'glib-picture hide';

    if (this.props.feedType === "replyList" || this.props.feedType === "readOnly") {
      // ----------------------------------------------------------------------------------------------
      // for the replies to a single glib page
      // ----------------------------------------------------------------------------------------------
      return (
        <div className="glib text-align-left" id={"glib" + this.props.glibId}>

          <table width="100%" className="glib-table">
            <tbody>
              <tr className="glib-row">

                <td className="glib-cell">
                  <div><span className="glibr-font font-upsize"><Link to={this.props.cleanHandle}>{this.props.cleanHandle}</Link></span>
                    <span className="glib-handle"> - <Link to={this.props.cleanHandle}>{this.props.author}</Link> -
                   {this.props.glibdate}</span>
                  </div>

                  <hr className="style-four-purple" />

                  <div className="glib-text" dangerouslySetInnerHTML={{ __html: this.props.glibtext }} />

                  <img src={this.state.glibPictureSrc} className={glibPhotoClassname} alt='glib pic' />

                </td>
              </tr>
            </tbody>
          </table>
        </div>
      );


    } else if (this.props.feedType === "glibReturnReGlib") {
      // ----------------------------------------------------------------------------------------------
      // this is the recursion for reGlibs
      // ----------------------------------------------------------------------------------------------

      return (
        <div className="glib text-align-left" id={"glib" + this.props.glibId}>

          <table width="100%" className="glib-table">
            <tbody>
              <tr className="glib-row">

                <td className="glib-cell">
                  <div><span className="glibr-font font-upsize"><Link to={this.props.cleanHandle}>{this.props.cleanHandle}</Link></span>
                    <span className="glib-handle"> - <Link to={this.props.cleanHandle}>{this.props.author}</Link> -
                   {this.props.glibdate}</span>
                  </div>

                  <hr className="style-four-purple" />


                  <div className="glib-text" dangerouslySetInnerHTML={{ __html: this.props.glibtext }} />

                  <img src={this.state.glibPictureSrc} className={glibPhotoClassname} alt='glib pic' />

                  {this._renderQuotedPost()}

                </td>
              </tr>
            </tbody>
          </table>
        </div>
      );


    } else if (this.props.feedType === "glibModal" || this.props.feedType === "glibSingleView") {
      // ----------------------------------------------------------------------------------------------
      // modal view or single view
      // ----------------------------------------------------------------------------------------------
      return (

        <div className="glib text-align-left" id={"glib" + this.props.glibId}>

          <table width="100%" className="glib-table">
            <tbody>
              <tr className="glib-row">
                <td width="15%" className="">

                  <div className="glib-avatar-circle-cropper">
                    <div className="glib-avatar-picture-inner" >
                      <img src={this.state.avatarSrc} alt={this.props.handle + "'s Avatar"} className="glib-avatar-picture" />
                    </div>
                  </div>

                </td>
                <td className="glib-cell"   >

                  {this._renderGlibUserControl(this.props.glibId)}

                  <div  >
                    <div><span className="glibr-font font-upsize"><Link to={this.props.cleanHandle}>{this.props.cleanHandle}</Link></span>
                      <span className="glib-handle"> - <Link to={this.props.cleanHandle}>{this.props.author}</Link> -
                                  {this.props.glibdate}</span>
                    </div>

                    <hr className="style-four-purple" />

                    <div className="glib-text" dangerouslySetInnerHTML={{ __html: this.props.glibtext }} />
                    {this._renderQuotedPost()}

                    <img src={this.state.glibPictureSrc} className={glibPhotoClassname} alt='glib pic' />
                  </div>
                </td>

              </tr>

              <tr>
                <td>
                  <div className="text-align-center"><span className="glib-cost">&cent;{this.props.glibcost}</span></div>
                </td>
                <td className="text-align-right">

                  {this._renderGlibFooter()}

                </td>
              </tr>

            </tbody>
          </table>

        </div>

      )
    } else if (this.props.feedType === "privateMessagesWithUser") {
      // ----------------------------------------------------------------------------------------------
      // Private Message
      // ----------------------------------------------------------------------------------------------

      return (
        <div className="glib text-align-left" id={"glib" + this.props.glibId}>

          <table width="100%" className="glib-table">
            <tbody>
              <tr className="glib-row">

                <td className="glib-cell"   >

                  {this._renderGlibUserControl(this.props.glibId)}

                  <div><span className="glibr-font">pm from: <Link to={this.props.cleanHandle} onClick={this.forceUpdate}>{this.props.cleanHandle}</Link></span>
                  </div>
                  <div><span className="glibr-font">pm to: <Link to={this.props.glibPMToHandle} onClick={this.forceUpdate}>{this.props.glibPMToHandle}</Link></span>
                  </div>

                  <hr className="style-four-purple" />
                  <div onClick={this.handleOpenModal.bind(this, 'glibSingle')}>
                    <div className="glib-text" dangerouslySetInnerHTML={{ __html: this.props.glibtext }} />
                    {this._renderQuotedPost()}

                    <img src={this.state.glibPictureSrc} className={glibPhotoClassname} alt='glib pic' />
                  </div>

                </td>

              </tr>

              <tr>

                <td className="text-align-right">
                  <hr className="style-five-purple" />
                  <span className="tiny-text">posted: {this.props.glibFullDate}</span>

                  {this._renderGlibFooter()}

                </td>
              </tr>

            </tbody>
          </table>

          <ReactModal
            isOpen={this.state.showModal}
            onRequestClose={this.handleCloseModal.bind(this)}
            contentLabel="glibr modal"
            className="glibr-modal"
            ariaHideApp={false}
            shouldFocusAfterRender={true}
            shouldCloseOnOverlayClick={true}
            shouldCloseOnEsc={true}
          >

            <GlibSingle glibId={this.props.glibId} glibPrivate={this.props.glibPrivate} userToPM={this.props.handle} feedType="glibModal" />

          </ReactModal>

        </div>
      )
    } else {
      // ----------------------------------------------------------------------------------------------
      // regular default main feed 
      // ----------------------------------------------------------------------------------------------
      return (
        <div className="glib text-align-left" id={"glib" + this.props.glibId}>

          <table width="100%" className="glib-table">
            <tbody>
              <tr className="glib-row">
                <td width="15%" className="">

                  <div className="glib-avatar-circle-cropper">
                    <div className="glib-avatar-picture-inner" >
                      <Link to={this.props.cleanHandle}>
                        <img src={this.state.avatarSrc} alt={this.props.handle + "'s Avatar"} className="glib-avatar-picture" />
                      </Link>
                    </div>
                  </div>

                </td>
                <td className="glib-cell"   >

                  {this._renderGlibUserControl(this.props.glibId)}

                  <div>
                    <div><span className="glibr-font font-upsize"><Link to={this.props.cleanHandle}>{this.props.cleanHandle}</Link></span>
                      <span className="glib-handle"> - <Link to={this.props.cleanHandle}>{this.props.author}</Link> -
                                      {this.props.glibdate}</span>
                    </div>

                    <hr className="style-four-purple" />

                    <div onClick={this.handleOpenModal.bind(this, 'glibSingle')} >
                            {/* post content */}
                          <div className="glib-text" dangerouslySetInnerHTML={{ __html: this.props.glibtext }} />
                          {this._renderQuotedPost()}

                          <img src={this.state.glibPictureSrc} className={glibPhotoClassname} alt='glib pic' />
                            {/* end post content */}
                    </div>
                    
                  </div>
                </td>

              </tr>

              <tr>
                <td>
                  <div className="text-align-center"><span className="glib-cost">&cent;{this.props.glibcost}</span></div>
                </td>
                <td className="text-align-right">

                  {this._renderGlibFooter()}


                </td>
              </tr>

            </tbody>
          </table>


          <ReactModal
            isOpen={this.state.showModal}
            onRequestClose={this.handleCloseModal.bind(this)}
            contentLabel="glibr modal"
            className="glibr-modal"
            ariaHideApp={false}
            shouldFocusAfterRender={true}
            shouldCloseOnOverlayClick={true}
            shouldCloseOnEsc={true}
          >

            {/* 
              <GlibSingle glibId={this.props.glibId} glibPrivate={this.props.glibPrivate} userToPM={this.props.handle} feedType="glibModal" /> */}
            {this.state.modalContentItem ? this._renderModalContent(this.state.modalContentItem) : null}

          </ReactModal>

        </div>

      );

    }
  }



  _renderModalContent() {
    if (this.state.modalContentItem === 'glibSingle') {
      return (
        <GlibSingle glibId={this.props.glibId} glibPrivate={this.props.glibPrivate} userToPM={this.props.handle} feedType="glibModal" />
      )
    } else if (this.state.modalContentItem === 'shareGlib') {
      return (
        <ShareGlibDialog glibId={this.props.glibId} />
      )
    } else {
      return (
        <span>no modal</span>
      )
    }

  }

  _renderQuotedPost() {
    if (this.props.quotedGlibID !== null && this.props.quotedGlibID.length > 0) {
      return (
        <div>
          <br />
          <GlibSingle feedType="glibReturnReGlib" feedLocation="reglibList" glibId={this.props.quotedGlibID} />

        </div>
      );
    } 

  }

  /*---------------------------------------------------------------------------------------------
  * _renderGlibFooter
  * all the buttons and controls at the foot of the glib
  * this will allow us to not render it for certain feeds - 
  * eg. secondary feed location on side of main page
  ----------------------------------------------------------------------------------------------*/
  _renderGlibFooter() {

    //need to change margin depending on view (single glib view needs 0 margin adjustment)
    let glibButtonCollapseMarginClassname = "containerFullWidth  glib-margin-reduce";
    if (this.props.glibType === "singleGlib") {
      glibButtonCollapseMarginClassname = "containerFullWidth text-align-right";
    }

    // Don't return anything for secondary feed
    if (this.props.feedLocation === "secondaryFeed") {
      return null;

      // Otherwise return the full set of controls in the footer.
    } else if (this.props.glibType === 'glibShare') {
      return (
        <section>
          <hr className="style-five-purple" />
          <div className='text-align-right '>

            <div className="div-no-wrap text-align-right">

              <a href={"/glib?glibId=" + this.props.glibId} className="btn  btn-link" target="_top"><img src="/images/glib-link-icon.png" alt='glib link' /></a>

              <span className="glibr-font font-upsize">glibr</span> <img src="/images/logo.png" alt="logo" width="25px" /></div>

          </div>

          <div className={this.state.glibShowReplyClassname}> {(this.state.viewGlibReplyControl) ? this.glibReplyEditControl : ''}</div>

        </section>
      )

    } else if (this.props.glibType === 'singleGlib') {
      return (
        <section>
          <hr className="style-five-purple" />
          <div className={glibButtonCollapseMarginClassname}>

            <div className="containerGlibButtons">
              <div className="iconGlib">
                <button onClick={() => this._likeGlib()} className="btn  btn-link"><img src={this.state.glibLikeImage} alt='glib like' /></button>
                <span className="tiny-text iconGlibText">{this.state.glibLikeCount}</span>
              </div>

              <div className="iconGlib">
                <button onClick={() => this._unLikeGlib()} className="btn  btn-link"><img src={this.state.glibUnlikeImage} alt='glib unlike' /></button>
                <span className="tiny-text iconGlibText">{this.state.glibUnlikeCount}</span>
              </div>

              <div className="iconGlib">
                <button onClick={this.handleOpenModal.bind(this, 'shareGlib')} className="btn  btn-link"><img src="/images/glib-share-icon.png" alt='glib share' /></button>
              </div>

              <div className="buttonGlib">
                {this._renderReplyReGlibButtons()}
              </div>

            </div>
          </div>

          <div className={this.state.glibShowReplyClassname}> {(this.state.viewGlibReplyControl) ? this.glibReplyEditControl : ''}</div>

        </section>
      )

    } else {
      return (
        <section>
          <hr className="style-five-purple" />
          <div className={glibButtonCollapseMarginClassname}>
            <div className="containerGlibButtons">
              <div className="iconGlib text-align-center">
                <button onClick={() => this._likeGlib()} className="btn  btn-link"><img src={this.state.glibLikeImage} alt='glib like' /></button>
                <br />
                <span className="tiny-text iconGlibText">{this.state.glibLikeCount}</span>
              </div>
              <div className="iconGlib">
                <button onClick={() => this._unLikeGlib()} className="btn  btn-link"><img src={this.state.glibUnlikeImage} alt='glib unlike' /></button>
                <br />
                <span className="tiny-text iconGlibText">{this.state.glibUnlikeCount}</span>
              </div>
              <div className="iconGlib">
                <a href={"/glib?glibId=" + this.props.glibId} className="btn  btn-link"><img src="/images/glib-link-icon.png" alt='glib link' /></a>
              </div>
              <div className="iconGlib">

                <button onClick={this.handleOpenModal.bind(this, 'shareGlib')} className="btn  btn-link"><img src="/images/glib-share-icon.png" alt='glib share' /></button>
              </div>

                {this._renderReplyReGlibButtons()}
                
            </div>
          </div>

          <div className={this.state.glibShowReplyClassname}> {(this.state.viewGlibReplyControl) ? this.glibReplyEditControl : ''}</div>

        </section>
      )

    }

  }

  /*---------------------------------------------------------------------------------------------
  * _renderReplyReGlibButtons
  * just the reply and reGlib bottons.  We hide these for the reglib display.
  ----------------------------------------------------------------------------------------------*/
  _renderReplyReGlibButtons() {
    if (this.props.feedType === "glibReturnReGlib") {
      return null;
    } else {
      return (
     

          <table className="table-fix-overflow">
            <tbody>
              <tr>
                <td className="text-align-center">
                  <button className="glib-button" onClick={() => this._renderReplyToGlib('reGlib')}>
                              <span className="glibr-font-white">re-glib</span>
                            </button>
                            <br />
                            <span className="tiny-text ">{this.props.glibReglibCount}</span>
                </td>

                <td className="text-align-center">
                 <button className="glib-button" onClick={() => this._renderReplyToGlib('reply')}>
                            <span className="glibr-font-white">reply</span>
                          </button>
                          <br />
                            <span className="tiny-text " onClick={this.handleOpenModal.bind(this, 'glibSingle')} >{this.props.glibReplyCount}</span>
                </td>
              </tr>
            </tbody>
          </table>
         
      )
    }

  }

  _renderReplyToGlib(glibReplyType) {

    this.setState({ viewGlibReplyControl: true });
    //glibType
    this.glibReplyEditControl = <GlibEditControl
      parentID={this.props.glibId}
      glibReplyType={glibReplyType}
      glibType={this.props.glibType}
      glibEditClass='glib-edit-clean'
      onCancel={this._cancelReplyPost.bind(this)}
      onComplete={this._completeReplyPost.bind(this)}
      reloadFeed={this._reloadGlibFeed.bind(this)}
      glibPrivate={this.props.glibPrivate}
      glibPrivateUserId={this.props.glibPrivateUserId}
      userToPM={this.props.handle}

    />;

  }

  _cancelReplyPost(e) {
    e.preventDefault();

    this.setState({
      viewGlibReplyControl: false,
    });
    this.glibReplyEditControl = "";
  }

  // bubble up a reglib
  _reloadGlibFeed() {
    this.props.reloadFeedBubble();

  }

  _completeReplyPost() {

    if (this.props.glibType === "singleGlib") {
      this.props.onCompleteRenderReplies(true);
    }
    //onCompleteRenderReplies

  }

  _renderGlibUserControl(glibId) {

    let sameUser = false;

    if (this.props.user.currentUserHandle === this.props.cleanHandle) {
      sameUser = true
    }


    //  inside a group, because we want to track group delete differently.
    if (this.props.groupId !== null) {
      // check for group admin or owner

      if (this.props.groupUserType === 'owner' || this.props.groupUserType === 'admin') {
        return (
          <div className="dropdown float-right">
            <span className="" data-toggle="dropdown"><img src="/images/glib-hamburger.png" alt="glib user control" /></span>
            <ul className="dropdown-menu ">
              <li className="glib-font  "><button className="glib-button" onClick={() => this._deleteGlib('group')}>delete glib</button></li>
            </ul>
          </div>
        );
      }

    }

    // check for admin first (but not same user)
    if (this.props.authenticate.currentUserRole === 'admin' && !sameUser) {
      // check for admin 
      return (
        <div className="dropdown float-right">
          <span className="" data-toggle="dropdown"><img src="/images/glib-hamburger.png" alt="glib user control" /></span>
          <ul className="dropdown-menu ">
            <li className="glib-font  "><button className="glib-button" onClick={() => this._deleteGlib('admin')}>delete glib</button></li>
          </ul>
        </div>
      );
    }

    // check for user 
    if (sameUser) {
      return (
        <div className="dropdown float-right">
          <span className="" data-toggle="dropdown"><img src="/images/glib-hamburger.png" alt="glib user control" /></span>
          <ul className="dropdown-menu ">
            <li className="glib-font  "><button className="glib-button" onClick={() => this._deleteGlib('user')}>delete glib</button></li>
          </ul>
        </div>
      );
    }


  }

  _likeGlib() {
    ///glibs/glibaddlike/:handle/:glibid'
    var feedQuery = '';

    if (!this.state.userLikesGlib) {
      feedQuery = `/glibs/glibaddlike/${this.props.glibId}/${this.props.user.currentUserHandle}`;
    } else {

      feedQuery = `/glibs/glibremovelike/${this.props.glibId}/${this.props.user.currentUserHandle}`;
    }

    RestClient.fetchGet(feedQuery, (response) => {
      if (response.success) {

        if (!this.state.userLikesGlib) {
          this.setState({
            glibLikeCount: response.glibLikes.length,
            //change like hand image      
            glibLikeImage: '/images/glib-like-done-icon.png',
            // set user like boolean
            userLikesGlib: true
          });
        } else {

          this.setState({
            glibLikeCount: response.glibLikes.length,
            //change like hand image      
            glibLikeImage: '/images/glib-like-icon.png',
            // set user like boolean
            userLikesGlib: false
          });
        }


      }
    },
      (err) => {
        console.log(err);
      });
  }

  _unLikeGlib() {
    ///glibs/glibaddunlike/:handle/:glibid'
    var feedQuery = '';

    if (!this.state.userUnlikesGlib) {
      feedQuery = `/glibs/glibaddunlike/${this.props.glibId}/${this.props.user.currentUserHandle}`;
    } else {

      feedQuery = `/glibs/glibremoveunlike/${this.props.glibId}/${this.props.user.currentUserHandle}`;
    }

    RestClient.fetchGet(feedQuery, (response) => {
      if (response.success) {

        if (!this.state.userUnlikesGlib) {
          this.setState({
            glibUnlikeCount: response.glibUnlikes.length,
            //change like hand image      
            glibUnlikeImage: '/images/glib-unlike-done-icon.png',
            // set user like boolean
            userUnlikesGlib: true
          });
        } else {

          this.setState({
            glibUnlikeCount: response.glibUnlikes.length,
            //change like hand image      
            glibUnlikeImage: '/images/glib-unlike-icon.png',
            // set user like boolean
            userUnlikesGlib: false
          });
        }
      }
    },
      (err) => {
        console.log(err);
      });
  }

  _deleteGlib(deleteType) {
    ///glibs/glibdelete/:glibid

    var feedQuery = `/glibs/glibdelete/${this.props.glibId}/${deleteType}/${this.props.user.currentUserHandle}`;
    RestClient.fetchGet(feedQuery, (response) => {
      if (response.success) {
        this.props.reloadFeedBubble();
      } else {
        console.log("Failed to delete glib");
      }
    },
      (err) => {
        console.log("failed to delete glib with error: " + err);
      });
  }

}//end class



const mapStateToProps = function (store) {
  return {
    //glib properties
    glibs: store.glibsState,

    //user properties
    user: store.userState,

    //authenticate properties
    authenticate: store.authState
  };
};

export default connect(mapStateToProps)(Glib);