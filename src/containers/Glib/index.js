import React, { Component } from 'react';
import { Link } from 'react-router';
//var Utilities = require('../../../common/utils.js');
import './style.css';

export default class Glib extends Component {
  constructor(props) {
    super(props);
    this.feedType = this.props.feedType;
    this.glibParentPostId = this.props.glibParentPostId;
  }

  _toggleElement(oEl) {
    if (oEl.classList.contains("show")) {
      oEl.classList.remove('show');
      oEl.classList.add('hide');
    } else if (oEl.classList.contains("hide")) {
      oEl.classList.remove('hide');
      oEl.classList.add('show');
    }
  }

  _glibRespondDisplay(glibID, reGlibString) {
    //outer response container
    let oEl = document.getElementById("glibResponseContainer_"+ glibID);
    //if there is a reglib then there is a reglib
    let oElreglib = document.getElementById("reGlibContainer_" + glibID);
    //and the text container
    let oElText = document.getElementById("reGlibContainerText_"+ glibID);

    if (typeof(oEl) !== 'undefined' && oEl !== null){
      this._toggleElement(oEl);
      if (typeof(oElText) !== 'undefined' && oElText !== null){
        if(reGlibString.length > 0){
          oElText.innerHTML = reGlibString;
          //this._toggleElement(oElreglib);
          oElreglib.classList.remove("hide");
          oElreglib.classList.add("show");
          this._toggleElement(oElText);
        } else {
          oElreglib.classList.remove("show");
          oElreglib.classList.add("hide");
        }
      }
    }
  }


_glibFeedViewResponses(glibID){
    alert(glibID);
    //get glib id

    //pass into method that replaces right sidebar of glibs with replies
  this.props.feedType = "GLIB_REPLIES_TO_PARENT";
  this.props.glibParentPostId = glibID;

}


  render() {
    
    let avatar =this.props.avatar ?this.props.avatar : '/images/logo_NO_profilePic.png';

    return (
      <div className="glib" id="glib">

      <table width="100%" className="glib-table">
      <tbody>
          <tr className="glib-row">
            <td width="15%" className="glib-cell">

            <div className="glib-avatar-circle-cropper">
                  <div className="glib-avatar-picture-inner" >
                        <img src={avatar} alt={this.props.handle + "'s Avatar"}  className="glib-avatar-picture" />
                  </div>
              </div>

            </td>
            <td className="glib-cell">
              <div className=""><Link to={this.props.cleanHandle}>{this.props.author}</Link></div>
              <div className="glib-handle"><Link to={this.props.cleanHandle}>{this.props.handle}</Link></div>
              <div className="glib-date">{this.props.glibdate}</div>
              <div className="glib-text">{this.props.glibtext}</div>
                  
              <span onClick={this._glibFeedViewResponses.bind(this, this.props.glibID)}   className="tiny-text">test click</span>

            </td>

         </tr>
      </tbody>
      </table>



        <div className="row">
          <div className="col-xs-4"><span className="glib-cost">&cent;{this.props.glibcost}</span></div>
          <div className="col-xs-8 text-align-right">
            <button className="glib-button-response glibr-font " id={"glibReply_" + this.props.glibID} onClick={this._glibRespondDisplay.bind(this, this.props.glibID, '')}>reply</button>
            &nbsp;
            &nbsp;
            <button className="glib-button-response glibr-font " id={"glibReglib_" + this.props.glibID} onClick={this._glibRespondDisplay.bind(this, this.props.glibID, this.props.glibtext)}>re-glib</button>
          </div>
        </div>

        <div className="glib-response-container hide" id={"glibResponseContainer_"+ this.props.glibID}>
          <div id={"reGlibContainer_"+ this.props.glibID} className="row glib-reglib-container hide">
            <div className="col-xs-2">
              <img className="glibr-user-avatar" src={this.props.avatar} alt={this.props.handle + "'s Avatar"} />
            </div>
            <div className="col-xs-10">
              <div className="glib-author"><Link to={this.props.cleanHandle}>{this.props.author}</Link></div>
              <div className="glib-handle"><Link to={this.props.cleanHandle}>{this.props.handle}</Link></div>
              <div className="glib-date">{this.props.glibdate}</div>

              <div id={"reGlibContainerText_"+ this.props.glibID}></div>
            </div>
          </div>
          <textarea placeholder="What's on your mind?" className="response-glib-body" id={"glibResponse_"+ this.props.glibID}></textarea>
          <div className="new-glib-toolbar">
            <label className="new-glib-cost-label">pennies:</label>
            <input type="number" id={"reGlibCost_"+ this.props.glibID} className="new-glib-cost" placeholder="1" defaultValue="1" />
            &nbsp;
            <button className="glib-button" onClick={this.props.composeGlib.bind(this, 'reply', this.props.glibID)}>
              <span className="fa fa-bullhorn icon"></span>
              <span className="glibr-font-white">glib</span>
            </button>



          </div>
        </div>
      </div>
    );
  }
}
