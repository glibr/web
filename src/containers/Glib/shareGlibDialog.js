import React, { Component } from 'react';
import html2canvas from 'html2canvas'
import './style.css';
import '../../global.css';

export default class ShareGlibDialog extends Component {
      constructor(props) {
            super(props);

            this.state={
                  sharedGlibWidth:"400px",
                  glibCopyString:null,
                  glibURL: `${window.location.protocol}//${window.location.host}/shareglib?glibId=${this.props.glibId}`,
            }

      }//end constructor

      componentDidMount(){

            this.setState({
                  glibCopyString: `<iframe src="${this.state.glibURL}" onload='javascript:(function(o){o.style.height=(o.contentWindow.document.body.scrollHeight+5)+"px";}(this));' style="height:20px;width:${this.state.sharedGlibWidth};border:none;overflow:hidden;"></iframe>`,
            
            })

      }

      // shouldComponentUpdate(nextProps, nextState) {
      //       return nextState.sharedGlibWidth != this.state.sharedGlibWidth;
      //   }

      render() {
            return (
                  <div >

                        {this._renderShareControl()}

                  </div>

            )
      }

      copyToClipboard = (e) => {
            this.textArea.select();
            document.execCommand('copy');
            // This is just personal preference.
            // I prefer to not show the the whole text area selected.
            e.target.focus();

      };

      copyGIFToClipboard = (e) => {

            console.log("in");
            console.log(this.props.glibId);
            console.log(document.getElementById(`glib${this.props.glibId}`));

            html2canvas(document.getElementById(`glib${this.props.glibId}`) , {                  
                  onrendered: function (canvas) {
                        console.log("in html 2 canvas");
                        document.getElementById("thismodal").appendChild(canvas);
                  },
                  width:320,
                  height:220
              })
      };

      _renderShareControl() {
            if (this.props.glibId !== null && this.props.glibId.length >= 0 && this.state.glibCopyString) {


                  return (
                        <div id="thismodal">
                              <div>
                                    <span className="glibr-font">share the glib</span>
                                    <button onClick={this.copyToClipboard} className="glib-button float-right" >  <span className="glibr-font-white">copy glib HTML to clipboard</span></button>                                    
                                    <button onClick={this.copyGIFToClipboard} className="glib-button float-right" id="joe" >  <span className="glibr-font-white">copy glib gif to clipboard</span></button>

                              </div>

                              <textarea rows="4"  className="container-full-width" 
                                    ref={(textarea) => this.textArea = textarea} defaultValue={this.state.glibCopyString}
                              />

                              <div className="container-full-width ">
                                    <table className="table-align-right table-no-padding"  >
                                          <tbody >
                                                <tr className="valign-bottom" >
                                                      <td className="valign-top" >set width of glib:</td>
                                                      <td className="valign-bottom" ><input type="radio" name="glibWidth" value="300px"  onClick={this._setSharedGlibWidth.bind(this, '300px')} /><label>300px</label></td>
                                                      <td><input type="radio" name="glibWidth" value="400px"   onClick={this._setSharedGlibWidth.bind(this, '400px')} /><label>400px</label></td>
                                                      <td><input type="radio" name="glibWidth" value="600px"  onClick={this._setSharedGlibWidth.bind(this, '600px')} /><label >600px</label></td>
                                                      <td>  <input type="radio" name="glibWidth" value="100%"  onClick={this._setSharedGlibWidth.bind(this, '100%')} /><label >100%</label></td>
                                                </tr>
                                          </tbody>
                                    </table>
                              </div>



                        </div>
                  )

            } else {
                  return (
                        <span>no glib id present</span>
                  )
            }
      }


      _setSharedGlibWidth(thisWidth){

            this.textArea.value = `<iframe src="${this.state.glibURL}" onload='javascript:(function(o){o.style.height=(o.contentWindow.document.body.scrollHeight+5)+"px";}(this));' style="height:20px;width:${thisWidth};border:none;overflow:hidden;"></iframe>`;
      }


}//end class

