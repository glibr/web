import React, { Component } from 'react';
import { connect } from 'react-redux';
import GLOBALS from "../../common/globals.js";
import Glib from '../Glib/glib.js';
import EnvironmentSettings from "../../common/environmentsettings.js";
import ProfileWidget from '../../components/ProfileWidget';

import Helper from '../../common/helpers';
let parseMentions = require('parse-mentions');


// glibr Components

// Libraries and constants
let envSettings = new EnvironmentSettings();

export class Search extends Component {
  constructor(props) {
    super(props);

    this.searchRef = React.createRef();

    this.state = {
      searchQuery: null,
      searchResultFeed: null,
      searchResultSuccess: false,
      searchFeedRendered: null,
    }

  }//end constructor


  async componentDidMount() {

    this.setState({
      searchQuery: escape(this.props.location.query.q),
    })

    try {
      const response = await this.setSearchResult(escape(this.props.location.query.q));

      if (response.success) {
        await this.buildGlibFeed(response.searchResults);
      }
    } catch (err) {
      this.setState({
        searchFeedRendered: 'no results',
      });
    }

  }


  async setSearchResult(searchQuery) {
    try {

      const settings = {
        credentials: 'include',
        method: 'GET',
      };
      const apikey = `?apikey=${GLOBALS.APIKEY}`
      const response = await fetch(`${envSettings.GLIB_API_URI()}/search/${searchQuery}${apikey}`, settings);

      if (response.status === 200) {
        const responseJSON = await response.json();

        if (responseJSON.success) {
          
          this.setState({
            searchResultSuccess: true,
          })
          return {
            success: true,
            searchResults: responseJSON.searchResults,
          }
        } else {
          this.setState({
            searchResultSuccess: false,
          })
          return {
            success: false,
            searchResults: null,
          }

        }
      } else {

        this.setState({
          searchResultSuccess: false,
        })
        return {
          success: false,
          searchResults: null,
        }

      }

    } catch (err) {
      console.log(err);
      return {
        success: false,
        searchResults: null,
      }
    }

  }

  async buildGlibFeed(searchResults) {
    let glibFeed = [],
      gPicture = "",
      iter = 1000;

    searchResults.forEach(glib => {
      // Check to see if we have a picture as part of the glib.
      // Defensive coding, check for existence first.
      if (glib.glibPictures === undefined || glib.glibPictures === null || glib.glibPictures.length < 1) {
        gPicture = "";
      } else {
        gPicture = glib.glibPictures[0];
      }

      glibFeed.push(
        <Glib glibId={glib._id}
          avatar={glib.avatar}
          author={glib.author}
          handle={glib.handle}
          cleanHandle={glib.cleanHandle}
          glibdate={glib.glibDate}
          glibFullDate={glib.glibFullDate}
          glibcost={glib.cost}
          glibtext={glib.glibPost}
          glibPicture={gPicture}
          key={iter + '-' + glib._id}
          // Props passed in
          parentGlibID={glib.parentPost}
          quotedGlibID={glib.quotedPost}
          //  feedType={this.props.feedType}
          // feedLocation={this.props.feedLocation}
          glibType={this.gType}
          glibLikes={glib.glibLikes}
          glibUnlikes={glib.glibUnlikes}
          glibReglibCount={glib.glibReglibCount}
          glibReplyCount={glib.glibReplyCount}
          // reloadFeedBubble={this._reloadGlibFeedBubble}
          // groups data - if there is a groupId pass in groupsuser type too
          groupId={glib.groupId}
          // groupUserType={this.props.groupUserType}
          // private message attributes
          glibPrivate={glib.glibPrivate}
          glibPrivateUserId={glib.glibPrivateUserId}
          glibPMToHandle={glib.glibPMToHandle}
        />
      );
      iter++;
    });

    this.setState({
      searchFeedRendered: glibFeed
    })

  }


  async _processSearch() {
    var node = this.searchRef.current;

    // check for  mentions
    if (node.value.length > 0) {
      //short curcuit search if there is an @mention in the search box
      //regardless of length of search
      var mentions = parseMentions(node.value);
      if (mentions.matches.length > 0) {
        // go to the first person.
        window.location.href = '/' + mentions.matches[0].name;
      }

      // shorten string
      let searchString = escape(node.value.substring(0, GLOBALS.CONSTANTS.SEARCH_STRING_LENGTH));

      // clean and check for bad words
      const checkSearchString = await Helper.checkForBadWords(searchString);

      // success? go to search results page
      if (checkSearchString.success) {

        this.setState({
          searchQuery: escape(checkSearchString.cleanedString),
        })

        const response = await this.setSearchResult(escape(checkSearchString.cleanedString));

        if (response.success) {
          await this.buildGlibFeed(response.searchResults);
        }

      } else {
        console.log(checkSearchString.foundBadText);
      }

    }
  }



  render() {

    
    let isValidUserState = false;

    // check that user state is set
    if (!this.props.authenticate.userSetStateDone)
    // This value gets set in the header to note when the other redux user state values are set.
    {
      return (<div>patience, the squirrels are working as fast as they can. yo.</div>);

    } else {
      // User redux state set

      if (!this.props.authenticate.userLoggedIn) {

        window.location = '/login';

      } 
      
      
      if (this.props.authenticate.currentUserStatus === "active"){
        isValidUserState = true;
      } else if (this.props.authenticate.currentUserStatus === 'active-provisional'){
        isValidUserState = true;
      }
        
      if(!isValidUserState){
        window.location = '/userproblem';

      } else {
        if (!this.state.searchFeedRendered) {
          return (
            <div>
              <div className="form-group row">

                <div className="d-none d-sm-block col-sm-3">
                  <ProfileWidget
                    currentUser={true}
                    profilePicture={this.props.user.currentUserProfilePicture}
                    userId={this.props.user.currentUserId}
                    handle={this.props.user.currentUserHandle}
                    firstName={this.props.user.currentUserFirstName}
                    numberGlibs={this.props.user.currentUserNumberGlibs}
                    numberFollows={this.props.user.currentUserNumberFollows}
                    numberFollowers={this.props.user.currentUserNumberFollowers}
                    numberOfDoubled={this.props.user.currentUserNumberOfDoubled}

                  />
                </div>

                <div className="col-xs-12 col-sm-9 col-md-6">


                  <div className="container-green-border">
                    <table>
                      <tbody>
                        <tr>
                          <td>
                            <h4>Search Text: </h4>
                          </td>
                          <td>
                            <input type="text" placeholder="Search..." id="glibrSearch" ref={this.searchRef} />
                            <input type="button" id="glibrSearchButton" value="go" onClick={() => { this._processSearch() }} />
                          </td>
                        </tr>
                      </tbody>
                    </table>

                    {this.props.location.query.q}
                    <hr className="style-two" />

                  </div>

                </div>

                <div className="col-xs-12 col-sm-3">
                  <div className="form-elements-container  d-none d-md-block">

                    {/* <GlibFeed feedType={this.state.secondaryFeedType} numGlibs="4" feedLocation="secondaryFeed" /> */}

                  </div>
                </div>
              </div>
            </div>
          )
        } else {
          return (
            <div>
              <div className="form-group row">

                <div className="d-none d-sm-block col-sm-3">
                  <ProfileWidget
                    currentUser={true}
                    profilePicture={this.props.user.currentUserProfilePicture}
                    userId={this.props.user.currentUserId}
                    handle={this.props.user.currentUserHandle}
                    firstName={this.props.user.currentUserFirstName}
                    numberGlibs={this.props.user.currentUserNumberGlibs}
                    numberFollows={this.props.user.currentUserNumberFollows}
                    numberFollowers={this.props.user.currentUserNumberFollowers}
                    numberOfDoubled={this.props.user.currentUserNumberOfDoubled}

                  />
                </div>

                <div className="col-xs-12 col-sm-9 col-md-6">


                  <div className="container-green-border">
                    <table className="div-full-width">
                      <tbody>
                        <tr>
                          <td>
                            <h4>Search Text: </h4>
                          </td>
                          <td className="text-align-right">
                            <input type="text" placeholder="Search..." id="glibrSearch" ref={this.searchRef} />
                            <input type="button" id="glibrSearchButton" value="go" onClick={() => { this._processSearch() }} />
                          </td>
                        </tr>
                      </tbody>
                    </table>

                    {this.props.location.query.q}
                  </div>

                  <br />
                  <div>{this.state.searchFeedRendered}</div>

                </div>

                <div className="col-xs-12 col-sm-3">
                  <div className="form-elements-container  d-none d-md-block">

                    {/* <GlibFeed feedType={this.state.secondaryFeedType} numGlibs="4" feedLocation="secondaryFeed" /> */}

                  </div>
                </div>
              </div>
            </div>

          )

        }
      }
    }
  }// end class
}

const mapStateToProps = function (store) {
  return {
    //glib properties
    glibs: store.glibsState,

    //user properties
    user: store.userState,

    //authenticate properties
    authenticate: store.authState
  };
};

export default connect(mapStateToProps)(Search);