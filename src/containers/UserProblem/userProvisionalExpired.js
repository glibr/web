
import React, { Component } from 'react';
import { Link } from 'react-router';
import GLOBALS from "../../common/globals.js";



export default class UserProvisionalExpired extends Component {
   


      render() {

            return (
                  <div className="register-container container">

                        <div className="register-container container">
                              <div className="container-green-border">
                                    <h5 className="text-align-center">Hello {this.props.handle}, sorry for the inconvenience.</h5>
                              </div>
                        </div>

                        <br />

                        <div className="container-green-border">
                        <h5  className="text-align-center">Your free provisional period is over.</h5>
                        <hr className="style-two" />
                              <p className="text-align-center">
                              <img src="/images/hearts-left.png" height="50px" alt="hearts" />  You are seeing this page because you've posted enough glibs that you have exceeded the provisional amount of {GLOBALS.CONSTANTS.GLIB_PROVISIONAL_PENNY_LIMIT} pennies.</p>

                              <p  className="text-align-center"><b>What do you need to do now?  </b>
                              <br />
                              Well to continue glib'in, you'll need to get a Credit Card associated with your account.</p>

                              <p className="text-align-center">
                                    Click on this <b><Link to="/register" className="font-upsize">link</Link></b> to return to the registration process and add a credit card.</p>


                                    <p className="text-align-center">
                                    After you have that done, we'll change your status to fully active and you'll be back in glibness.  :) <img src="/images/hearts-right.png" height="50px"  alt="hearts" /></p>
                           

                              <hr className="style-two" />
                              <div className="text-align-center">Here's a snorkler to make you happy.<br /><img src="/images/fun/snorkler.jpg" className="container-full-width"  alt="snorkler" /></div>
                              <hr className="style-two" />
                              <table width="100%">
                                    <tbody>
                                          <tr>
                                                <td>
                                                      <span className="glibr-font-black">Thanks</span>
                                                </td>
                                                <td className="text-align-right">
                                                      <Link to="/login">back to login screen</Link>
                                                </td>
                                          </tr>
                                    </tbody>
                              </table>


                        </div>

                  </div>

            )

      }
} //end class