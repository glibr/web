
import React, { Component } from 'react';
import { Link } from 'react-router';



export default class UserRegisteredInactive extends Component {

      render() {

            return (
                  <div className="register-container container">

                        <div className="register-container container">
                              <div className="container-green-border">
                                    <h5 className="text-align-center">Hello {this.props.handle}, sorry for the inconvenience.</h5>
                              </div>
                        </div>

                        <br />

                        <div className="container-green-border">
                        <h5  className="text-align-center">Registration not complete.</h5>
                        <hr className="style-two" />
                              <p className="text-align-center">
                              You are seeing this page because you have not completed your registration. <br />
                               You will need to go and complete it..</p>

                              <p className="text-align-center">
                                    Click on this <b><Link to="/register">link</Link></b> to return to the registration process where you left off.</p>

                                    <p className="text-align-center">
                                  If there is an issue with you completing the process please feel free to drop us a line at: <a href="mailto:glibr.help@gmail.com" >glibr.help@gmail.com</a></p>

                              <hr className="style-two" />
                              <div className="text-align-center">Here's a beer on the mountain.<br /><img src="/images/fun/beer-mountain.png" alt="beer on the mountain" className="container-full-width" /></div>
                              <hr className="style-two" />
                              <table width="100%">
                                    <tbody>
                                          <tr>
                                                <td>
                                                      <span className="glibr-font-black">Thanks</span>
                                                </td>
                                                <td className="text-align-right">
                                                      <Link to="/login">back to login screen</Link>
                                                </td>
                                          </tr>
                                    </tbody>
                              </table>


                        </div>

                  </div>

            )

      }
} //end class