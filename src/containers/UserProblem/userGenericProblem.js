
import React, { Component } from 'react';
import { Link } from 'react-router';



export default class UserGenericProblem extends Component {
      
      render() {

            return (
                  <div className="register-container container">

                        <div className="register-container container">
                              <div className="container-green-border">
                                    <h5 className="text-align-center">Hello {this.props.handle}, sorry for the inconvenience.</h5>
                              </div>
                        </div>

                        <br />

                        <div className="container-green-border">
                        <h5  className="text-align-center">There is some tricky problem with your account..</h5>
                        <hr className="style-two" />
                              <p className="text-align-center">
                              You are seeing this page because well... we don't really know why exaclty. <br />
                              ...but we really want to find out what it is and fix it.</p>

                              <p  className="text-align-center">First of all click here and see if it just works now: <Link to="/">click this.</Link></p>

                              <p className="text-align-center">Second, if that didn't work, send us an email to: <a href="mailto:glibr.help@gmail.com" >glibr.help@gmail.com</a> and tell us these things.</p>

                              <div className="text-align-center">
                             <ol>
                                   <li>Your handle: {this.props.handle}</li>
                                   <li>Your status: {this.props.userStatus}</li>
                                   <li>Your user Role: {this.props.userRole}</li>
                                   </ol>
                                   
                                   and we'll try and figure out what is going on.
                                   </div>


                              <hr className="style-two" />
                              <div className="text-align-center">Here's a sailboat.<br /><img src="/images/fun/sailboat.png" alt="sailboat" className="container-full-width" /></div>
                              <hr className="style-two" />
                              <table width="100%">
                                    <tbody>
                                          <tr>
                                                <td>
                                                      <span className="glibr-font-black">Thanks</span>
                                                </td>
                                                <td className="text-align-right">
                                                      <Link to="/login">back to login screen</Link>
                                                </td>
                                          </tr>
                                    </tbody>
                              </table>


                        </div>

                  </div>

            )

      }
} //end class