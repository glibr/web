
import React, { Component } from 'react';
import { Link } from 'react-router';



export default class UserRegisteredInactive extends Component {

      render() {

            return (
                  <div className="register-container container">

                        <div className="register-container container">
                              <div className="container-green-border">
                                    <h5 className="text-align-center">Hello {this.props.handle}, sorry for the inconvenience.</h5>
                              </div>
                        </div>

                        <br />

                        <div className="container-green-border">
                        <h5  className="text-align-center">Account not activated.</h5>
                        <hr className="style-two" />
                              <p className="text-align-center">
                              You are seeing this page because you have not completed your account activation.  You should have an email from the glibr team with an activation token link.  Click on the link in that email to complete your registration.</p>

                              <p className="text-align-center">
                                    <b> *Note:</b> please check your junk mail or spam folder in your email just in case our message  happens to get dropped in there.</p>

                                    <p className="text-align-center">
                                  If you can't find the email or it isn't working feel free to send us an email at: <a href="mailto:glibr.help@gmail.com" >glibr.help@gmail.com</a></p>

                              <hr className="style-two" />
                              <div className="text-align-center">Here's a pina colada on a beach.<br /><img src="/images/fun/pinacolada-beach.png" alt="pina colada" className="container-full-width" /></div>
                              <hr className="style-two" />
                              <table width="100%">
                                    <tbody>
                                          <tr>
                                                <td>
                                                      <span className="glibr-font-black">Thanks</span>
                                                </td>
                                                <td className="text-align-right">
                                                      <Link to="/login">back to login screen</Link>
                                                </td>
                                          </tr>
                                    </tbody>
                              </table>


                        </div>

                  </div>

            )

      }
} //end class