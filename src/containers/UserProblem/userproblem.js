import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';


// Controls
import UserRegisteredInactive from "./userRegisteredInactive"
import UserRegistrationMidProcess from "./userRegistrationMidProcess"
import UserGenericProblem from './userGenericProblem'
import UserProvisionalExpired from './userProvisionalExpired'


/**----------------------------------------------------------------------
 *  User Problem
 * 
 *  User States:
            active	201		can read/write on site
            active-provisional	202		can read/write on site – until penny threshold
            payment-pending	203		can read/write on site
                              
            registration-two-charity 	221		can only access registration page 3
            registration-three-payment	222		
            registered-inactive 	223		can only access activation notice page
                              
            registered-provisional-inactive	231		can only access activation notice page
            provisional-expired	233		over penny threshold
                              
            suspended-payment-failure	241		goes to user problem page and payment widget
            suspended 	242		goes to user problem page with email form

------------------------------------------------------------------------ */
class UserProblem extends Component {

      render() {

            // check that user state is set
            if (!this.props.authenticate.userSetStateDone)
            // This value gets set in the header to note when the other redux user state values are set.
            {
                  return (<div>patience, the squirrels are working as fast as they can.</div>);

            } else {
                  // User redux state set

                  if (!this.props.authenticate.userLoggedIn) {

                        window.location = '/login';

                  } else if(this.props.authenticate.currentUserStatus==='registered-inactive' || this.props.authenticate.currentUserStatus==='registered-provisional-inactive' ){
                   
                        return(
                              <Fragment>
                                 <UserRegisteredInactive handle={this.props.user.currentUserHandle}/>
                              </Fragment>
                        )

                  } else if(this.props.authenticate.currentUserStatus==='provisional-expired'){
                   
                        return(
                              <Fragment>
                                 <UserProvisionalExpired handle={this.props.user.currentUserHandle}/>
                              </Fragment>
                        )

                  }  else if(this.props.authenticate.currentUserStatus!=='registered-inactive' && this.props.authenticate.currentUserStatus!=='active'){
                   
                        return(
                              <Fragment>
                                 <UserRegistrationMidProcess handle={this.props.user.currentUserHandle}/>
                              </Fragment>
                        )

                  } else {

                        return (
                           
                              <Fragment>
                              <UserGenericProblem handle={this.props.user.currentUserHandle} userRole={this.props.authenticate.currentUserRole} userStatus={this.props.authenticate.currentUserStatus} />
                           </Fragment>
                           
                        );

                  }
            }
      }

}//end class


const mapStateToProps = function (store) {
      return {
            //glib properties
            glibs: store.glibsState,

            //user properties
            user: store.userState,

            //authenticate properties
            authenticate: store.authState
      };
};

export default connect(mapStateToProps)(UserProblem);
