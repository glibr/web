import React, { Component, Fragment } from "react";
import { Link } from "react-router";
import { connect } from "react-redux";
import GLOBALS from "../../common/globals.js";
import Helper from "../../common/helpers.js";
import ValidText from "../../components/shared/ValidText.js";
import EnvironmentSettings from "../../common/environmentsettings.js";

import UploadImageControl from "../../components/UploadButton/";

import "./style.css";

let envSettings = new EnvironmentSettings();

export class CreateGroup extends Component {
	constructor(props) {
		super(props);

		this.handleGroupPrivateChange = this.handleGroupPrivateChange.bind(this);
		this.saveGroup = this.saveGroup.bind(this);

		this.state = {
			formReady: false,
			showForm: false,
			createGroupSuccess: false,
			saveButtonText: "create group",
			formName: "Create New Group",
			groupId: null,

			groupNameValue: "",
			groupNameReady: false,

			groupDescriptionValue: "",
			groupDescriptionReady: false,

			groupCategoryValue: "",
			groupCategoryReady: false,

			groupPrivate: false,

			groupPicture: null,
			showGroupPicture: false,
			groupPictureFullPath: null,
			groupPhotoClassname: "group-picture-upload-hide",

			errorGroupMessage: null,
			errorMessageClassName: null,
			tempImageSource: null
		};

		this.uploadButtonResponse = this.uploadButtonResponse.bind(this);

		if (this.props.saveType === "update") {
		}
	}

	async componentDidMount() {
		let currentGroup = null;

		console.log(this.props.saveType);

		if (this.props.saveType === "update") {
			currentGroup = await Helper.getGroupDetails(this.props.groupId);
			
			if (currentGroup.groupFound) {
				this.setState({
					groupId: this.props.groupId,
					groupNameValue: currentGroup.groupData.name,
					groupDescriptionValue: currentGroup.groupData.description,
					groupCategoryValue: currentGroup.groupData.category,
					groupPrivate: currentGroup.groupData.isPrivate,
					tempImageSource:
						envSettings.GLIBR_IMAGE_ROOT_URI() +
						currentGroup.groupData.groupPicture,
					groupPicture: currentGroup.groupData.groupPicture,
					groupPhotoClassname: "group-picture-upload",
					saveButtonText: "update group",
					formName: "Update Group",
					showForm: true,
					formReady: true //turn on button
				});
			} else {
				//failed to load group
				console.log(currentGroup.message);
				//do not set showForm to true.
			}
		} else {
			// not an update - show empty form
			this.setState({
				showForm: true
			});
		}
	}

	//method injected into UploadImageControl
	uploadButtonResponse(response) {
		if (response.success) {
			this.setState({
				tempImageSource: response.tempFIleSource,
				groupPicture: response.fileName,
				groupPhotoClassname: "group-picture-upload"
			});
		}
	}

	captureGroupNameValue = gNameValue => {
		this.setState({ groupNameValue: gNameValue });
	};
	captureGroupNameDirty = gNameDirty => {
		// invert the error flag coming back
		// setting the group name ready value
		this.setState({ groupNameReady: !gNameDirty });

		this.checkFormReady();
	};

	captureGroupDescriptionValue = gDescriptionValue => {
		this.setState({ groupDescriptionValue: gDescriptionValue });
	};
	captureGroupDescriptionDirty = gDescriptionDirty => {
		// invert the error flag coming back
		// setting the group name ready value
		this.setState({ groupDescriptionReady: !gDescriptionDirty });

		this.checkFormReady();
	};

	captureGroupCategoryValue = gCategoryValue => {
		this.setState({ groupCategoryValue: gCategoryValue });
	};
	captureGroupCategoryDirty = gCategoryDirty => {
		// invert the error flag coming back
		// setting the group name ready value
		this.setState({ groupCategoryReady: !gCategoryDirty });

		this.checkFormReady();
	};

	handleGroupPrivateChange(e) {
		this.setState({ groupPrivate: e.target.checked });

		this.checkFormReady();
	}

	// check for form ready
	// this will enable the create group button.
	checkFormReady() {
		if (
			this.state.groupNameReady &&
			this.state.groupDescriptionReady &&
			this.state.groupCategoryReady &&
			this.state.groupPicture
		) {
			this.setState({
				formReady: true
			});
            } else if(this.props.saveType==='update'){
                  this.setState({
				formReady: true
			});

            } else {
			this.setState({
				formReady: false
			});
		}
	}

	render() {
		if (this.state.createGroupSuccess) {
			return (
				<Fragment>
					<div className="text-align-center glibr-font">
						group succesfully created.
					</div>
					<hr className="style-two" />
					<Link to={{ pathname: "group", query: { id: this.state.groupId } }}>
						My New Group
					</Link>
				</Fragment>
			);
		} else if (!this.state.createGroupSuccess && this.state.showForm) {
			return (
				<div className="container-purple-border  text-align-center">
					<span className="glibr-font">{this.state.formName}</span>
					<br />
					<span className="tiny-text-red">
						required fields: name, photo, description, category
					</span>

					<ValidText
						onSetTextValue={this.captureGroupNameValue}
						onSetElementDirty={this.captureGroupNameDirty}
						ElementPlaceHolder="name"
						elementType="text"
						checkForGroupDupe={true}
						textLength="55"
						required={true}
						defaultValue={this.state.groupNameValue}
					/>

					<div className="form-group row">
						<div className="col-12 text-align-center">
							<img
								alt="group pic"
								className={this.state.groupPhotoClassname}
								src={this.state.tempImageSource}
							/>

							<UploadImageControl
								uploadFileResponse={this.uploadButtonResponse}
							/>

							<br />
							<div className="tiny-text-red text-align-right">*required</div>
						</div>
					</div>

					<ValidText
						onSetTextValue={this.captureGroupDescriptionValue}
						onSetElementDirty={this.captureGroupDescriptionDirty}
						ElementPlaceHolder="description"
						elementType="textarea"
						checkForGroupDupe={false}
						textAreaRows="3"
						textLength="1000"
						required={true}
						defaultValue={this.state.groupDescriptionValue}
					/>

					<ValidText
						onSetTextValue={this.captureGroupCategoryValue}
						onSetElementDirty={this.captureGroupCategoryDirty}
						ElementPlaceHolder="category"
						elementType="text"
						checkForGroupDupe={false}
						textLength="55"
						required={true}
						defaultValue={this.state.groupCategoryValue}
					/>

					<div className="form-group row ">
						<div className="col-12  text-align-center div-no-wrap ">
							<span className="dynamic-font ">Private Group: &nbsp;&nbsp;</span>
							<input
								type="checkbox"
								name="groupPrivate"
								className="checkmark"
								checked={this.state.groupPrivate}
								onChange={this.handleGroupPrivateChange}
							/>
						</div>
					</div>

					<div className="form-group row ">
						<div className="col-12  text-align-center">
							<span className={this.state.errorMessageClassName}>
								{this.state.errorGroupMessage}
							</span>
							<button
								className="btn btn-primary"
								onClick={this.saveGroup}
								disabled={!this.state.formReady}
							>
								<span className="glibr-font-white">
									{this.state.saveButtonText}
								</span>
							</button>
						</div>
					</div>
				</div>
			);
		} else {
			return <div>squirrels are in ur base stealin ur nuts.</div>;
		}
	}

	async saveGroup() {
            
		if (this.props.saveType === "update") {
			await this.updateGroup();
		} else {
			await this.createGroup();
		}
	}

	async updateGroup() {
		const validFormValues = await this.validateForm();

		if (!validFormValues) {
                  // failed form validation
                  
		} else {
			this.setState({
				errorMessageClassName: null
			});

			// and we can proceed with updating the group.
			let data = {
				name: this.state.groupNameValue,
				nameLower: this.state.groupNameValue.toLowerCase(),
				description: this.state.groupDescriptionValue,
				category: this.state.groupCategoryValue,
				ownerHandle: this.props.user.currentUserHandle,
				groupPicture: this.state.groupPicture,
				isPrivate: this.state.groupPrivate
			};

			const resUpdateGroup = await Helper.updateGroup(this.props.groupId, data);

			if (resUpdateGroup.success) {
				this.setState({
					createGroupSuccess: true,
					groupId: resUpdateGroup.groupId
				});

				// close modal
				// update list
			} else {
				// update error message

				this.setState({
					errorMessageClassName: "response-container-error",
					errorGroupMessage: resUpdateGroup.message
				});
			}
		}
	}

	async createGroup() {
		const validFormValues = await this.validateForm();

		if (!validFormValues) {
			// failed form validation
		} else {
			this.setState({
				errorMessageClassName: null
			});

			// and we can proceed with creating and saving  the group.
			let data = {
				name: this.state.groupNameValue,
				nameLower: this.state.groupNameValue.toLowerCase(),
				description: this.state.groupDescriptionValue,
				category: this.state.groupCategoryValue,
				ownerHandle: this.props.user.currentUserHandle,
				groupPicture: this.state.groupPicture,
				isPrivate: this.state.groupPrivate
			};

			const resCreateGroup = await Helper.createGroup(data);

			if (resCreateGroup.success) {
				this.setState({
					createGroupSuccess: true,
					groupId: resCreateGroup.groupId
				});

				// close modal
				// update list
			} else {
				// update error message
				this.setState({
					errorMessageClassName: "response-container-error",
					errorGroupMessage: resCreateGroup.message
				});
			}
		}
	}

	async validateForm() {
		let validationMessages = [];
		let errorCountThisPass = 0;

		// Name
		const groupNameExists = await Helper.validateThis(
			"text",
			"exists",
			this.state.groupNameValue
		);
		// validate group name field
		if (!groupNameExists) {
			validationMessages.push(
				`${GLOBALS.STRINGS.GROUPS_EMPTY_FORM_FIELD} - Group Name`
			);
			errorCountThisPass++;
		}
		const groupNameHasBadWords = await Helper.checkForBadWords(
			this.state.groupNameValue
		);
		// validate group name field
		if (groupNameHasBadWords.foundBadText) {
			validationMessages.push(
				`Group Name - ${GLOBALS.STRINGS.TEXT_INPUT_CONTAINS_BAD_WORDS} `
			);
			errorCountThisPass++;
		}
		const groupNameIs55CharactersLong = await Helper.validateThis(
			"text",
			"text-length-55",
			this.state.groupNameValue
		);
		// validate group name field
		if (!groupNameIs55CharactersLong) {
			validationMessages.push(
				`Group Name - ${GLOBALS.STRINGS.TEXT_INPUT_LENGTH_55} `
			);
			errorCountThisPass++;
		}

		// Description
		const groupDescExists = await Helper.validateThis(
			"text",
			"exists",
			this.state.groupDescriptionValue
		);
		// validate group name field
		if (!groupDescExists) {
			validationMessages.push(
				`Group Description - ${GLOBALS.STRINGS.GROUPS_EMPTY_FORM_FIELD}`
			);
			errorCountThisPass++;
		}
		const groupDescriptionHasBadWords = await Helper.checkForBadWords(
			this.state.groupDescriptionValue
		);
		// validate group name field
		if (groupNameHasBadWords.foundBadText) {
			groupDescriptionHasBadWords.push(
				`Group Description - ${GLOBALS.STRINGS.TEXT_INPUT_CONTAINS_BAD_WORDS} `
			);
			errorCountThisPass++;
		}
		const groupDescriptionIs1000CharactersLong = await Helper.validateThis(
			"text",
			"text-length-1000",
			this.state.groupCategoryValue
		);
		// validate group name field
		if (!groupDescriptionIs1000CharactersLong) {
			validationMessages.push(
				`Group Category - ${GLOBALS.STRINGS.TEXT_INPUT_LENGTH_1000} `
			);
			errorCountThisPass++;
		}

		// Category
		const groupCatExists = await Helper.validateThis(
			"text",
			"exists",
			this.state.groupCategoryValue
		);
		// validate group name field
		if (!groupCatExists) {
			validationMessages.push(
				`Group Category - ${GLOBALS.STRINGS.GROUPS_EMPTY_FORM_FIELD}`
			);
			errorCountThisPass++;
		}
		const groupCategoryHasBadWords = await Helper.checkForBadWords(
			this.state.groupCategoryValue
		);
		// validate group name field
		if (groupCategoryHasBadWords.foundBadText) {
			groupDescriptionHasBadWords.push(
				`Group Category - ${GLOBALS.STRINGS.TEXT_INPUT_CONTAINS_BAD_WORDS} `
			);
			errorCountThisPass++;
		}
		const groupCategoryIs55CharactersLong = await Helper.validateThis(
			"text",
			"text-length-55",
			this.state.groupCategoryValue
		);
		// validate group name field
		if (!groupCategoryIs55CharactersLong) {
			validationMessages.push(
				`Group Category - ${GLOBALS.STRINGS.TEXT_INPUT_LENGTH_55} `
			);
			errorCountThisPass++;
		}

            if(this.props.saveType !== 'update'){
                  const resDupe = await Helper.checkFoGroupDupe(this.state.groupNameValue);
                  //and check for dupe
                  if (resDupe.success) {
                        validationMessages.push(GLOBALS.STRINGS.GROUPS_DUPE_NAME);
                        errorCountThisPass++;
                  }

            }
     

		if (errorCountThisPass > 0) {
			let vMsgs = "";
			// unpack array of messages.
			for (const singleMessage of validationMessages) {
				vMsgs += `${singleMessage}  --- `;
			}

			this.setState({
				errorMessageClassName: "response-container-error",
				errorGroupMessage: vMsgs
			});

			return false;
		} else {
			console.log("form values should be valid");
			return true;
		}
	}
} //end class

//connect is the redux method that connects redux and react//Map the properties of the page to store items
const mapStateToProps = store => {
	return {
		//glib properties
		glibs: store.glibsState,

		//user properties
		user: store.userState,

		//authenticate properties
		authenticate: store.authState
	};
};

//connect is the redux method that connects redux and react
export default connect(mapStateToProps)(CreateGroup);
