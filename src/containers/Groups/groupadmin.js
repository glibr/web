import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import ReactModal from 'react-modal';
import GLOBALS from "../../common/globals.js";
import Helper from "../../common/helpers.js";
import EnvironmentSettings from "../../common/environmentsettings.js";


// glibr Components
import CreateGroup from "./createGroup.js";
import RESTCLIENT from "../../common/restClient.js";
let RestClient = new RESTCLIENT();

// Libraries and constants
let envSettings = new EnvironmentSettings();

export class GroupAdmin extends Component {
      constructor(props) {
            super(props);
            this.state = {
                  groupId: null,
                  groupName: null,
                  userType: null,
                  userList: null,
                  userListType: null,
                  isAdmin: false,
                  isOwner: false,
                  foundUserList: false,
                  groupUserType: null,
                  userListLinkClass: "glibr-font",
                  adminListLinkClass: "glibr-font",
                  ownerListLinkClass: "glibr-font",
                  pendingJoinListLinkClass: "glibr-font",
                  bannedListLinkClass: "glibr-font",
                  showModal: false,
                  modalContentItem: null,
            }


            this.setNewUserType = this.setNewUserType.bind(this);


      }


      async componentDidMount() {

            const gId = await Helper.getUrlParameterByName('id');
            let uType = await Helper.getUrlParameterByName('userType');

            
            await this.getGroupDetails(gId);

            await this.setGroupUserType(gId);

            if (uType === null) {
                  uType = 'user';
                  this.setState({
                        groupId: gId,
                        userListType: uType,
                  })
                  this.loadUsersList(uType);
            }

      }
      
      
      async getGroupDetails(groupId) {

                  try {
                        const mQuery = `/groups/${groupId}`;
      
                        const settings = {
                              credentials: 'include',
                              method: 'GET',
                        };
                        const apikey = `?apikey=${GLOBALS.APIKEY}`
      
                        const response = await fetch(`${envSettings.GLIB_API_URI()}${mQuery}${apikey}`, settings);
      
                        if (response.status === 200) {
                              const responseJSON = await response.json();
      
                              if (responseJSON.success === true) {
      
                                    this.setState({
                                        groupName: responseJSON.groupName,
                                    })
      
                              } else {
                                    this.setState({
                                          groupName: 'group name not found',
                                    })
                              }
                        }
                  } catch (err) {
                        this.setState({
                              groupName: 'group name find error',
                        })
                  }
      }



      async setNewUserType(userType, userId) {
            let formData = new FormData();
            formData.append('apikey', GLOBALS.APIKEY);

            const mQuery = `/groups/${this.state.groupId}/users/${userId}/${userType}`;

            try {
                  const settings = {
                        credentials: 'include',
                        method: 'PUT',
                        body: formData,
                  };

                  const response = await fetch(`${envSettings.GLIB_API_URI()}${mQuery}`, settings)

                  if (response.status === 200) {
                        const responseJSON = await response.json();

                        if (responseJSON.success) {
                              this.loadUsersList(userType);


                              return {
                                    success: true,
                              }
                        } else {
                              // failure branch
                              return {
                                    success: false,
                              }
                        }
                  } else {
                        return {
                              success: false,
                        }
                  }
            } catch (err) {
                  console.log(err)
                  return {
                        success: false,
                        message: err,
                  }
            }
      }



      async setGroupUserType(groupId) {
            // get user type by group id and handle
            const currentUserHandle = localStorage.getItem('currentUserHandle');

            try {
                  const feedQuery = `/groups/getgroupusertype/${currentUserHandle}/${groupId}`;

                  const settings = {
                        credentials: 'include',
                        method: 'GET',
                  };
                  const apikey = `?apikey=${GLOBALS.APIKEY}`

                  const response = await fetch(`${envSettings.GLIB_API_URI()}${feedQuery}${apikey}`, settings);

                  if (response.status === 200) {
                        const responseJSON = await response.json();

                        if (responseJSON.groupUserType.success === true) {

                              this.setState({
                                    groupUserType: responseJSON.groupUserType.userType,
                              })

                        } else {
                              this.setState({
                                    groupUserType: null,
                              })
                        }
                  }
            } catch (err) {
                  this.setState({
                        groupUserType: null,
                  })
            }
      }

      loadUsersList(userType) {

            this.setUserListLinkClass(userType);

            var feedQuery = "";
            //'/groups/:groupId/users/:userType
            console.log("do we have a group id?: " + this.state.groupId);
            feedQuery = "/groups/" + this.state.groupId + "/users/" + userType;

            console.log(feedQuery);

            RestClient.fetchGet(feedQuery, (response) => {
                  if (response.groupUsers.success) {
                        this.setState({
                              userList: response.groupUsers.userList,
                              userListType: userType,
                              foundUsers: true,
                        })
                  } else {
                        // no list
                        this.setState({
                              userList: "no users found"
                        })
                  }
            },
                  (err) => {
                        console.log(err);
                  });
      }

      setUserListLinkClass(userType) {
            console.log(userType)
            switch (userType) {
                  case 'user':
                        console.log("in user class set");
                        this.setState({
                              userListLinkClass: "glibr-font-purple font-upsize",
                              adminListLinkClass: "glibr-font",
                              ownerListLinkClass: "glibr-font",
                              pendingJoinListLinkClass: "glibr-font",
                              bannedListLinkClass: "glibr-font",
                        })
                        break;
                  case 'owner':
                        this.setState({
                              userListLinkClass: "glibr-font",
                              adminListLinkClass: "glibr-font",
                              ownerListLinkClass: "glibr-font-purple font-upsize",
                              pendingJoinListLinkClass: "glibr-font",
                              bannedListLinkClass: "glibr-font",
                        })
                        break;
                  case 'admin':
                        this.setState({
                              userListLinkClass: "glibr-font",
                              adminListLinkClass: "glibr-font-purple font-upsize",
                              ownerListLinkClass: "glibr-font",
                              pendingJoinListLinkClass: "glibr-font",
                              bannedListLinkClass: "glibr-font",
                        })
                        break;
                  case 'pendingJoin':
                        this.setState({
                              userListLinkClass: "glibr-font",
                              adminListLinkClass: "glibr-font",
                              ownerListLinkClass: "glibr-font",
                              pendingJoinListLinkClass: "glibr-font-purple font-upsize",
                              bannedListLinkClass: "glibr-font",
                        })
                        break;
                  case 'banned':
                        this.setState({
                              userListLinkClass: "glibr-font",
                              adminListLinkClass: "glibr-font",
                              ownerListLinkClass: "glibr-font",
                              pendingJoinListLinkClass: "glibr-font",
                              bannedListLinkClass: "glibr-font-purple font-upsize",
                        })
                        break;

                  default:
                        return null;
            }
      }


      handleOpenModal(thisModalContentItem) {
            this.setState({
                  // if we are a larger screen then open modal
                  modalContentItem: thisModalContentItem,
                  showModal: true,
            });
      }

      handleCloseModal() {
            this.setState({ showModal: false });
      }


      render() {

            if (this.state.userList === null) {
                  return (
                        <div>Stand by, stand by.....</div>
                  )
            } else if (this.state.groupUserType !== 'admin' && this.state.groupUserType !== 'owner') {
                  return (
                        <div className="container-green-border">
                              You are not allowed to manage this group.
                  </div>
                  );

            } else if (this.state.foundUsers) {

                  if (true) {

                        return (
                              <div className="container-green-border">
                                    <div className="text-align-center  div-full-width">
                                          
                                    <h3>Edit group properties: {this.state.groupName}</h3>
                                    <button className="glib-button" onClick={this.handleOpenModal.bind(this, 'group')}>edit</button>
                                    <hr className="style-two" />
                                    <h3>Edit group users status: {this.state.groupName}</h3>

                                    <span className={this.state.userListLinkClass} onClick={this.loadUsersList.bind(this, 'user')}>users</span> |
                                    <span className={this.state.adminListLinkClass} onClick={this.loadUsersList.bind(this, 'admin')}>admin</span> |
                                    <span className={this.state.ownerListLinkClass} onClick={this.loadUsersList.bind(this, 'owner')}>owner</span> |
                                    <span className={this.state.pendingJoinListLinkClass} onClick={this.loadUsersList.bind(this, 'pendingJoin')}>pending join</span> |
                                    <span className={this.state.bannedListLinkClass} onClick={this.loadUsersList.bind(this, 'banned')}>banned</span>
                                         
                                          <table border="1" width="100%"  ><tbody>
                                                <tr className="text-align-center glibr-font-black">

                                                      <td>handle</td>
                                                      <td>id</td>
                                                      <td colSpan="3"></td>

                                                </tr>
                                                {this.state.userList.map(userLine => (

                                                      <tr key={userLine._id} className="tiny-text-black">
                                                            <td>{userLine.handle}</td>
                                                            <td colSpan="2" className="td-nowrap">{userLine.ownerId}{userLine.adminId}{userLine.userId}{userLine.pendingJoinId}{userLine.suspendedId}{userLine.bannedId}</td>
                                                            
                                                            
                                                            <td>
                                                                  {this.returnElevateToAdminButton(userLine.userId, userLine.adminId)}
                                                                  {this.returnEnableMembershipButton(userLine.pendingJoinId)}
                                                            </td>
                                                            <td>{this.returnBanButton(userLine.userId, userLine.bannedId)}</td>
                                                      </tr>
                                                ))}
                                          </tbody></table>
                                    </div>


                                    <div className="glibr-modal-wrapper">
                                          <ReactModal
                                                isOpen={this.state.showModal}
                                                onRequestClose={this.handleCloseModal.bind(this)}
                                                contentLabel="group modal"
                                                className="glibr-modal"
                                                ariaHideApp={false}
                                                shouldFocusAfterRender={true}
                                                shouldCloseOnOverlayClick={true}
                                                shouldCloseOnEsc={true}
                                          >

                                                <CreateGroup saveType='update' groupId={this.state.groupId} />

                                          </ReactModal>

                                    </div>




                              </div>
                        )


                  } else {
                        return (
                              <div>you aren't allowed to see this.</div>
                        );
                  }
            } else {
                  return (
                        <div>no users found: {this.state.userList}</div>
                  )

            }
      }

      returnElevateToAdminButton(userId, adminId) {

            console.log(this.state.userListType)

            if (this.state.userListType === 'user') {
                  return (
                        <Fragment>

                              <button className="glib-button" onClick={() => this.setNewUserType('admin', userId)}>elevate to admin</button>

                        </Fragment>
                  )
            } else if (this.state.userListType === 'admin') {
                  return (
                        <Fragment>

                              <button className="glib-button" onClick={() => this.setNewUserType('user', adminId)}>reduce to user</button>

                        </Fragment>
                  )
            }
      }
      returnEnableMembershipButton(pendingJoinId) {
            if (this.state.userListType === 'pendingJoin') {
                  return (
                  <Fragment>            
                              <button className="glib-button" onClick={() => this.setNewUserType('user', pendingJoinId)}>enable user</button>
                  </Fragment>
                  )
            }
      }
      returnBanButton(userId, bannedId) {
            if (this.state.userListType === 'banned') {
                  return (
                        <Fragment>

                              <button className="glib-button" onClick={() => this.setNewUserType('user', bannedId)}>UN-ban user</button>

                        </Fragment>
                  )
            } else if (this.state.userListType === 'user') {
                  return (
                        <Fragment>

                              <button className="glib-button" onClick={() => this.setNewUserType('banned', userId)}>ban user</button>

                        </Fragment>
                  )
            }
      }

}





const mapStateToProps = function (store) {
      return {
            //glib properties
            glibs: store.glibsState,

            //user properties
            user: store.userState,

            //authenticate properties
            authenticate: store.authState
      };
};

export default connect(mapStateToProps)(GroupAdmin);