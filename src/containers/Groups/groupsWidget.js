import React, { Component } from 'react';
import ReactModal from 'react-modal';
import GLOBALS from "../../common/globals.js";
import HelperMethods from "../../common/helpers.js";
import CreateGroup from "./createGroup.js";

import GroupList from "./groupList.js";


import './style.css';



export default class GroupWidget extends Component {
      constructor(props) {
            super(props);

            this.state = {

                  showModal: false,
                  modalContentItem: null,
            }

      }

      handleOpenModal(thisModalContentItem) {
            this.setState({
                  // if we are a larger screen then open modal
                  modalContentItem: thisModalContentItem,
                  showModal: true,
            });
      }

      handleCloseModal() {
            this.setState({ showModal: false });
      }


      render() {

            console.log("in groups widget: " + this.props.handle);
            return (
                  <div className="container-purple-border">

                        <table className="container-full-width">
                              <tbody>
                                    <tr>
                                          <td >{this.renderHeaderLine()}</td>
                                          <td className="text-align-right td-collapse">
                                                      {this.renderCreateButton()}
                                          </td>
                                    </tr>
                              </tbody>
                        </table>



                        <hr className="style-two" />
                        {this.renderGroupOwnerLine()}
                        <GroupList
                              currentUser={this.props.currentUser}
                              handle={this.props.handle}
                              groupType='owner'
                        />
                        <hr className="style-two" />

                        {this.renderGroupMemberLine()}
                        <GroupList
                              currentUser={this.props.currentUser}
                              handle={this.props.handle}
                              groupType='user'
                        />

                        <div className="glibr-modal-wrapper">
                              <ReactModal
                                    isOpen={this.state.showModal}
                                    onRequestClose={this.handleCloseModal.bind(this)}
                                    contentLabel="group modal"
                                    className="glibr-modal"
                                    ariaHideApp={false}
                                    shouldFocusAfterRender={true}
                                    shouldCloseOnOverlayClick={true}
                                    shouldCloseOnEsc={true}
                              >

                                    <CreateGroup saveType='create' />

                              </ReactModal>

                        </div>

                  </div>

            );
      }

      renderCreateButton(){
            if(this.props.currentUser){
                  return(
                        <button className="glib-button tiny-text-white font-bold" onClick={this.handleOpenModal.bind(this, 'group')}>create</button>
                  )
            } 
      }

      renderHeaderLine(){
            if(this.props.currentUser){
                  return(
                        <h5>
                              your groups
                        </h5>
                  )
            } else{
                  return(
                        <h5>
                              {this.props.handle}'s groups
                        </h5>
                  )
            }

      }

      renderGroupOwnerLine(){
            if(this.props.currentUser){
                  return(
                        
                       <span className="glibr-font">                             
                              groups you own
                        </span>
                  )
            } else{
                  return(
                        <span className="glibr-font">
                             groups {this.props.handle} owns
                        </span>
                  )
            }

      }

      renderGroupMemberLine(){
            if(this.props.currentUser){
                  return(
                        <span className="glibr-font">
                              groups you belong to
                        </span>
                  )
            } else{
                  return(
                        <span className="glibr-font">
                             groups {this.props.handle} belongs to
                        </span>
                  )
            }

      }


      /* ************************************
      * Upload a glib picture
      **************************************/
      async  _uploadGroupPicture(e) {

            this.setState({
                  errorGroupMessage: null
            })

            try {
                  const pictureUploadResponse = await HelperMethods.uploadPicture(e);

                  if (pictureUploadResponse.success) {

                        this.setState({
                              groupPicture: pictureUploadResponse.picture,
                              groupPictureFullPath: pictureUploadResponse.pictureFullPath,
                        });

                  } else {
                        this.setState({
                              errorGroupMessage: pictureUploadResponse.message
                        });
                  }
            } catch (err) {
                  // error 
                  this.setState({
                        errorGroupMessage: GLOBALS.STRINGS.IMAGE_UPLOAD_ERROR,
                  })
            }
      }




}// end class
