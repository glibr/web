import React, { Component } from 'react';
import { connect } from 'react-redux';
import GLOBALS from "../../common/globals.js";
import EnvironmentSettings from "../../common/environmentsettings.js";
import './style.css';

import GroupListLine from "./groupListLine.js";

let envSettings = new EnvironmentSettings();

class GroupList extends Component {
      constructor(props) {
            super(props);
            this.state = {
                  groupsListRendered: 'no groups',

            }
      }

      async componentDidMount() {

             await this.renderGroupList();
      }

      async   renderGroupList() {

            const gListResponse = await this.getGroupList();
            let bgClass = 'container-background-light-green';

            let gListRendered = [],
                  iter = 1000;

            if (!gListResponse.success) {
                  // no feed returned
                  gListRendered = 'this is devoid of groups';

            } else {

            
                  gListResponse.grouplist.forEach(group => {
                        // Check to see if we have a picture as part of the glib.
                        if(bgClass ==='container-background-light-purple'){
                              bgClass='container-background-light-green';
                        } else {
                              bgClass ='container-background-light-purple';
                        }


                        gListRendered.push(
                              <GroupListLine groupId={group.groupId}
                                    groupName={group.groupName}
                                    ownerId={group.ownerID}
                                    bgClassName={bgClass}                                 
                                    key={iter + '-' + group.groupId}
                              />
                        );
                        iter++;
                  });


                  this.setState({
                        groupsListRendered: gListRendered,
                  })

            }

      }



      // HelperMethods.checkFoGroupDupe(checkThisGroupName);
      getGroupList = async () => {

            try {

                  const settings = {
                        credentials: 'include',
                        method: 'GET',
                  };

                  const apikey = `?apikey=${GLOBALS.APIKEY}`

                  const response = await fetch(`${envSettings.GLIB_API_URI()}/groups/getgroupsbyhandle/${this.props.handle}/${this.props.groupType}${apikey}`, settings);

                  if (response.status === 200) {
                        const responseJSON = await response.json();

                        if (responseJSON.groups.success) {

                              return {
                                    success: true,
                                    handle: responseJSON.groups.handle,
                                    usersId: responseJSON.groups.userId,
                                    grouplist:responseJSON.groups.grouplist,
                              }
                        } else {
                              return {
                                    success: false,
                                    message: 'no payload returned',
                              }
                        }
                  } else {
                        return {
                              success: false,
                              message: "failed on response from server",
                        }
                  }
            } catch (err) {
                  
                  return {
                        success: false,
                        message: err,
                  }
            }

      }


      render() {
            return (

                  <div>{this.state.groupsListRendered}</div>

            );
      }
}

const mapStateToProps = function (store) {
      return {
            //glib properties
            glibs: store.glibsState,

            //user properties
            user: store.userState,

            //authenticate properties
            authenticate: store.authState
      };
};


export default connect(mapStateToProps)(GroupList);
