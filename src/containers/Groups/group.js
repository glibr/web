import React, { Component, Fragment } from 'react';
import { Link } from 'react-router';
import { connect } from 'react-redux';
import GLOBALS from "../../common/globals.js";
import Helper from "../../common/helpers.js";
import EnvironmentSettings from "../../common/environmentsettings.js";

// glibr Components
import GlibFeed from '../../containers/GlibFeed/GlibFeed';
import GlibEditControl from '../../containers/Glib/glibEditControl.js';
import ProfileWidget from '../../components/ProfileWidget';
import GroupsWidget from '../../containers/Groups/groupsWidget.js';

// Libraries and constants
let envSettings = new EnvironmentSettings();

export class Group extends Component {
  constructor(props) {
    super(props);

    this._reloadGlibFeed = this._reloadGlibFeed.bind(this);

    this.state = {
      groupId: null,
      groupData: null,
      groupFound: false,
      message: null,
      isGroupMember: false,
      requestMembershipSent: false,
      feedType: 'groupFeed',
      secondaryFeedType: "userFeed",
      feedReload: 1,
      groupUserType: null,
    }

  }

  async componentDidMount() {

    const groupId = await Helper.getUrlParameterByName('id');

    await this.setGroupData(groupId);
    await this.checkGroupMembership(groupId);
    await this.setGroupUserType(groupId);

  }

  /**Check for group membership
   * 
   * the call to the api checks for group membership
   * it will check against all types (user, admin, owner)
   */
  async checkGroupMembership(groupId) {

    try {

      const currentUserHandle = localStorage.getItem('currentUserHandle');

      const settings = {
        credentials: 'include',
        method: 'GET',
      };
      const apikey = `?apikey=${GLOBALS.APIKEY}`
      const response = await fetch(`${envSettings.GLIB_API_URI()}/groups/isgroupmember/${currentUserHandle}/${groupId}${apikey}`, settings);


      if (response.status === 200) {
        const responseJSON = await response.json();

        if (responseJSON.success) {
          this.setState({
            isGroupMember: true,
          });
        } else {
          this.setState({
            isGroupMember: false,
          })
        }
      }
    } catch (err) {
      this.setState({
        isGroupMember: false,
      })
    }
  }

  async  setGroupData(groupId) {

    try {

      const settings = {
        credentials: 'include',
        method: 'GET',
      };
      const apikey = `?apikey=${GLOBALS.APIKEY}`

      const response = await fetch(`${envSettings.GLIB_API_URI()}/groups/getgroupbyid/${groupId}${apikey}`, settings);

      if (response.status === 200) {
        const responseJSON = await response.json();

        if (responseJSON.success) {
          this.setState({
            groupId: groupId,
            groupFound: true,
            groupData: responseJSON.group,
            message: responseJSON.message,
          })

        } else {
          // failure branch
          this.setState({
            groupId: groupId,
            groupFound: false,
            message: responseJSON.message,
          })
        }
      } else {
        this.setState({
          groupId: groupId,
          groupFound: false,
          message: "failed on response from server",
        })

      }
    } catch (err) {
      console.log(err)
      this.setState({
        groupId: groupId,
        groupFound: false,
        message: err,
      })
    }
  }


  async setGroupUserType(groupId) {
    // get user type by group id and handle
    const currentUserHandle = localStorage.getItem('currentUserHandle');

    try {
      const feedQuery = `/groups/getgroupusertype/${currentUserHandle}/${groupId}`;

      const settings = {
        credentials: 'include',
        method: 'GET',
      };
      const apikey = `?apikey=${GLOBALS.APIKEY}`

      const response = await fetch(`${envSettings.GLIB_API_URI()}${feedQuery}${apikey}`, settings);

      if (response.status === 200) {
        const responseJSON = await response.json();        

        if (responseJSON.groupUserType.success === true) {
          
          this.setState({
            groupUserType: responseJSON.groupUserType.userType,
          })

        } else {
          this.setState({
            groupUserType: null,
          })
        }
      }
    } catch (err) {
      this.setState({
        groupUserType: null,
      })
    }
  }


  async joinGroup(userType) {

    console.log(this.state.groupId);

    try {

      let formData = new FormData();
      formData.append('apikey', GLOBALS.APIKEY);

      const settings = {
        credentials: 'include',
        method: 'POST',
        body: formData,
      };

      const response = await fetch(`${envSettings.GLIB_API_URI()}/groups/${this.state.groupId}/users/${this.props.user.currentUserHandle}/${userType}`, settings);

      if (response.status === 200) {
        // const responseJSON = await response.json();

        this.setState({
          isGroupMember: true,
          groupUserType: userType,
        })

      } else {
        console.log("unable to add user to group");
      }
    } catch (err) {
      console.log(err);
    }

  }


  render() {

    
    let isValidUserState = false;


    if (!this.props.authenticate.userSetStateDone) {
      return (
        <div>patience while we get all set.</div>
      )
    } else {

      if (!this.props.authenticate.userLoggedIn) {

        window.location = '/login';

      } 
      
      if (this.props.authenticate.currentUserStatus === "active"){
        isValidUserState = true;
      } else if (this.props.authenticate.currentUserStatus === 'active-provisional'){
        isValidUserState = true;
      }

      
      if(!isValidUserState){
        window.location = '/userproblem';

      } else {

      // if current user is in current group: happy path
      if (this.state.groupFound && this.state.isGroupMember) {

        // Render:
        // Header group info
        // glibEditControl
        // glibFeed for this group.
        console.log("groups: group user type -  " + this.state.groupUserType);

        return (
          <div>


            <div className="form-group row">
              <div className="d-none d-sm-block col-sm-3">
                <ProfileWidget
                  currentUser={true}
                  profilePicture={this.props.user.currentUserProfilePicture}
                  userId={this.props.user.currentUserId}
                  handle={this.props.user.currentUserHandle}
                  firstName={this.props.user.currentUserFirstName}
                  numberGlibs={this.props.user.currentUserNumberGlibs}
                  numberFollows={this.props.user.currentUserNumberFollows}
                  numberFollowers={this.props.user.currentUserNumberFollowers}
                  numberOfDoubled={this.props.user.currentUserNumberOfDoubled}
                />
              </div>

              <div className="col-xs-12 col-sm-9 col-md-6">

                {this.renderGroupHeader()}
                {this.renderGlibEditControl()}
                {this.renderGlibFeed()}

              </div>

              <div className="col-xs-12 col-sm-3">
                <div className="form-elements-container  d-none d-md-block">

                {this.renderAdminLink()}
              
              <br />
              
                <GroupsWidget 
                currentUser={true}
                handle={this.props.user.currentUserHandle}
                />


                  {/* 
              {/* <GlibEditControl reloadFeed={this._reloadGlibFeed} />
  
          <GlibFeed feedType={this.state.feedType} feedReload={this.state.feedReload} numGlibs="50" autoRefresh={true} /> */}

                </div>
              </div>
            </div>
          </div>
        );

      } else if (this.state.groupFound && !this.state.isGroupMember) {
        // Not a member

        //public group - not member - join button right on page.
        if (!this.state.groupData.isPrivate) {

          // Render:
          // Header group info
          // join group control (public)

          return (
            <div>
              <div className="form-group row">
                <div className="d-none d-sm-block col-sm-3">

                  <ProfileWidget
                    currentUser={true}
                    profilePicture={this.props.user.currentUserProfilePicture}
                    userId={this.props.user.currentUserId}
                    handle={this.props.user.currentUserHandle}
                    firstName={this.props.user.currentUserFirstName}
                    numberGlibs={this.props.user.currentUserNumberGlibs}
                    numberFollows={this.props.user.currentUserNumberFollows}
                    numberFollowers={this.props.user.currentUserNumberFollowers}
                    numberOfDoubled={this.props.user.currentUserNumberOfDoubled}
                  />
                </div>

                <div className="col-xs-12 col-sm-9 col-md-6">

                  {this.renderGroupHeader()}


                </div>

                <div className="col-xs-12 col-sm-3">
                  <div className="form-elements-container  d-none d-md-block">
                    right column
  {/* 
{/* <GlibEditControl reloadFeed={this._reloadGlibFeed} />

<GlibFeed feedType={this.state.feedType} feedReload={this.state.feedReload} numGlibs="50" autoRefresh={true} /> */}

                  </div>
                </div>
              </div>
            </div>
          )


        } else {
          // private group - not member - send email to ask for memebership

          // Render:
          // join group control (private)

          return (
            <div>
              <div className="form-group row">
                <div className="d-none d-sm-block col-sm-3">

                  <ProfileWidget
                    currentUser={true}
                    profilePicture={this.props.user.currentUserProfilePicture}
                    userId={this.props.user.currentUserId}
                    handle={this.props.user.currentUserHandle}
                    firstName={this.props.user.currentUserFirstName}
                    numberGlibs={this.props.user.currentUserNumberGlibs}
                    numberFollows={this.props.user.currentUserNumberFollows}
                    numberFollowers={this.props.user.currentUserNumberFollowers}
                    numberOfDoubled={this.props.user.currentUserNumberOfDoubled}
                  />
                </div>


                <div className="col-xs-12 col-sm-9 col-md-6">

                  {this.renderGroupHeader()}


                </div>

                <div className="col-xs-12 col-sm-3">
                  <div className="form-elements-container  d-none d-md-block">
                    right column
  {/* 
{/* <GlibEditControl reloadFeed={this._reloadGlibFeed} />

<GlibFeed feedType={this.state.feedType} feedReload={this.state.feedReload} numGlibs="50" autoRefresh={true} /> */}

                  </div>
                </div>
              </div>
            </div>
          )

        }

      } else {
        // NO GROUP FOUND
        return (
          <div>no group</div>
        );

      }

    }
  }
}




  /** Render Group Header
   *  renders the group title and image
   */
  renderGroupHeader() {

    return (

      <Fragment>
        <div className="container-green-border">
          <div className="container-80pct-left">
            <h4>
              {this.state.groupData.name}
            </h4>
          </div>
          <hr className="style-two" />
          {this.state.groupData.description}
          <span className="group-image-circle-cropper">
            <div className="inner">
              <img
                alt='this is the group'
                className="group-picture"
                src={`${envSettings.GLIBR_IMAGE_ROOT_URI()}${this.state.groupData.groupPicture}`} />
            </div>
          </span>


          {this.renderJoinButton()}

        </div>
      </Fragment>

    );

  }



  /**render Join Button
 * renders the correct join button for public or private groups
 */
  renderJoinButton() {

    if (!this.state.isGroupMember) {
      // if public
      if (!this.state.groupData.isPrivate) {
        return (
          <Fragment>
            <hr />
            <button onClick={this.joinGroup.bind(this, 'user')} className="btn btn-primary" >join here</button>
          </Fragment>
        )
        // else if private
      } else {
        return (
          <Fragment>
            <hr />
            <button onClick={this.joinGroup.bind(this, 'pendingJoin')} className="btn btn-primary" >request membership</button>
          </Fragment>
        )

      }

    }

  }

  /** Render the Glib Edit Control for appropriate usertype */
  renderGlibEditControl() {
    console.log("render check: " + this.state.groupUserType);
    switch (this.state.groupUserType) {
      case 'user':
      case 'owner':
      case 'admin':
        return (
          <Fragment>
            <br />
            <GlibEditControl reloadFeed={this._reloadGlibFeed} groupId={this.state.groupId} />
            <br />
          </Fragment>
        )
      case 'pendingJoin':
          return (
            <Fragment>
              <br />
            <div className="container-green-border">
              Your membership to this group is pending approval from the group owner.    
            </div>
          </Fragment>
          )

      case 'suspended':
      case 'banned':
        return null;
      default:
        return this.state.groupUserType;
    }
	
  }

  /** Render the Glib Feed for appropriate usertype */
  renderGlibFeed() {
    switch (this.state.groupUserType) {
      case 'user':
      case 'owner':
      case 'admin':
        return (
          <Fragment>
            <GlibFeed feedType={this.state.feedType} groupId={this.state.groupId} feedReload={this.state.feedReload} groupUserType={this.state.groupUserType} numGlibs="50" autoRefresh={true} />
          </Fragment>
        )
      case 'pendingJoin':
      case 'suspended':
      case 'banned':
        return null;
      default:
        return null;
		}
    }


 renderAdminLink(){
  switch (this.state.groupUserType) {

    case 'owner':
    case 'admin':
      return (
        <div className="container-green-border text-align-center">
          <Link to={{ pathname: 'GroupAdmin', query: { id: this.state.groupId } }}>Manage This Group</Link>
         </div>
      )
    case 'user':
    case 'pendingJoin':
    case 'suspended':
    case 'banned':
      return null;
    default:
      return null;
  }
 }
    


  //-----------------------------------------------
  // reload the feed 
  //-----------------------------------------------
  _reloadGlibFeed() {

    this.setState({
      feedReload: Date.now()
    });

  }


} //end class


const mapStateToProps = function (store) {
  return {
    //glib properties
    glibs: store.glibsState,

    //user properties
    user: store.userState,

    //authenticate properties
    authenticate: store.authState
  };
};

export default connect(mapStateToProps)(Group);