import React, { Component } from 'react';
import { Link } from 'react-router';

class GroupListLine extends Component {
      constructor(props) {
            super(props);
            this.state = {  }
      }
      render() { 
            const gListRowclass = `${this.props.bgClassName} container-full-width thin-light-border`;
            return ( 
                  <table className={gListRowclass}>
                        <tbody>
                              <tr>
                                    <td className="tiny-text-purple">{this.props.groupName}</td>
                                    <td className="text-align-right tiny-text">
                                    <Link to={{ pathname: 'Group', query: { id: this.props.groupId } }}>
                                                            group page</Link></td>
                              </tr>
                        </tbody>
                  </table>

            );
      }
}
 
export default GroupListLine;