import React, { Component } from 'react';
import { connect } from 'react-redux';

import HeaderUI from "../../components/Header_UI";
import EnvironmentSettings from "../../common/environmentsettings.js";
import RESTCLIENT from "../../common/restClient.js";
import GLOBALS from "../../common/globals.js";

// Import Actions
import {
  setCurrentUserId,
  setCurrentUserNumberGlibs,
  setCurrentUserProfilePicture,
  setCurrentUserHandle,
  setCurrentUserFirstName,
  setCurrentUserLastName,
  setCurrentUserFollowsList,
  setCurrentUserNumberOfFollows,
  setCurrentUserFollowersList,
  setCurrentUserNumberOfFollowers,
  setCurrentUserDoubleFollowsList,
  setCurrentUserNumberOfDoubleFollows,
  setCurrentUserCharity,
  setCurrentUserCharityLogo
} from '../../actions/userActions';

import {
  setUserLoggedIn,
  setCurrentUserRole,
  setCurrentUserStatus,
  setUserStateSetDone

} from '../../actions/authActions';

// Short hand for mapDispatch
// removes the need for the mapdispatch method and the long list of setters
// "actionCreators" is passed as the second parameter in the connect call - see bottom of this module
const actionCreators = {
  //user actions
  setCurrentUserId,
  setCurrentUserNumberGlibs,
  setCurrentUserProfilePicture,
  setCurrentUserHandle,
  setCurrentUserRole,
  setCurrentUserFirstName,
  setCurrentUserLastName,
  setCurrentUserNumberOfFollows,
  setCurrentUserFollowsList,
  setCurrentUserFollowersList,
  setCurrentUserNumberOfFollowers,
  setCurrentUserDoubleFollowsList,
  setCurrentUserNumberOfDoubleFollows,
  setCurrentUserCharity,
  setCurrentUserCharityLogo,
  //auth actions
  setUserLoggedIn,
  setCurrentUserStatus,
  setUserStateSetDone
};



let envSettings = new EnvironmentSettings();
let RestClient = new RESTCLIENT();

class Header extends Component {
  constructor(props) {
    super(props);

    this.state = {
      avatarSrc: '/images/logo_NO_profilePic.png',
      setUserStateSetDone: false,
    };

  }//end constructor


  async componentDidMount() {

    const responseGetCurrentUser = await this.getCurrentUser();

    if (!responseGetCurrentUser.success) {
      this.props.setUserStateSetDone(true);
      localStorage.setItem('isUserActive', false);
      this.props.setUserLoggedIn(false);
      localStorage.setItem('isUserLoggedIn', false);
    } else {
      // got a current user
      this.setState({
        isAuthenticated: true,
        currentUser: responseGetCurrentUser.currentUser
      });
      
      // console.log("props set start - handle: " + responseGetCurrentUser.currentUser.handle);

      this.props.setCurrentUserId(responseGetCurrentUser.currentUser._id);
      this.props.setCurrentUserHandle(responseGetCurrentUser.currentUser.handle);
      // need to set localStorage too for groups
      localStorage.setItem('currentUserHandle', responseGetCurrentUser.currentUser.handle);

      this.props.setCurrentUserRole(responseGetCurrentUser.currentUser.userRoleName);
      // need to set localStorage too for routes.js
      if (responseGetCurrentUser.currentUser.userRoleName === 'admin') {
        localStorage.setItem('isUserAdmin', true);
      } else {
        localStorage.setItem('isUserAdmin', false);
      }

      this.props.setCurrentUserStatus(responseGetCurrentUser.currentUser.userStatusName);
      // need to set localStorage too for routes.js
      if (responseGetCurrentUser.currentUser.userStatusName === 'active' || responseGetCurrentUser.currentUser.userStatusName === 'active-provisional') {
        localStorage.setItem('isUserActive', true);
      } else {
        localStorage.setItem('isUserActive', false);
      }

      this.props.setCurrentUserFirstName(responseGetCurrentUser.currentUser.firstName);
      this.props.setCurrentUserLastName(responseGetCurrentUser.currentUser.lastName);


      // user link counts
      this.props.setCurrentUserNumberOfFollows(responseGetCurrentUser.currentUser.followCount);
      this.props.setCurrentUserNumberOfFollowers(responseGetCurrentUser.currentUser.followerCount);
      this.props.setCurrentUserNumberOfDoubleFollows(responseGetCurrentUser.currentUser.doubleFollowCount);

      this.props.setCurrentUserCharity(responseGetCurrentUser.currentUser.userCharity);
      this.props.setCurrentUserCharityLogo(responseGetCurrentUser.currentUser.userCharityLogo);

      if (responseGetCurrentUser.currentUser.profilePicture) {
        this.setState({
          avatarSrc: envSettings.GLIBR_IMAGE_ROOT_URI() + responseGetCurrentUser.currentUser.profilePicture
        })
        this.props.setCurrentUserProfilePicture(envSettings.GLIBR_IMAGE_ROOT_URI() + responseGetCurrentUser.currentUser.profilePicture);
      }



      const responseGlibCount = await this.getUserGlibCount(localStorage.getItem('currentUserHandle'));
      if (responseGlibCount.success) {
        // console.log("success and count: " + responseGlibCount.glibCount);
        this.props.setCurrentUserNumberGlibs(responseGlibCount.glibCount);
      } else {
        this.props.setCurrentUserNumberGlibs(0);
      }

      const responseFollows = await this.setUserLinkIds(localStorage.getItem('currentUserHandle'), 'follows');
      if (responseFollows.success) {
        this.props.setCurrentUserFollowsList(responseFollows.userlink);
      } else {
        this.props.setCurrentUserFollowsList('empty');
      }

      const responseFollowers = await this.setUserLinkIds(localStorage.getItem('currentUserHandle'), 'followers');
      if (responseFollowers.success) {
        this.props.setCurrentUserFollowersList(responseFollowers.userlink);
      } else {
        this.props.setCurrentUserFollowersList('empty');
      }

      const responseDoubles = await this.setUserLinkIds(localStorage.getItem('currentUserHandle'), 'doubled');
      if (responseDoubles.success) {
        this.props.setCurrentUserDoubleFollowsList(responseDoubles.userlink);
      } else {
        this.props.setCurrentUserDoubleFollowsList('empty');
      }


      // Set user auth and user state
      // There is a lifecycle bug right here - setUserLoggedIn must be below the other  redux setters
      // for some reason (need to investigate) moving it to the top breaks the other setters.
      // makes zero sense as to why
      this.props.setUserLoggedIn(true);
      // need to set localStorage too for routes.js
      localStorage.setItem('isUserLoggedIn', true);

      // and sert the flag for completion.
      this.props.setUserStateSetDone(true);

    }


  }


  async getCurrentUser() {

    try {
      const settings = {
        credentials: 'include',
        method: 'GET',
      };
      const apikey = `?apikey=${GLOBALS.APIKEY}`;
      const mongoQ = `${envSettings.GLIB_API_URI()}/getcurrentuser${apikey}`;

      // console.log("mongoQ for get current user: " + mongoQ);

      const response = await fetch(mongoQ, settings);

      if (response.status === 200) {
        const responseJSON = await response.json();
        // success: true,
        // userlink: responseFollows.userlink,
        if (responseJSON.success) {
         
          return {
            success: true,
            isAuthenticated: true,
            currentUser: responseJSON.currentUser,
          }
        } else {
          return {
            success: false,
            isAuthenticated: false,
            currentUser: null,
          }
        }
      } else {
        return {
          success: false,
          isAuthenticated: false,
          currentUser: null,
        }
      }
    } catch (err) {
      return {
        success: false,
        isAuthenticated: false,
        currentUser: null,
      }
    }
  }



  async getUserGlibCount(handle) {
    try {
      const settings = {
        credentials: 'include',
        method: 'GET',
      };
      const apikey = `?apikey=${GLOBALS.APIKEY}`;
      const mongoQ = `${envSettings.GLIB_API_URI()}/glibs/numberglibsbyhandle/${handle}${apikey}`;

      // console.log("mongoQ for get glib count: " + mongoQ);

      const response = await fetch(mongoQ, settings);

      if (response.status === 200) {
        const responseJSON = await response.json();
        // success: true,
        // userlink: responseFollows.userlink,
        if (responseJSON.success) {
          // console.log("inside set get glib count: " + responseJSON.success)
          return {
            success: true,
            glibCount: responseJSON.glibs,
          }
        } else {
          return {
            success: false,
            glibCount: 0,
          }
        }
      }
    } catch (err) {
      return {
        success: false,
        glibCount: 0,
      }
    }
  }



  async setUserLinkIds(handle, followType) {

    try {

      const settings = {
        credentials: 'include',
        method: 'GET',
      };
      const apikey = `?apikey=${GLOBALS.APIKEY}`;
      const mongoQ = `${envSettings.GLIB_API_URI()}/user/getfollows/${handle}/${followType}${apikey}`;

      // console.log("mongoQ: " + mongoQ);

      const response = await fetch(mongoQ, settings);



      if (response.status === 200) {
        const responseJSON = await response.json();
        // success: true,
        // userlink: responseFollows.userlink,
        if (responseJSON.success) {
          // console.log("inside set user links: " + responseJSON.success)
          return {
            success: true,
            userlink: responseJSON.userlink,
          }
        } else {
          return {
            success: false,
          }
        }
      } else {
        return {
          success: false,
        }
      }

    } catch (err) {
      return {
        success: false,
      }
    }



  }

  render() {
    return (

      <header className="row">
        <HeaderUI
          isAuthenticated={this.state.isAuthenticated}
          currentUserStatus={this.props.authenticate.currentUserStatus}
          currentUserRole={this.props.authenticate.currentUserRole}
          profilePicture={this.props.user.currentUserProfilePicture}
          handle={this.props.user.currentUserHandle}
          onClick={this._logout.bind(this)}
        />
      </header>

    );
  }



  _logout(e) {
    e.preventDefault();

    RestClient.fetchGet('/logout', (response) => {

      if (response.success) {

        localStorage.setItem('isUserLoggedIn', false);
        localStorage.setItem('isUserAdmin', false);
        localStorage.setItem('currentUserHandle', '');

        window.location = '/login';
      }
    },
      (err) => {
        console.log("can't logout" + err);
      });
  }
}

const mapStateToProps = function (store) {
  return {
    //glib properties
    glibs: store.glibsState,

    //user properties
    user: store.userState,

    //authenticate properties
    authenticate: store.authState
  };
};

//connect is the redux method that connects redux and react
export default connect(mapStateToProps, actionCreators)(Header);    