import React, { Component } from "react";


import GroupsWidget from '../../containers/Groups/groupsWidget.js';
import "./style.css";
 
export default class SlideIn extends Component {
  constructor(props) {
    super(props);

    this.state = {
      visible: false	
    };

  }


  toggleSlideIn(){
    this.setState(prevState => ({
      visible: !prevState.visible
    }));
  }

  showSlideIn(){
    this.setState({ visible: true });
   
   document.addEventListener("click", this.hideSlideIn.bind(this), true);
 
  }

  hideSlideIn(){
    document.removeEventListener("click", this.hideSlideIn.bind(this), true);
    this.setState({ visible: false });
  }

  render() { 
    console.log("in slidein: " + this.props.handle);
    return (
    <div className="slide-in">
      <div className={(this.state.visible ? "visible " : "") + this.props.alignment}>
        
         <GroupsWidget 
                currentUser={true}
                handle={this.props.handle}
                />
        
        </div>
    </div>
    );
  }
}
