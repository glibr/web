import React, {Component} from "react";
import { connect } from 'react-redux';

import SlideIn from './slideIn.js';
import './style.css';

class TestContainer extends Component {
  constructor(props) {
    super(props);

   
    this.showLeft = this.showLeft.bind(this);
    this.hideLeft = this.hideLeft.bind(this);
  }

  

  showLeft(){
    this.refs.left.showSlideIn();
  }

  hideLeft(){
    this.refs.left.toggleSlideIn();
  }

  render() {
    console.log(this.props.user.currentUserHandle);
    return (
      <div>
      <button onClick={this.showLeft}>Show Left Menu!</button>
      <button onClick={this.hideLeft}>Hide Left Menu!</button>
  
      <SlideIn ref="left" alignment="left"
      currentUser={true}
      handle={this.props.user.currentUserHandle}
      >
       hello world 1
      </SlideIn>
  
    </div>
    )

  }
}


const mapStateToProps = function (store) {
  return {
    //glib properties
    glibs: store.glibsState,

    //user properties
    user: store.userState,

    //authenticate properties
    authenticate: store.authState
  };
};

export default connect(mapStateToProps)(TestContainer);
