import React, { Component } from 'react';
import { connect } from 'react-redux';
import Cookies from 'universal-cookie';
import PullToRefresh from 'pulltorefreshjs';

// glibr Components
import GlibFeed from '../../containers/GlibFeed/GlibFeed';
import GlibEditControl from '../../containers/Glib/glibEditControl.js';
import ProfileWidget from '../../components/ProfileWidget';
import NavBar from '../../components/NavBar/NavBar';
import GroupsWidget from '../../containers/Groups/groupsWidget.js';

import './style.css';
let cookies = new Cookies();

class Main extends Component {

  constructor(props) {
    super(props);

    this._changeFeedType = this._changeFeedType.bind(this);
    this._reloadGlibFeed = this._reloadGlibFeed.bind(this);

    var defaultFeed = "full";

    if (typeof cookies.get('mainFeed') !== 'undefined') {
      defaultFeed = cookies.get('mainFeed');

    }

    this.state = {
      feedType: defaultFeed,
      secondaryFeedType: "userFeed",
      feedReload: 1,
    }
  }

  componentDidMount() {

    clearInterval(this.props.glibs.intervalTimer_MainGlibFeed);
    clearInterval(this.props.glibs.intervalTimer_UserGlibFeed);
    clearInterval(this.props.glibs.intervalTimer_SecondaryGlibFeed);

    PullToRefresh.init({
      mainElement: 'body',
      triggerElement: 'header',
      // shouldPullToRefresh(){
      //   const scroll = window.scrollY || window.pageYOffset;     
      //   if(scroll<=1){         
      //     return true;
      //   }else{
      //     return false;
      //   }
      // },
      onRefresh() {
          window.location.reload();
      }
  });


  }


  componentWillUnmount(){
        // Don't forget to destroy all instances on unmout
        // or you will get some glitches.
        PullToRefresh.destroyAll();
    }

  render() {

    let isValidUserState = false;

    // check that user state is set
    if (!this.props.authenticate.userSetStateDone)
    // This value gets set in the header to note when the other redux user state values are set.
    {
      return (<div>patience, the squirrels are working as fast as they can. yo.      </div>);

    } else {
      // User redux state set

      if (!this.props.authenticate.userLoggedIn) {

        window.location = '/login';

      } 
      

      console.log("user status: " + this.props.authenticate.currentUserStatus)

      if (this.props.authenticate.currentUserStatus === "active"){
        isValidUserState = true;
      } else if (this.props.authenticate.currentUserStatus === 'active-provisional'){
        isValidUserState = true;
      }
  
      console.log("is valid user: " + isValidUserState);


      if(!isValidUserState){
        //console.log(this.props.authenticate.currentUserStatus);
        window.location = `/userproblem?${this.props.authenticate.currentUserStatus}`;

      } else {

        return (
          <div className="glibr-page-container">
            <NavBar resetGlibFeed={this._changeFeedType} id="glibrNavbar" />

            <div className="form-group row">

              <div className="d-none d-sm-block col-sm-3">
                <ProfileWidget
                  currentUser={true}
                  profilePicture={this.props.user.currentUserProfilePicture}
                  userId={this.props.user.currentUserId}
                  handle={this.props.user.currentUserHandle}
                  firstName={this.props.user.currentUserFirstName}
                  numberGlibs={this.props.user.currentUserNumberGlibs}
                  numberFollows={this.props.user.currentUserNumberFollows}
                  numberFollowers={this.props.user.currentUserNumberFollowers}
                  numberOfDoubled={this.props.user.currentUserNumberOfDoubled}
                  currentUserId={this.props.currentUserId}
                  charityLogoForUser={this.props.user.charityLogoForUser}
                />
              </div>

              <div className="col-xs-12 col-sm-9 col-md-6">
                <div className="form-elements-container">

                  <GlibEditControl reloadFeed={this._reloadGlibFeed} />
             
                  <GlibFeed feedType={this.state.feedType} feedLocation="mainFeed" feedReload={this.state.feedReload} numGlibs="50" pageNumber='1' autoRefresh={true} />

                </div>
              </div>

              <div className="col-xs-12 col-sm-3">
                <div className="form-elements-container  d-none d-md-block">

                <GroupsWidget 
                currentUser={true}
                handle={this.props.user.currentUserHandle}
                />
                     <br />

                     <div className="glibr-font-highlight font-upsize glibfeed-friendly-name text-align-center">your glibs</div>
          <br />
                  <GlibFeed feedType={this.state.secondaryFeedType} numGlibs="4" feedLocation="secondaryFeed" />

                </div>
              </div>
            </div>
          </div>
        );

      }
    }
  }


  //-----------------------------------------------
  // reload the feed 
  //-----------------------------------------------
  _reloadGlibFeed() {

    this.setState({
      feedReload: Date.now()
    });
  }

  //-----------------------------------------------
  // Reset state (and potentially other things)
  // Resetting state will pass down a new value to the glibFeed as its PROP for feedType
  // This will invoke a re-render - we check in glibFeed in the componentDidUpdate method.
  _changeFeedType(feedType) {

    // The Cask and Larder Fix ( The Bar in the Orlando Airport where I solved it )
    // after just a fantastic trip to DeLand Florida - August 2018
    // Solved after SIX months or more of futility.
    // https://stackoverflow.com/questions/24939623/can-i-update-a-components-props-in-react-js/51903015#51903015
    // ridiculous easy solution and not described anywhere
    // the result is that glibFeed may be its own SELF CONTAINED component YYUUUGE!

    this.setState({ feedType: feedType })

    cookies.set('mainFeed', feedType, { path: '/' });

  }

}//end class


const mapStateToProps = function (store) {
  return {
    //glib properties
    glibs: store.glibsState,

    //user properties
    user: store.userState,

    //authenticate properties
    authenticate: store.authState
  };
};

export default connect(mapStateToProps)(Main);
