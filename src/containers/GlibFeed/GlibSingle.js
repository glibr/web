import React, { Component } from 'react';
import { connect } from 'react-redux';
import '../GlibFeed/style.css';

//components
import Glib from '../Glib/glib';
import GlibFeed from '../../containers/GlibFeed/GlibFeed';
import Utilities from "../../common/utils.js";
import RESTCLIENT from "../../common/restClient.js";
// Actions
// Import Actions
import {
  setSingleGlib
} from '../../actions/glibActions';

// Short hand for mapDispatch
// removes the need for the mapdispatch method and the long list of setters
// "actionCreators" is passed as the second parameter in the connect call - see bottom of this module
const actionCreators = {
  setSingleGlib
};

let RestClient = new RESTCLIENT();

// Props passed in
//  -  ==  glib feed type - may be null [full]
//              full | userFeed | mostExpensiveAllTime | mostExpensiveToday
// - autoRefresh == boolean, turns off interval [true]
// - refreshRate == number of seconds to refresh - default 5 seconds
// -  numGlibs == number of glibs in view [50]
// -  thisUserFeedHandle == for user feed
class GlibSingle extends Component {
  constructor(props) {
    super(props);

    this.state = {
      glibFeedRendered: null,
      currentGlibFeed: "nothing",
      singleGlibPage: false,
      glibType: 'singleGlib',
      glibId: null,
      feedType: 'readOnly'
    };

  }//end constructor

  
  componentDidMount(){

    if ( localStorage.getItem('isUserActive') === 'true'){
      this._setGlibFeed("glibSingleView");      
  } else { 
    this._setGlibFeed("readOnly");          
  }
  }

  render() {    

    // check that user state is set
    if (!this.props.authenticate.userSetStateDone)
    // This value gets set in the header to note when the other redux user state values are set.
    {
      return (<div>patience, the squirrels are working as fast as they can.</div>);

    } else {
    
        return (
          <div className="text-align-center">
            {this.state.glibFeedRendered}

            {this._renderReplies(false)}

          </div>
        );
        }           
  }


  //--------------------------------------------------------
  // Set the glib feed 
  //
  // Meat of the module.  Derives the correct feedQuery to use then calls _getSingleGlib to get it.
  _setGlibFeed(feedType) {

    var feedQuery = "";

    const queryParams = {
      feedType: feedType,
      feedLocation: this.props.feedLocation,
      glibId: this.props.glibId,
      numGlibs: this.props.numGlibs || 50,
      feedHandle: this.props.thisUserFeedHandle,
      groupId: this.props.groupId,
      pageNumber: this.props.pageNumber || 1,
    }

    if (this.props.feedType === "glibReturnReGlib") {

      feedQuery = Utilities.common.getFeedQueryFromType(queryParams);

    } else if (this.props.glibId !== null) {
      //pass in glib id inot the feedLocation
      feedQuery = '/glibs/getsingleglib/' + this.props.glibId;

    } else if (Utilities.common.getUrlParameterByName('glibId') !== null) {
      feedQuery = '/glibs/getsingleglib/' + Utilities.common.getUrlParameterByName('glibId')
    }

    this._getSingleGlib(feedQuery, feedType);

  }//end _setGlibFeed




  _getSingleGlib(feedQuery, feedType) {

    RestClient.fetchGet(feedQuery, response => {
      let gPicture = "",
        iter = 1000,
        singleGlib = [];

      if (response.success) {

        // Check to see if we have a picture as part of the glib.
        // Defensive coding, check for existence first.
        if (response.glib.glibPictures === undefined || response.glib.glibPictures === null || response.glib.glibPictures.length < 1) {
          gPicture = "";
        } else {
          gPicture = response.glib.glibPictures[0];
        }

        singleGlib =
          <Glib glibId={this.props.glibId}
            avatar={response.glib.avatar}
            author={response.glib.author}
            handle={response.glib.handle}
            cleanHandle={response.glib.cleanHandle}
            glibdate={response.glib.glibDate}
            glibcost={response.glib.cost}
            glibtext={response.glib.glibPost}
            glibPicture={gPicture}
            key={iter + '-' + response.glib._id}
            // Props passed in
            parentGlibID={response.glib.parentPost}
            quotedGlibID={response.glib.quotedPost}
            feedType={feedType}
            glibType={this.state.glibType}
            onCompleteRenderReplies={this._renderReplies.bind(this)}
            groupId={response.glib.groupId}
            glibPrivate={response.glib.glibPrivate}
            glibPrivateUserId={response.glib.glibPrivateUserId}
          />


        if (singleGlib !== this.state.glibFeedRendered) {
          this.setState({
            glibFeedRendered: singleGlib
          });
        }


      } else {
        this.setState({
          profileEditError: response.message
        });


      }
    })
  }





  //--------------------------------------------------------------------------
  // _renderReplies - show the list of replies below the parent post.
  //
  // prevent unnecessary recursion when reGlibs are shown as part of the post.
  _renderReplies(reRender) {

    let glibId = null;
    if (this.props.glibId !== null) {
      glibId = this.props.glibId;
    } else if (Utilities.common.getUrlParameterByName('glibId') !== null) {
      glibId = Utilities.common.getUrlParameterByName('glibId');
    }
    // don't want to do replies to reglibs inside the single glib screen
    // so check if the feedType is glibReturnReGlib -- which is what it is filled with for reGlib
    // it is empty for the origination passthrough for our reply
    if ((glibId !== null && this.props.feedType !== 'glibReturnReGlib') || (this.reRender)) {
 

      if (reRender) {
        reRender = reRender.toString() + (new Date().getTime());
      }
      return (
        <div>
          <table width="100%">
            <tbody>
              <tr>
                <td width="42%"><hr className="style-five-purple" /></td>
                <td width="16%"><span className="glibr-font">replies</span></td>
                <td width="42%"><hr className="style-four-purple" /></td>
              </tr>
            </tbody>
          </table>

          <GlibFeed
            feedType='replyList'
            glibId={glibId}
            feedLocation='repliesList'
            numGlibs="50"
          />
        </div>
      );

    }
  }//end _renderReplies





}//end class
//-----------------------------------------------
//  Set Default props values
GlibSingle.defaultProps = {
  glibId: null,
  thisUserFeedHandle: 'glibrsmith',
  groupId: null,
};


//Map the properties of the page to store items
const mapStateToProps = (store) => {
  return {
    //glib properties
    glibs: store.glibsState,

    //user properties
    user: store.userState,

    //authenticate properties
    authenticate: store.authState
  };
};


//connect is the redux method that connects redux and react
export default connect(mapStateToProps, actionCreators)(GlibSingle);