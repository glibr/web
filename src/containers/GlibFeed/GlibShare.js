import React, { Component } from 'react';
import '../GlibFeed/style.css';

//components
import Glib from '../Glib/glib';
import RESTCLIENT from "../../common/restClient.js";
import Helper from "../../common/helpers.js";

let RestClient = new RESTCLIENT();

export default class GlibShare extends Component {
  constructor(props) {
    super(props);

    this.state = {    
      glibId: null,
      glibFeedRendered: null,
      feedType: 'shareView',
      glibType: 'glibShare',
    };

  }//end constructor


  //execute prior to render but after state
  async componentDidMount() {

    const qparam = await Helper.getUrlParameterByName('glibId')
 
      if  (qparam!==null){
        this.setState({
          glibId:qparam,
        })
      } else{
        window.location = '/noglib';
      }

    if  (this.state.glibId !==null){
      this._setGlibFeed();
      }

  }


  render() {

    // check that user state is set
    if (!this.state.glibFeedRendered)
    // This value gets set in the header to note when the other redux user state values are set.
    {
      return (<div>patience, the squirrels are working as fast as they can.</div>);

    } else {
      // User redux state set

        return (
          <div className="text-align-center">
            
            {this.state.glibFeedRendered}

          </div>
        );

    }
  }


  //--------------------------------------------------------
  // Set the glib feed 
  //
  // Meat of the module.  Derives the correct feedQuery to use then calls _getSingleGlib to get it.
  _setGlibFeed() {

    var  feedQuery = '/glibs/getsingleglib/' + this.state.glibId;

    this._getSingleGlib(feedQuery);

  }//end _setGlibFeed




  _getSingleGlib(feedQuery) {

    RestClient.fetchGet(feedQuery, response => {
      let gPicture = "",
        iter = 1000,
        singleGlib = [];

      if (response.success) {

        // Check to see if we have a picture as part of the glib.
        // Defensive coding, check for existence first.
        if (response.glib.glibPictures === undefined || response.glib.glibPictures === null || response.glib.glibPictures.length < 1) {
          gPicture = "";
        } else {
          gPicture = response.glib.glibPictures[0];
        }

        singleGlib =
          <Glib glibId={this.state.glibId}
            avatar={response.glib.avatar}
            author={response.glib.author}
            handle={response.glib.handle}
            cleanHandle={response.glib.cleanHandle}
            glibdate={response.glib.glibDate}
            glibcost={response.glib.cost}
            glibtext={response.glib.glibPost}
            glibPicture={gPicture}
            key={iter + '-' + response.glib._id}
            // Props passed in
            parentGlibID={response.glib.parentPost}
            quotedGlibID={response.glib.quotedPost}
            feedType={this.props.feedType}
            glibType={this.state.glibType}
            groupId={response.glib.groupId}
            glibPrivate= {response.glib.glibPrivate}
            glibPrivateUserId={response.glib.glibPrivateUserId}
          />


          this.setState({
            glibFeedRendered: singleGlib
          });
  


      } else {
        console.log("bump")
        this.setState({
          glibFeedRendered: response.message
        });


      }
    })
  }








}//end class

