import React, { Component } from 'react';
import { connect } from 'react-redux';

//actions
import {setMainGlibFeedType} from '../../actions/glibActions';

class GlibFeedControlBar  extends Component {


  
    render() {
        return (

                <div>
                         <a href="#" onClick={() => this.props.setMainGlibFeedType("all")}>all glibs</a>| |< a href="#" onClick={() => this.props.setMainGlibFeedType("mostExpensiveToday")}>top pennies today</a>
                </div>

        );
    };
}


//Map the properties of the page to store items
const mapStateToProps = (store) => {
        return {    
                //glib properties
                glibs: store.glibsState,

                //user properties
                user: store.userState


        };
};

const mapDispatchToProps = (dispatch) => {
        return {
                setMainGlibFeedType: (typeOfGilbFeed) => {        
                         dispatch(setMainGlibFeedType(typeOfGilbFeed));
                }        
        };
};


//connect is the redux method that connects redux and react
export default connect(mapStateToProps, mapDispatchToProps)(GlibFeedControlBar);