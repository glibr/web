import React, { Component, Fragment } from 'react';
import { connect } from 'react-redux';
import '../GlibFeed/style.css';

//components
import Glib from '../Glib/glib.js';
import Utilities from "../../common/utils.js";
import RESTCLIENT from "../../common/restClient.js";

let RestClient = new RESTCLIENT();

class GlibFeed extends Component {
  constructor(props) {
    super(props);

    this.gType = "happy";


    this._reloadGlibFeedBubble = this._reloadGlibFeedBubble.bind(this);
    this.addToFeed = this.addToFeed.bind(this);

    this.state = {
      glibFeedRendered: null,
      currentGlibFeed: "nothing",
      glibCount:0,
      numGlibsToGet:50,
      glibrMainFeed: [],
      glibrSecondaryFeed: [],
      paginated: [],
      reloadBubble: false,
      moreFeedContent: []
    };

  }//end constructor


  //execute prior to render but after state
  componentDidMount() {

    this._setGlibFeed(this.props.feedType);

  }


  componentDidUpdate(prevProps, prevState) {

    // only update chart if the data has changed
    if (prevProps.feedType !== this.props.feedType) {
    
      this._setGlibFeed(this.props.feedType);
    }
    // Check the reload time stamp
    else if (prevProps.feedReload !== this.props.feedReload) {
    
      this._setGlibFeed(this.props.feedType)

    } else if (prevState.reloadBubble !== this.state.reloadBubble) {
 
      this._setGlibFeed(this.props.feedType)

      this.setState({
        reloadBubble: false
      })

    }
  }


  addToFeed(e) {

    e.target.classList.add("hide");

    const newPageNumber = parseInt(this.props.pageNumber) + 1;

    console.log("page number: " + this.props.pageNumber);
   
    const newFeed = (

      <GlibFeed feedType={this.state.feedType} feedLocation="paginated" numGlibs={this.state.numGlibsToGet} pageNumber={newPageNumber} key={newPageNumber} />
    )

    this.setState(state => {
      const moreFeedContent = state.moreFeedContent.concat(newFeed);
      return {
        moreFeedContent,
      };
    });

    
  }



  render() {
    if (this.props.feedLocation === "mainFeed") {
      return (
        <div className="text-align-center ">
          <span className="glibr-font-highlight font-upsize glibfeed-friendly-name">{Utilities.common.getFriendlyFeedName(this.props.feedType, this.props.feedPage)}</span>
          <br />
          {this.state.glibrMainFeed}

          {this.renderMoreGlibsButton()}

        </div>
      );
    } else if (this.props.feedLocation === "paginated") {
      return (
        <div className="text-align-center">
          {this.state.paginated}

          {this.renderMoreGlibsButton()}

        </div>
      )
    }
    else if (this.props.feedLocation === "secondaryFeed") {
      return (
        <div className="text-align-center">
          {this.state.glibrSecondaryFeed}</div>
      )
    }
    else if (this.props.feedType === "glibReturnReGlib") {
      return (
        <div className="text-align-center">
          {this.state.glibFeedRendered}</div>
      );

    } else if (this.props.feedType === "replyList") {
      return (
        <div className="text-align-center">
          {this.state.glibFeedRendered}</div>
      );

    }

    return (<div>loading ..</div>);

  }


  renderMoreGlibsButton() {

    if(this.state.glibCount >= this.state.numGlibsToGet){
      return (
        <Fragment>
          <div className="text-align-center container-full-width" onClick={this.addToFeed}>
           <span className="glibr-font-highlight font-upsize">more glibs</span>
            <div className="triangle"></div>
          </div>
          <div> {this.state.moreFeedContent}</div>
          <br />
        </Fragment>
      );

    } else {
      return null;
    }

  }

  _reloadGlibFeedBubble() {

    this.setState({
      reloadBubble: true
    });
  }

  //--------------------------------------------------------
  // Set the glib feed 
  //
  // Meat of the 
  _setGlibFeed(feedType) {

    const queryParams = {
      feedType: feedType,
      feedLocation: this.props.feedLocation,
      glibId: this.props.glibId,
      numGlibs: this.props.numGlibs || 50,
      feedHandle: this.props.thisUserFeedHandle,
      groupId: this.props.groupId,
      pageNumber: this.props.pageNumber || 1,
    }

    var feedQuery = Utilities.common.getFeedQueryFromType(queryParams);

    this._getGlibFeed(feedQuery);

  }//end _setGlibFeed


  _getGlibFeed(feedQuery) {

    RestClient.fetchGet(feedQuery, response => {
      let glibFeed = [],
        gPicture = "",
        iter = 1000;

      if (!response.success) {
        // no feed returned
        glibFeed = null;

      } else {
        // got us some glibs

        this.setState({
          glibCount: response.glibCount,
        })

        response.glibs.forEach(glib => {
          if (glib !== undefined) {

            // Check to see if we have a picture as part of the glib.
            // Defensive coding, check for existence first.
            if (glib.glibPictures === undefined || glib.glibPictures === null || glib.glibPictures.length < 1) {
              gPicture = "";
            } else {
              gPicture = glib.glibPictures[0];
            }

            glibFeed.push(
              <Glib glibId={glib._id}
                avatar={glib.avatar}
                author={glib.author}
                handle={glib.handle}
                cleanHandle={glib.cleanHandle}
                glibdate={glib.glibDate}
                glibFullDate={glib.glibFullDate}
                glibcost={glib.cost}
                glibtext={glib.glibPost}
                glibPicture={gPicture}
                key={iter + '-' + glib._id}
                // Props passed in
                parentGlibID={glib.parentPost}
                quotedGlibID={glib.quotedPost}
                feedType={this.props.feedType}
                feedLocation={this.props.feedLocation}
                glibType={this.gType}
                glibLikes={glib.glibLikes}
                glibUnlikes={glib.glibUnlikes}
                glibReglibCount={glib.glibReglibCount}
                glibReplyCount={glib.glibReplyCount}
                reloadFeedBubble={this._reloadGlibFeedBubble}
                // groups data - if there is a groupId pass in groupsuser type too
                groupId={glib.groupId}
                groupUserType={this.props.groupUserType}
                // private message attributes
                glibPrivate={glib.glibPrivate}
                glibPrivateUserId={glib.glibPrivateUserId}
                glibPMToHandle={glib.glibPMToHandle}
                glibCharityLogo={glib.charityLogo}
              />
            );
          }
          iter++;
        });

      }

      if (this.props.feedType === "glibReturnReGlib") {
        this.setState({
          glibFeedRendered: glibFeed
        })
      } else if (this.props.feedType === "replyList") {
        this.setState({
          glibFeedRendered: glibFeed
        })
      } else if (this.props.feedLocation === "mainFeed") {
        this.setState({
          glibrMainFeed: glibFeed
        })
      } else if (this.props.feedLocation === "secondaryFeed") {
        this.setState({
          glibrSecondaryFeed: glibFeed
        })
        //this.props.setGlibsSecondaryFeed(glibFeed);
      } else if (this.props.feedLocation === "paginated") {
        this.setState({
          paginated: glibFeed
        })
      }

    }, err => {
      console.log(err);
    });


  }


}//end class

//-----------------------------------------------
//  Set Default props values
GlibFeed.defaultProps = {
  feedType: 'full',
  feedLocation: "mainFeed",
  numGlibs: 50,
  feedPage: 1,
  thisUserFeedHandle: 'glibrsmith',
  glibid: null,
  doFeedReload: false,
  groupId: null,
};


//Map the properties of the page to store items
const mapStateToProps = (store) => {
  return {
    //glib properties
    glibs: store.glibsState,

    //user properties
    user: store.userState,

    //authenticate properties
    authenticate: store.authState
  };
};


//connect is the redux method that connects redux and react
export default connect(mapStateToProps)(GlibFeed);