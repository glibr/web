import React from 'react';
import ReactDOM from 'react-dom';
import { browserHistory, Router } from 'react-router';
import { Provider } from 'react-redux';
import store from './store';
import GlibrRoutes from './routes';

import './global.css';

ReactDOM.render(


  <Provider store={ store }>
  <Router history={ browserHistory }>
    { GlibrRoutes(store) }
  </Router>
</Provider>,
  document.getElementById('root')
);
