//adding a middleware import to enable the redux logger
import {createStore, combineReducers} from "redux";
import glibsReducer from "./reducers/glibsReducer";
import userReducer from "./reducers/userReducer";
import authReducer from "./reducers/authReducer";

// Combine Reducers
const reducers = combineReducers({
  authState: authReducer,
  glibsState: glibsReducer,
  userState: userReducer
});

const store = createStore(reducers);

export default store;

