
import RESTCLIENT from "../common/restClient.js";
let RestClient = new RESTCLIENT();

export function setUserLoggedIn(userLoggedIn) {
        return {
                type: 'USER_DID_LOGIN',
                payload: userLoggedIn
        };
}

export function setCurrentUserRole(currentUserRole) {
        return {
                type: 'SET_USER_ROLE',
                payload: currentUserRole
        };
}

export function setCurrentUserStatus(currentUserStatus) {
        return {
                type: 'SET_USER_STATUS',
                payload: currentUserStatus
        };
}

export function setUserStateSetDone(userStateSet) {
        return {
                type: 'USER_SET_STATE_DONE',
                payload: userStateSet
        };
}

export function checkAndSetAuth() {

        return dispatch => {

                RestClient.fetchGet('/getcurrentuser', (response) => {
                        if (response.success) {
                                dispatch({
                                        type: 'USER_DID_LOGIN',
                                        payload: true
                                }) 
                       
                        }
                })

        }

      
}