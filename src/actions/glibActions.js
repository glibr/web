
export function  setSecondaryGlibFeedType(typeOfGilbFeed){
        return{
                type: "SET_SIDE_GLIB_FEED_TYPE",
                payload: typeOfGilbFeed
        };
}

export function  setMainGlibFeedType(typeOfGilbFeed){
        return{
                type: "SET_MAIN_GLIB_FEED_TYPE",
                payload: typeOfGilbFeed
        };
}


export function  setSingleGlib(singleGlib){
        return{
                type: "SET_SINGLE_GLIB",
                payload: singleGlib
        };
}



//timers

   //INTERVAL_RELOAD_TIMER_MAIN_GLIB_FEED
   // setIntervalReloadTimer_MainGlibFeed(intervalReloadTimer);
   export function  setIntervalReloadTimer_MainGlibFeed(intervalReloadTimer){
        return{
                type: "INTERVAL_RELOAD_TIMER_MAIN_GLIB_FEED",
                payload: intervalReloadTimer
        };

}

   //INTERVAL_RELOAD_TIMER_MAIN_GLIB_FEED
   // setIntervalReloadTimer_MainGlibFeed(intervalReloadTimer);
   export function  setIntervalReloadTimer_UserGlibFeed(intervalReloadTimer){
        return{
                type: "INTERVAL_RELOAD_TIMER_USER_GLIB_FEED",
                payload: intervalReloadTimer
        };
        

}

//INTERVAL_RELOAD_TIMER_SECONDARY_GLIB_FEED
export function  setIntervalReloadTimer_SecondaryGlibFeed(intervalReloadTimer){
        return{
                type: "INTERVAL_RELOAD_TIMER_SECONDARY_GLIB_FEED",
                payload: intervalReloadTimer
        };

}


export function  setGlibsMainFeed(glibs){
        return{
                type: "SET_GLIBS_MAIN_FEED",
                payload: glibs
        };
}

export function  setGlibsSecondaryFeed(glibs){
        return{
                type: "SET_GLIBS_SECONDARY_FEED",
                payload: glibs
        };
}

