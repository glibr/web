export function  setCurrentUserProfilePicture(profilePic){
        return{
                type: "SET_CURRENT_USER_PROFILE_PICTURE",
                payload: profilePic
        };
}


export function  setCurrentUserFirstName(currentUserFirstName){
        return{
                type:  'SET_CURRENT_USER_FIRST_NAME',
                payload: currentUserFirstName
        };      
}
export function  setCurrentUserLastName(currentUserLastName){
        return{
                type:  'SET_CURRENT_USER_LAST_NAME',
                payload: currentUserLastName
        };      
}


export function  setCurrentUserHandle(currentUserHandle){
        return{
                type: 'SET_CURRENT_USER_HANDLE',
                payload: currentUserHandle
        };      
}

//SET_CURRENT_USER_ROLE
export function  setCurrentUserRole(currentUserRole){
        return{
                type: 'SET_CURRENT_USER_ROLE',
                payload: currentUserRole
        };      
}

//SET_CURRENT_USER_ID
export function  setCurrentUserId(currentUserId){
        return{
                type: 'SET_CURRENT_USER_ID',
                payload: currentUserId
        };      
}

// Follows
export function  setCurrentUserFollowsList(followList){
        
        return{
                type: 'SET_CURRENT_USER_FOLLOWS_LIST',
                payload: followList
        };      
}
export function  setCurrentUserNumberOfFollows(numberFollows){
        return{
                type: 'SET_CURRENT_USER_NUMBER_OF_FOLLOWS',
                payload: numberFollows
        };      
}

// Followers
export function  setCurrentUserFollowersList(followersList){

        return{
                type: 'SET_CURRENT_USER_FOLLOWERS_LIST',
                payload: followersList
        };      
}
export function  setCurrentUserNumberOfFollowers(numberFollowers){
        return{
                type: 'SET_CURRENT_USER_NUMBER_OF_FOLLOWERS',
                payload: numberFollowers
        };      
}



// Double Follows
export function  setCurrentUserDoubleFollowsList(doubleFollowsList){

        return{
                type: 'SET_CURRENT_USER_DOUBLED_FOLLOWS_LIST',
                payload: doubleFollowsList
        };      
}
export function  setCurrentUserNumberOfDoubleFollows(numberDoubleFollows){
        return{
                type: 'SET_CURRENT_USER_NUMBER_OF_DOUBLED_FOLLOWS',
                payload: numberDoubleFollows
        };      
}



export function  setCurrentUserNumberGlibs(numberGlibs){
        return{
                type: 'SET_CURRENT_USER_NUMBER_OF_GLIBS',
                payload: numberGlibs
        };      
}

export function  setCurrentUserCharity(charity){
        return{
                type: 'SET_CHARITY_FOR_USER',
                payload: charity
        };      
}


//SET_CHARITY_LOGO_FOR_USER
export function  setCurrentUserCharityLogo(charityLogo){
        return{
                type: 'SET_CHARITY_LOGO_FOR_USER',
                payload: charityLogo
        };      
}
