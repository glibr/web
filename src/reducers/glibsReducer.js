//--------------------------------------------------------------------
// glibsReducer
// Our reducer for glibs

const glibsReducer = (state={
        //initial values for the store properties.
        topFiveExpensiveGlibs: [],
        topFiveExpensiveGlibsToday: [],
        mostExpensiveGlibsToday: [],
        getRepliesToThisGlib: [],
        currentTrendingGlibs: [],
        glibsSingleGlib: [],
       // glibsInUserFeed: [],
        mainGlibFeedType: "all",
        sideGlibFeedType: "all",
        glibsReceivedMainFeed: [],
        //feeds
        glibsMainFeed: [],
        glibsSecondaryFeed: [],
        //timers
        intervalTimer_MainGlibFeed: "0",
        intervalTimer_SecondaryGlibFeed: "0",
        intervalTimer_UserGlibFeed: "0"


      }, action) => {
      
                //using state object to be immutable
             //   let state = state;
        switch (action.type) {

        //new pattern
                case 'SET_MAIN_GLIB_FEED_TYPE':
                    state =  {
                            //spread the old state across the new object
                            //this duplicates the old value[s]
                            ...state,
                            //capture new value[s]
                            mainGlibFeedType: action.payload
                    };
                    break;

                case 'SET_SIDE_GLIB_FEED_TYPE':
                    state =  {                
                            ...state,             
                            sideGlibFeedType: action.payload
                    };
                    break;

                case 'SET_TOP_FIVE_EXPENSIVE_GLIBS':
                    state = {
                            ...state,
                            topFiveExpensiveGlibs: action.glibs 
                    };
                    break;

                case 'SET_TOP_FIVE_EXPENSIVE_GLIBS_TODAY':
                    state = {
                            ...state,
                            topFiveExpensiveGlibsToday: action.glibs 
                    };
                    break;

                case 'SET_CURRENT_TRENDING_GLIBS':
                    state = {
                            ...state,
                            currentTrendingGlibs: action.glibs 
                    };
                    break;

                case 'SET_GLIBS_MOST_EXPENSIVE_TODAY':
                    state = {
                            ...state,    
                            mostExpensiveGlibsToday: action.glibs 
                    };
                    break;

                case 'SET_GLIBS_REPLIES_TO_PARENT':
                    state = {
                            ...state,   
                            getRepliesToThisGlib: action.glibs 
                    };
                    break;



                    case "SET_GLIBS_MAIN_FEED":
                    state= {
                        ...state,
                        glibsMainFeed: action.payload
                    };
                    break;

                    case 'SET_GLIBS_SECONDARY_FEED':
                    state = {
                        ...state, 
                        glibsSecondaryFeed: action.payload 
                    };
                    break;

                    case 'SET_SINGLE_GLIB':
                    state = {
                        ...state, 
                        glibsSingleGlib: action.payload 
                    };
                    break;


                    //------------------------------------------------------------
                    //SET_GLIB_FEED
                    // 
                    // This will be for the main glib feed display on the homepage.
                    case 'SET_GLIB_FEED':
                        
                        state = {
                            ...state, 
                            //set glibs in feed
                            glibsMainFeed: action.payload
                        };

                    break;
                    //end SET_GLIB_FEED


                    case 'RECEIVE_GLIBFEED':
                        state = {
                            ...state,
                            glibsReceivedMainFeed:action.payload
                        }
                    break;

                    //used to store the interval timer id and hence be able to clearInterval as required
                    //INTERVAL_RELOAD_TIMER_MAIN_GLIB_FEED
                    // setIntervalReloadTimer_MainGlibFeed(intervalReloadTimer);
                    case "INTERVAL_RELOAD_TIMER_MAIN_GLIB_FEED":
                    state= {
                        ...state,
                        intervalTimer_MainGlibFeed: action.payload
                    };
                    break;

                      //INTERVAL_RELOAD_TIMER_USER_GLIB_FEED
                      case "INTERVAL_RELOAD_TIMER_USER_GLIB_FEED":
                      state= {
                          ...state,
                          intervalTimer_UserGlibFeed: action.payload
                      };
                      break;

                        //INTERVAL_RELOAD_TIMER_USER_GLIB_FEED
                        case "INTERVAL_RELOAD_TIMER_SECONDARY_GLIB_FEED":
                        state= {
                            ...state,
                            intervalTimer_SecondaryGlibFeed: action.payload
                        };
                        break;
  


                    default:
                    break;
        }

      
      
        return state;
};

export default glibsReducer;
