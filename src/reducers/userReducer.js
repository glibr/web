
const initialUserState = {
      user: {},
      currentUserId: [],
      currentUserHandle: [],
      currentUserFirstName: [],
      currentUserLastName:[],
      currentUserProfilePicture: '/images/logo_NO_profilePic.png',
      currentUserRole: [],

      //follows
      currentUserFollowList: [],
      currentUserNumberFollows: [],
      //followers
      currentUserFollowersList: [],
      currentUserNumberFollowers: [],
      //doubled
      currentUserDoubledFollowsList: [],
      currentUserNumberOfDoubled: [],
      //glibs
      currentUserNumberGlibs: [],
      //glibsInUserFeed: [],
      glibsAuthoredByUser: [],
      charityForUser: "",
      charityLogoForUser: '/images/logo_NO_profilePic.png',

};

// The User reducer
const userReducer = (state = initialUserState, action) => {

      //using newState object to be immutable
      let newState = state;

      switch (action.type) {


            case 'SET_CURRENT_USER_ID':
                  newState = {
                        ...state,
                        currentUserId: action.payload
                  };
                  break;

            case 'SET_CURRENT_USER_HANDLE':
                  newState = {
                        ...state,
                        currentUserHandle: action.payload
                  };
                  break;

            case 'SET_CURRENT_USER_FIRST_NAME':
                  newState = {
                        ...state,
                        currentUserFirstName: action.payload
                  };
                  break;

                  case 'SET_CURRENT_USER_LAST_NAME':
                        newState = {
                              ...state,
                              currentUserLastName: action.payload
                        };
                        break;

            case 'SET_CURRENT_USER_PROFILE_PICTURE':
                  newState = {
                        ...state,
                        currentUserProfilePicture: action.payload
                  };
                  break;

            case 'SET_CURRENT_USER_ROLE':
                  newState = {
                        ...state,
                        currentUserRole: action.payload
                  };
                  break;




            //follows
            case 'SET_CURRENT_USER_FOLLOWS_LIST':
                  newState = {
                        ...state,
                        currentUserFollowList: action.payload
                  };
                  break;

            case 'SET_CURRENT_USER_NUMBER_OF_FOLLOWS':
                  newState = {
                        ...state,
                        currentUserNumberFollows: action.payload
                  };
                  break;


            //followers
            case 'SET_CURRENT_USER_FOLLOWERS_LIST':
                  newState = {
                        ...state,
                        currentUserFollowersList: action.payload
                  };
                  break;

            case 'SET_CURRENT_USER_NUMBER_OF_FOLLOWERS':
                  newState = {
                        ...state,
                        currentUserNumberFollowers: action.payload
                  };
                  break;

            //doubled follows
            case 'SET_CURRENT_USER_DOUBLED_FOLLOWS_LIST':
                  newState = {
                        ...state,
                        currentUserDoubledFollowsList: action.payload
                  };
                  break;

            case 'SET_CURRENT_USER_NUMBER_OF_DOUBLED_FOLLOWS':
                  newState = {
                        ...state,
                        currentUserNumberOfDoubled: action.payload
                  };
                  break;



            //glibs
            case 'SET_CURRENT_USER_NUMBER_OF_GLIBS':
                  newState = {
                        ...state,
                        currentUserNumberGlibs: action.payload
                  };
                  break;


            case 'SET_GLIBS_AUTHORED_BY_USER':
                  newState = {
                        ...state,
                        glibsAuthoredByUser: action.glibs
                  };
                  break;


            // Charities

            case 'SET_CHARITY_FOR_USER':
                  newState = {
                        ...state,
                        charityForUser: action.payload
                  };
                  break;

            case 'SET_CHARITY_LOGO_FOR_USER':
                  newState = {
                        ...state,
                        charityLogoForUser: action.payload
                  };
                  break;
      

            default:
                  break;
      }

      return newState;
};

export default userReducer;