const authReducer = (state = {
      //initial values for the store properties.
      userSetStateDone: false,
      userLoggedIn: false,
      loginInProgress: false,
      registerInProgress: false,
      userIsRegistered: false,
      currentUserStatus: 'inactive',
      currentUserRole: 'user'

}, action) => {

      //using newState object to be immutable
      let newState = state;

      switch (action.type) {

            case 'USER_SET_STATE_DONE':
            newState = {
                  ...state,
                  userSetStateDone: action.payload
            };
            break;

            case 'USER_DO_LOGIN':
                  newState = Object.assign({}, state, { loginInProgress: true });
                  break;

            case 'USER_DID_LOGIN':
                  newState = {
                        ...state,
                        loginInProgress: false,
                        userLoggedIn: action.payload
                  };
                  break;

            case 'USER_DID_LOGOUT':
                  newState = {
                        ...state,
                        loginInProgress: false,
                        userLoggedIn: false
                  };
                  break;

            case 'USER_DO_REGISTER':
                  newState = {
                        ...state,
                        registerInProgress: true
                  };
                  break;

            case 'USER_DID_REGISTER':
                  newState = {
                        ...state,
                        registerInProgress: false,
                        userIsRegistered: true
                  };
                  break;

            case 'SET_USER_ROLE':
                  newState = {
                        ...state,
                        currentUserRole: action.payload
                  };
                  break;

            case 'SET_USER_STATUS':
                  newState = {
                        ...state,
                        currentUserStatus: action.payload
                  };
                  break;


            default:
                  break;
      }

      return newState;
};

export default authReducer;