import EnvironmentSettings from "./environmentsettings.js";
import GLOBALS from "./globals.js";

// Libraries and constants
let envSettings = new EnvironmentSettings();

export default class Helper {
	/** @summary
	 * Extract a URL query string parameter given the URL and parameter name.
	 * @parameters
	 * queryParamName: String value representing the name of a URL Parameters
	 * url: String value representing the URL string where the name parameter resides
	 *
	 * @return
	 * Returns the string value held within the URL's name query parameter
	 */
	static getUrlParameterByName = async (queryParamName, url) => {
		let results = null;

		try {
			if (!url) {
				url = window.location.href;
			}
			queryParamName = queryParamName.replace(/[[\]]/g, "\\$&");

			let regex = new RegExp("[?&]" + queryParamName + "(=([^&#]*)|&|#|$)");

			results = regex.exec(url);

			if (!results) return null;

			if (!results[2]) return "";

			return decodeURIComponent(results[2].replace(/\+/g, " "));
		} catch (err) {
			return "error: " + err;
		}
	};

	/**Upload Picture
	 *  centralized the upload picture routine for all uses.
	 */
	static uploadPicture = async e => {
		let formData = new FormData();
		formData.append("picToUpload", e.target.files[0]);
		formData.append("apikey", GLOBALS.APIKEY);

		try {
			const settings = {
				credentials: "include",
				method: "POST",
				body: formData
			};

			const response = await fetch(
				envSettings.GLIB_API_URI() + "/utils/uploadpicture",
				settings
			);

			if (response.status === 200) {
				const responseJSON = await response.json();

				if (responseJSON.success) {
					return {
						success: true,
						message: responseJSON.message,
						picture: responseJSON.imageUrl,
						pictureFullPath:
							envSettings.GLIBR_IMAGE_ROOT_URI() + responseJSON.imageUrl
					};
				} else {
					// failure branch
					return {
						success: false,
						message: responseJSON.message
					};
				}
			} else {
				return {
					success: false,
					message: "failed on response from server"
				};
			}
		} catch (err) {
			console.log(err);
			return {
				success: false,
				message: err
			};
		}
	};

	static saveFileNameToDB = async (collectionType, fileName, userHandle) => {
		if (collectionType === "profile") {
			const data = {
				collectionType: collectionType,
				fileName: fileName,
				userHandle: userHandle,
				apikey: GLOBALS.APIKEY
			};

			const settings = {
				credentials: "include",
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify(data)
			};

			try {
				const response = await fetch(
					envSettings.GLIB_API_URI() + "/utils/savefilenametodb",
					settings
				);

				if (response.status === 200) {
					const responseJSON = await response.json();

					if (responseJSON.success) {
						return {
							success: true,
							message: responseJSON.message
						};
					} else {
						// failure branch
						return {
							success: false,
							message: responseJSON.message
						};
					}
				} else {
					return {
						success: false,
						message: "failed on response from server"
					};
				}
			} catch (err) {
				console.log(`error in try-catch forupload picture: ${err}`);
				return {
					success: false,
					message: err
				};
			}
		}
	};


	static updateGroup = async (groupId, data) =>{
		try{

			// add apikey
			data.apikey = GLOBALS.APIKEY;

			const settings = {
				credentials: "include",
				method: "PUT",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify(data)
			};

			const response = await fetch(
				`${envSettings.GLIB_API_URI()}/groups/update/${groupId}`,
				settings
			);

			if (response.status === 200 || response.status === 400) {
				const responseJSON = await response.json();

				if (responseJSON.success) {
					return {
						success: true,
						groupId: groupId,
						message: responseJSON.message
					};
				} else {
					// failure branch
					return {
						success: false,
						message: responseJSON.message
					};
				}
			} else {
				return {
					success: false,
					message: "Unkown error from server."
				};
			}



		}catch(err){
			console.log("error updating group: " + err);
			return {
				success: false,
				message: err
			};
		}
	}

	static createGroup = async data => {
		try {
			// add apikey
			data.apikey = GLOBALS.APIKEY;

			const settings = {
				credentials: "include",
				method: "POST",
				headers: {
					"Content-Type": "application/json"
				},
				body: JSON.stringify(data)
			};

			const response = await fetch(
				envSettings.GLIB_API_URI() + "/groups/creategroup",
				settings
			);

			if (response.status === 200 || response.status === 400) {
				const responseJSON = await response.json();

				if (responseJSON.success) {
					return {
						success: true,
						groupId: responseJSON.groupId,
						message: responseJSON.message
					};
				} else {
					// failure branch
					return {
						success: false,
						message: responseJSON.message
					};
				}
			} else {
				return {
					success: false,
					message: "Unkown error from server."
				};
			}
		} catch (err) {
			console.log("error creating group: " + err);
			return {
				success: false,
				message: err
			};
		}
	};

	static getGroupDetails = async (groupId) => {
            let returnData = {};

		try {		

			const settings = {
				credentials: "include",
				method: "GET"
			};
			const apikey = `?apikey=${GLOBALS.APIKEY}`;

			const response = await fetch(
				`${envSettings.GLIB_API_URI()}/groups/getgroupbyid/${groupId}${apikey}`,
				settings
			);

			if (response.status === 200) {
				const responseJSON = await response.json();

				if (responseJSON.success) {
					returnData.groupId = groupId;
					returnData.groupFound = true;
					returnData.groupData = responseJSON.group;
					returnData.message = responseJSON.message;
				} else {
					// failure branch
					returnData.groupId = groupId;
					returnData.groupFound = false;
					returnData.message = responseJSON.message;
				}
			} else {
				returnData.groupId = groupId;
				returnData.groupFound = false;
				returnData.message = "failed on response from server";
			}
		} catch (err) {
			returnData.groupId = groupId;
			returnData.groupFound = false;
			returnData.message = err;
		}

		return returnData;
	}


	static htmlEncode(html) {
		return document
			.createElement("a")
			.appendChild(document.createTextNode(html)).parentNode.innerHTML;
	}

	static htmlDecode(html) {
		var a = document.createElement("a");
		a.innerHTML = html;
		return a.textContent;
	}

	static stripTagsFromText(textToClean) {
		const tagBody = "(?:[^\"'>]|\"[^\"]*\"|'[^']*')*";

		const tagOrComment = new RegExp(
			"<(?:" +
				// Comment body.
				"!--(?:(?:-*[^->])*--+|-?)" +
				// Special "raw text" elements whose content should be elided.
				"|script\\b" +
				tagBody +
				">[\\s\\S]*?</script\\s*" +
				"|style\\b" +
				tagBody +
				">[\\s\\S]*?</style\\s*" +
				// Regular name
				"|/?[a-z]" +
				tagBody +
				")>",
			"gi"
		);

		// remove HTML tags from, post to prevent XSS
		let oldText;
		do {
			oldText = textToClean;
			textToClean = textToClean.replace(tagOrComment, "");
		} while (textToClean !== oldText);

		textToClean.replace(/</g, "&lt;");

		return textToClean;
	}

	/** Check for Bad words
	 * calling the server so that our bad word configuration will be in one spot
	 */
	static checkForBadWords = async checkThisString => {
		let responseCheckObj = {
			success: false,
			foundBadText: false,
			foundHTML: false,
			message: "not processed"
		};
		try {
			const cleanedThisString = this.stripTagsFromText(checkThisString);

			if (cleanedThisString.length !== checkThisString.length) {
				responseCheckObj.success = true;
				responseCheckObj.foundHTML = true;
				responseCheckObj.message = "cleaned out tags";
				responseCheckObj.cleanedString = cleanedThisString;
			}

			const settings = {
				credentials: "include",
				method: "GET"
			};

			const apikey = `?apikey=${GLOBALS.APIKEY}`;

			const response = await fetch(
				`${envSettings.GLIB_API_URI()}/utils/checkforbadwords/${cleanedThisString}${apikey}`,
				settings
			);

			if (response.status === 200) {
				const responseJSON = await response.json();

				if (responseJSON.success) {
					// good response, pass in the flag for found bad words and cleaned string
					responseCheckObj.success = true;
					responseCheckObj.cleanedString = responseJSON.cleanedString;
					responseCheckObj.foundBadText = responseJSON.foundBadText;
					responseCheckObj.message = "all good";
				} else if (!responseCheckObj.foundHTML) {
					// failure branch - but exluding if we found HTML already
					console.log("failed for bad words check.");
					responseCheckObj.success = false;
					responseCheckObj.message = responseJSON.message;
				}
			} else {
				responseCheckObj.success = false;
				responseCheckObj.message = "failed on response from server";
			}
		} catch (err) {
			console.log(`error in try-catch for clean bad words: ${err}`);
			responseCheckObj.success = false;
			responseCheckObj.message = err;
		}
		
		return responseCheckObj;
	};

	// HelperMethods.checkFoGroupDupe(checkThisGroupName);
	static checkFoGroupDupe = async checkThisGroupForDupe => {
		try {
			const settings = {
				credentials: "include",
				method: "GET"
			};

			const apikey = `?apikey=${GLOBALS.APIKEY}`;

			const response = await fetch(
				`${envSettings.GLIB_API_URI()}/groups/checkforgroupdupe/${checkThisGroupForDupe}/${apikey}`,
				settings
			);

			if (response.status === 200) {
				const responseJSON = await response.json();

				if (responseJSON.success) {
					return {
						success: true
					};
				} else {
					return {
						success: false
					};
				}
			} else {
				return {
					success: false,
					message: "failed on response from server"
				};
			}
		} catch (err) {
			console.log(`error in try-catch for check dupe: ${err}`);
			return {
				success: false,
				message: err
			};
		}
	};

	static validateThis = async (thisType, thisValidation, whatToCheck) => {
		if (thisType === "text") {
			switch (thisValidation) {
				case "exists":
					// if not null, not empty, not undefined
					console.log(
						"in existence check - |" + whatToCheck + "| " + (whatToCheck !== "")
					);
					let doesExist = false;
					if (
						typeof whatToCheck !== "undefined" &&
						whatToCheck !== undefined &&
						whatToCheck !== "" &&
						whatToCheck !== null
					) {
						doesExist = true;
					}
					console.log("after exist check: " + doesExist);
					return doesExist;
				case "text-length-55":
					return whatToCheck.length <= 55;
				case "text-length-150":
					return whatToCheck.length <= 150;
				case "text-length-255":
					return whatToCheck.length <= 255;
				case "text-length-1000":
					return whatToCheck.length <= 1000;
				case "alphanumeric-only":
					console.log(whatToCheck.length);
					if (whatToCheck.length > 0) {
						return /^[ \w-]+$/.test(whatToCheck);
					} else {
						return true;
					}
				case "clean-words":
					const resClean = await this.checkForDirtyWords(whatToCheck);
					if (resClean.success) {
						console.log(
							`${whatToCheck} checked: ${resClean.foundBadText} message: ${resClean.message}`
						);						
						// returning true for finding bad words
						// the validation check will look for true for a "hit"
						return resClean.foundBadText;
					} else {
						console.log("failed to clean the string for bad words.");
						return false;
					}
				default:
					return true;
			}
		}
	};

	static combineURLs(baseURL, relativeURL) {
		return relativeURL
			? baseURL.replace(/\/+$/, "") + "/" + relativeURL.replace(/^\/+/, "")
			: baseURL;
	}

	static parseFirstHashtag(str) {
		const regex = /#(\w*[0-9a-zA-Z]+\w*[0-9a-zA-Z])/g;

		let m;

		while ((m = regex.exec(str)) !== null) {
			// This is necessary to avoid infinite loops with zero-width matches
			if (m.index === regex.lastIndex) {
				regex.lastIndex++;
			}

			return {
				success: true,
				name: m[1],
				hashName: m[0]
			};
		}
		return { success: false };
	}

	//take a single JavaScript File object
	static getBase64File(file) {
		var reader = new FileReader();
		return new Promise((resolve, reject) => {
			reader.onerror = () => {
				reader.abort();
				reject(new Error("Error parsing file"));
			};

			reader.onload = function() {
				//This will result in an array that will be recognized by C#.NET WebApi as a byte[]
				let bytes = Array.from(new Uint8Array(this.result));

				//if you want the base64encoded file you would use the below line:
				let base64StringFile = btoa(
					bytes.map(item => String.fromCharCode(item)).join("")
				);

				//Resolve the promise with your custom file structure
				resolve({
					bytes: bytes,
					base64StringFile: base64StringFile,
					fileName: file.name,
					fileType: file.type
				});
			};
			reader.readAsArrayBuffer(file);
		});
	}
} //end class
