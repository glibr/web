module.exports = {
  SITE_NAME: "glibr",
  APIKEY: "38761e76-4628-4ffc-b1f8-79bf47d4ceca",
  ENV_VARS: {
    // Prod
    GLIB_API_URI: "https://api.glibr.com",
    // GLIBR_IMAGE_ROOT_URI: 'https://api.glibr.com/public/uploads/',
    // Dev
    // GLIB_API_URI: 'http://api.glibr:5000',
    //GLIBR_IMAGE_ROOT_URI: 'http://api.glibr:5000/public/uploads/',
    GLIBR_IMAGE_ROOT_URI:
      "https://glibr-public.s3-us-west-2.amazonaws.com/uploads/"
  },
  CONSTANTS: {
    GLIB_POST_MAX_LENGTH: 150,
    GLIB_MAX_PENNIES: 250,
    GLIB_MIN_PENNIES: 2,
    GLIB_BILLING_THRESHOLD: 300,
    GLIB_PROVISIONAL_PENNY_LIMIT: 60,
    SEARCH_STRING_LENGTH: 255,
    NAME_FIELD_LENGTH: 75
  },
  STRINGS: {
    GLIBR_TAGLINE: "The goodest social network on the whole internet!",
    PROFILE_SALUTATION_ANON: "Say hi to, ",
    PROFILE_SALUTATION_USER: "Welcome, ",
    IMAGE_UPLOAD_ERROR: "We had a problem uploading that image.",
    GROUPS_EMPTY_FORM_FIELD: "Hey man, you gotta fill in that field.",
    GROUPS_DUPE_NAME:
      "Hey, that group name is already taken, try something else.",
    TEXT_INPUT_ALPHA_NUMERIC_CHECK:
      "Hey, you can only use letters and numbers and characters _ -",
    TEXT_INPUT_LENGTH_55:
      "The maximum length for a Group Name is 55 characters.",
    TEXT_INPUT_LENGTH_150:
      "The maximum length for a Group Name is 150 characters.",
    TEXT_INPUT_LENGTH_255:
      "The maximum length for a Group Name is 255 characters.",
    TEXT_INPUT_LENGTH_1000:
      "The maximum length for a Group Name is 1000 characters.",
    TEXT_INPUT_CONTAINS_BAD_WORDS: "You can't use that dirty word, try again.",
    TEXT_INPUT_CONTAINS_HTML: "Dude you can't use HTML."
  },
  COLOR: {
    // cool pallette maybe implement this: https://coolors.co/1f2041-4b3f72-399552-bcd8c1-f1ffd8
    GLIBR_GREEN: "#399552",
    DARKGREEN: "#0A3200",
    LIGHTGREEN: "#379634",
    LIMEGREEN: "#B3FF63",
    SOFTGREEN: "#F1FFD8"
  }
};
