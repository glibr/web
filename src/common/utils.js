
const store = require('../store.js').default

module.exports = {
    
  // form validation methods
  validation: {

    validHandle: handle =>{
      return/^[A-Za-z0-9][A-Za-z0-9]*(?:_[A-Za-z0-9]+)*(?:\.[A-Za-z0-9]+)*(?:-[A-Za-z0-9]+)*$/.test(handle);  
    },
    validHandleLength: handle =>{
      return(3<handle.length && handle.length<=24);
    },
    validEmail: email => {
      return /[a-zA-Z0-9+._%-+]{1,256}@[a-zA-Z0-9][a-zA-Z0-9-]{0,64}(.[a-zA-Z0-9][a-zA-Z0-9-]{0,25})/.test(email);
    },
    validPhone: phone => {
      return /(\+)?(\d{1,3})?(\s)?(\()?(\d{0,3})?(\)|-)?(\s)?\d{3,3}(-)?(\s)?\d{4,4}/.test(phone);
    },
    //tagline can be only 55 characters long
    isTextLessThan55Characters: tagline => {
      return(tagline.length<=55);
    },
    //a glib has to be 150 characters or less
    isTextLessThan150Characters: glib => {
          return(glib.length<=150);
    },
    isFirstName: firstName =>{
      return(firstName.length<=50);
    },
    isLastName: lastName =>{
      return(lastName.length<=50);
    },

    weakPassword: pw => {
      // private methods
      let isWeakPassword = password => {
        if (password.length < 8) {
            return true;
        }
        let charTypes = 0;
        if (hasAtleastOneLowercaseLetter(password)) { charTypes += 1; } // at least one lowercase
        if (hasAtleastOneUppercaseLetter(password)) { charTypes += 1; } // at least one uppercase
        if (hasAtleastOneNumeric(password)) { charTypes += 1; } // at least one numeric
        if (hasAtleastOneNonAlphaNumeric(password)) { charTypes += 1; } // at least one non-alphanumeric
        return charTypes < 4;
      }
      let hasAtleastOneLowercaseLetter = string => {
        return /.*[a-z].*/.test(string);
      }
      let hasAtleastOneUppercaseLetter = string => {
        return /.*[A-Z].*/.test(string);
      }
      let hasAtleastOneNumeric = string => {
        return /.*[0-9].*/.test(string);
      }
      let hasAtleastOneNonAlphaNumeric = string => {
        return !/^[a-zA-Z0-9]*$/.test(string);
      }
      // let isValidPhoneNumber = phoneNumber => {
      //   return /^(\([0-9]{3}\)|[0-9]{3}-?)[0-9]{3}-?[0-9]{4}$/.test(phoneNumber);
      // }


      return isWeakPassword(pw);
    },
    messages: {
      INVALID_HANDLE: 'Yo, bad handle dude.  Your handle can only contain letters, numbers, and characters  .  -  _ ',
      INVALID_HANDLE_LENGTH:'Heya, your handle has to be between 4 and 24 characters long.',
      INVALID_EMAIL: 'Oops, looks like the email you entered may have a typo or something. Please check that guy for errors and try again.',
      INVALID_PHONE: 'Oops, we just tried calling you, but we couldn\'t get through. Totally kidding, but seriously, please enter a valid phone number. :)',
      IS_WEAK_PASSWORD: 'Whoops, lets make a stronger password. Try including at least one uppercase letter, one special character, and one number.  Also more than 8 characters long.',
      MISSING_FIELDS: 'Oops looks like you forgot to enter some stuff, please fill in all the boxes above.',
      TAGLINE_LONG55: 'Your Tagline can only hold 55 characters.  Remeber, brevity is the soul of wit.',
      FIRSTNAME_LEN50: 'Your First Name is limited to 50 characters.',
      LASTNAME_LEN50: 'Your Last Name is limited to 50 characters.'
    }
  },
  // useful common methods
  common: {
   
    /** @summary
     * Extract a URL query string parameter given the URL and parameter name.
     * @parameters
     * name: String value representing the name of a URL Parameters
     * url: String value representing the URL string where the name parameter resides
     *
     * @return
     * Returns the string value held within the URL's name query parameter
     */
    getUrlParameterByName: (name, url) => {
      if (!url) {
          url = window.location.href;
      }
      name = name.replace(/[[\]]/g, "\\$&");
      let regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
      if (!results) return null;
      if (!results[2]) return '';
      return decodeURIComponent(results[2].replace(/\+/g, " "));
    },

    
    //--------------------------------------------------------------------------
    // getFeedQueryFromType
    //
    // Returns the correct feed api query string for the
    // passed in feedType     
    // feedLocation will used for glibId for single glib lookup
    getFeedQueryFromType(queryParams){
                    
        var feedQuery = "/glibs/50/0/full";
        var hList = '';
        var storeState = store.getState();
        
        // --> queryParams --
        // feedType: feedType,
        // feedLocation: this.props.feedLocation,
        // glibId: this.props.glibId,
        // numGlibs: this.props.numGlibs||50,
        // feedHandle: this.props.thisUserFeedHandle,
        // groupId: this.props.groupId,
        // pageNumber: this.props.pageNumber ||0,

        switch(queryParams.feedType){

        case "full":
            //pagination
            feedQuery = `/glibs/glibs/${queryParams.numGlibs}/${queryParams.pageNumber}/${queryParams.feedType}`; // :per_page/:page_no/:sort' "/glibs/getFullRawFeed/" + numGlibs;
        break;

        case "pagination":
            //pagination
            feedQuery = `/glibs/glibs/${queryParams.numGlibs}/${queryParams.pageNumber}/${queryParams.feedType}`; // :per_page/:page_no/:sort' "/glibs/getFullRawFeed/" + numGlibs;
        break;

        case "userFeed":                     
            feedQuery =`/glibs/getuserglibs/${storeState.userState.currentUserHandle}/${queryParams.numGlibs}/${queryParams.pageNumber}`;
        break;

        case "profileUserFeed":        
        feedQuery =`/glibs/getuserglibs/${queryParams.feedHandle}/${queryParams.numGlibs}/${queryParams.pageNumber}`;
        break;

        case "privateMessagesWithUser":
        feedQuery =  `/glibs/getprivatemessages/${storeState.userState.currentUserHandle}/${queryParams.feedHandle}/${queryParams.numGlibs}`;
        break;

        case "groupFeed":
        feedQuery = `/glibs/getgroupglibs/${queryParams.groupId}/${queryParams.numGlibs}`;
          break;

        case "mostExpensiveAllTime":
            feedQuery ='/glibs/getmostexpensiveglibs/' +queryParams.numGlibs;
        break;

        case "mostExpensiveToday":
            feedQuery ='/glibs/getmostexpensiveglibstoday/' + queryParams.numGlibs;
        break;

        case "glibsOfFollows":
            hList = JSON.stringify( storeState.userState.currentUserFollowList);
            feedQuery =`/glibs/getglibsbyhandlelist/${hList}/${queryParams.numGlibs}/${queryParams.pageNumber}` ;
        break;

        case "glibsOfFollowers":
            hList = JSON.stringify( storeState.userState.currentUserFollowersList);
            feedQuery =`/glibs/getglibsbyhandlelist/${hList}/${queryParams.numGlibs}/${queryParams.pageNumber}` ;
        break;


        case "glibsOfDoubled":
            hList = JSON.stringify( storeState.userState.currentUserDoubledFollowsList);
            feedQuery =`/glibs/getglibsbyhandlelist/${hList}/${queryParams.numGlibs}/${queryParams.pageNumber}` ;
        break;

        case "glibReturnReGlib":        
            // feedLocation will used for glibId
            feedQuery ='/glibs/getsingleglib/'+queryParams.glibId;
        break;

        //replyList
        case "replyList":        
            // feedLocation will used for glibId
            feedQuery =`/glibs/getrepliestothisglib/${queryParams.glibId}/${queryParams.numGlibs}/${queryParams.pageNumber}`;
        break;

        default:
        break;
        }

        return feedQuery;

    },


    //--------------------------------------------------------------------------
    // getFriendlyFeedName
    //
    //we want to see a freindly name for the feedType.
    getFriendlyFeedName(feedType, feedPage){
    
      var friendlyFeedName = "glib feed"
      var showPage = "";
      if(feedPage>1){
        showPage = " - page: " + feedPage;
      }

      switch(feedType){
      
        case "full":
          friendlyFeedName = "glib feed" +showPage;
        break;

        case "userFeed":                     
          friendlyFeedName = "user  feed" +showPage;
        break;

        case "mostExpensiveAllTime":
            friendlyFeedName = "glibs with the most pennies" +showPage;
        break;

        case "mostExpensiveToday":
            friendlyFeedName = "glibs with the most pennies today" +showPage;
        break;

        case "glibsOfFollows":
            friendlyFeedName = "glibs of people you are following" +showPage;
        break;

        case "glibsOfFollowers":
            friendlyFeedName = "glibs from people following you" +showPage;
        break;


        case "glibsOfDoubled":
            friendlyFeedName = "glibs of people both you are following and who are following you - doubled" +showPage;
        break;

        default:
        break;
      }

      return friendlyFeedName;
    },


    hideLoadingSpinner: hide => {
      try{
        if (hide) {
          document.getElementById('spinnerLoadingImage').classList.remove('show');
          document.getElementById('spinnerLoadingImage').classList.add('hide');
        } else {
          document.getElementById('spinnerLoadingImage').classList.remove('hide');
          document.getElementById('spinnerLoadingImage').classList.add('show');
        }

      } catch(err){
        console.log("error with spinner: " + err)
      }
    
    },


    toggleElement: oEl => {
      if (oEl.classList.contains("show")) {
        oEl.classList.remove('show');
        oEl.classList.add('hide');
      } else if (oEl.classList.contains("hide")) {
        oEl.classList.remove('hide');
        oEl.classList.add('show');
      }
    },

    

  }
};
