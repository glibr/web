

var GLOBALS = require('./globals.js');
//console.log("Environment check: " + process.env.NODE_ENV + " " + JSON.stringify(process.env));
/**
 *  Environment Settings
 * 
 *  Change the GLOBALS value  ENVIRONMENT
 *  This will propogate through the system.
 *  currently passing that value in... this may be refactored
 */
export default class EnvironmentSettings {

      /**
       *  Set Mongo db connection string depending on the environment
       * @param { } env 
       */
      GLIB_API_URI() {

           return GLOBALS.ENV_VARS.GLIB_API_URI;

            // switch (process.env.NODE_ENV) {
            //       case 'development':
            //             return GLOBALS.ENV_VARS.GLIB_DEV_API_URI;
            //       case 'production':
            //             return GLOBALS.ENV_VARS.GLIB_API_URI;
            //       default:
            //             return GLOBALS.ENV_VARS.GLIB_DEV_API_URI;
            // }
      }


      GLIBR_IMAGE_ROOT_URI() {
           // console.log("env: " + process.env.NODE_ENV);
            return GLOBALS.ENV_VARS.GLIBR_IMAGE_ROOT_URI;
            // switch (process.env.NODE_ENV) {
            //       case 'development':
            //             return GLOBALS.ENV_VARS.GLIBR_DEV_IMAGE_ROOT_URI;
            //       case 'production':
            //             return GLOBALS.ENV_VARS.GLIBR_IMAGE_ROOT_URI;
            //       default:
            //             return GLOBALS.ENV_VARS.GLIBR_DEV_IMAGE_ROOT_URI;
            // }
      }



} //end class
