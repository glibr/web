// require this module at the top of a component like so:
// var Utilities = require('../../../common/utils.js');
// then to use it: Utilities.common.fetch(url, data, etc...);

//components
import EnvironmentSettings from "./environmentsettings.js";
import GLOBALS from './globals.js';

let envSettings = new EnvironmentSettings();

export default class RestClient {


  /** @summary
* Sends a POST request to the Glibr API layer
*
* @parameters
* url: String value representing the URL of the API's end point.
* data: JSON object containing fetch parameters.
* successCallback: Javascript function which will be called, and passed the response object, upon successful fetch.
* errorCallback: Javascript function which will be called upon a failed fetch.
*/
  fetchPost(url, data, successCallback, errorCallback) {
    if (!data) {
      console.error('Error: No data object was passed into the fetchPost() method.')
      return false;
    }

    // add apikey
    data.apikey = GLOBALS.APIKEY;

    fetch(envSettings.GLIB_API_URI() + url, {
      credentials: 'include',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify(data)
    }).then(response => {
      return response.json();
    }).then(json => {
      successCallback(json);
    }).catch(err => {
      errorCallback(err);
    });
  }




  /** @summary
* Sends a POST request to the Glibr API layer
*
* @parameters
* url: String value representing the URL of the API's end point.
* data: JSON object containing fetch parameters.
* successCallback: Javascript function which will be called, and passed the response object, upon successful fetch.
* errorCallback: Javascript function which will be called upon a failed fetch.
*/
fetchPut(url, data, successCallback, errorCallback) {
  if (!data) {
    console.error('Error: No data object was passed into the fetchPut() method.')
    return false;
  }

  // add apikey
  data.apikey = GLOBALS.APIKEY;

  fetch(envSettings.GLIB_API_URI() + url, {
    credentials: 'include',
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json'
    },
    body: JSON.stringify(data)
  }).then(response => {
    return response.json();
  }).then(json => {
    successCallback(json);
  }).catch(err => {
    errorCallback(err);
  });
}




  /** @summary
  * Sends a GET request to the Glibr API layer
  *
  * @parameters
  * url: String value representing the URL of the API's end point.
  * successCallback: Javascript function which will be called, and passed the response object, upon successful fetch.
  * errorCallback: Javascript function which will be called upon a failed fetch.
  */
  async  fetchGet(url, successCallback, errorCallback) {
    // add apikey
    url += `?apikey=${GLOBALS.APIKEY}`;

    try {
    
      await fetch(envSettings.GLIB_API_URI() + url, {
        credentials: 'include',
        method: 'GET',
        headers: {
          'Content-Type': 'application/json'
        }
      }).then(response => {
        return response.json();
      }).then(json => {
        successCallback(json);
      }).catch(err => {
        
        errorCallback(err);
      });
    } catch (err) {
      console.log("rest client failed: " + envSettings.GLIB_API_URI() +  url + "  - error: " + err);
    //  errorCallback(err);
    }
  }



}
