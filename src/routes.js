import React from 'react';
import { Route, IndexRoute } from 'react-router';

import App from './components/App';
import Home from './components/Home';
import Login from './containers/Auth/Login.js';
import Admin from './containers/Admin/admin.js';
import UserProblem from './containers/UserProblem/userproblem.js';
import Registration from './containers/Auth/Registration.js';
import ForgotPassword from './containers/Auth/ForgotPassword.js';
import ResetPassword from './containers/Auth/ResetPassword.js'; //second half of the ForgotPassword flow
import ChangePassword from './containers/Auth/ChangePassword.js'; //user changes their own password
import Profile from './containers/Account/Profile.js';
import Group from './containers/Groups/group.js';
import GroupAdmin from './containers/Groups/groupadmin.js';
import UserHomeContainer from './containers/Account/UserHomeContainer.js';
import NotFound from './components/NotFound';
import SingleGlib from './containers/GlibFeed/GlibSingle.js';
import GlibShare from './containers/GlibFeed/GlibShare.js';
import Search from './containers/Search/search.js';
// import Payments from './containers/Payments/payments.js';
import TermsOfService from './components/Pages/TermsOfService.js';
import PrivacyPolicy from './components/Pages/PrivacyPolicy.js';
import About from './components/Pages/About.js';
import FAQ from './components/Pages/FAQ.js';
//import ZomboCom from './components/Pages/zombocom.js';

import Charities from './containers/Charities/charities.js';
// import StripeUserSignUp from './containers/Payments/stripeUserSignUp.js';
import TestContainer from './containers/TestContainer';

const GlibrRoutes = (store) => {

  // function requireAuth(nextState, replace) {
  //   if (!localStorage.getItem('isUserLoggedIn') || localStorage.getItem('isUserLoggedIn')=== 'false') {
  //     // Not authenticated, redirect to homepage.
  //     replace({
  //       pathname: '/login'
  //     })
  //   }
  // }

  function requireActiveUser(nextState, replace) {
    // check for logged in first.
    if (!localStorage.getItem('isUserLoggedIn') || localStorage.getItem('isUserLoggedIn')=== 'false') {
      // Not authenticated, redirect to homepage.
      replace({
        pathname: '/login'
      })
      return;
    } else  if (!localStorage.getItem('isUserActive') || localStorage.getItem('isUserActive')=== 'false') {
      // Not authenticated, redirect to homepage.
      replace({
        pathname: '/userproblem'
      })
    }
  }
  

  function requireAdmin(nextState, replace) {

    // check for logged in first.
    if (!localStorage.getItem('isUserLoggedIn') || localStorage.getItem('isUserLoggedIn')=== 'false') {
      // Not authenticated, redirect to homepage.
      replace({
        pathname: '/login'
      })
      return;
    } else  if (!localStorage.getItem('isUserActive') || localStorage.getItem('isUserActive')=== 'false') {
      // Not authenticated, redirect to homepage.
      replace({
        pathname: '/userproblem'
      })
      return;
    }else  if (!localStorage.getItem('isUserAdmin') || localStorage.getItem('isUserAdmin')=== 'false') {
      // Not authenticated, redirect to homepage.
      replace({
        pathname: '/'
      })
    }
  }

  
return (

  <Route path="/" component={App} >

    {/* Now we get our ACTUAL PARENT page - based on slash URL */}
    {/* Home is a simple wrapper around Main */}
    <IndexRoute component={Home}  />



    {/* private routes */}
    <Route path="/profile" component={Profile} onEnter={requireActiveUser} />
    {/* <Route path="/payments" component={Payments}  onEnter={requireAdmin} /> */}
    <Route path="/admin" component={Admin} onEnter={requireAdmin} />
    <Route path="/group" component={Group} onEnter={requireActiveUser}  />    
    <Route path="/groupadmin" component={GroupAdmin} onEnter={requireActiveUser}  />
    <Route path="/search" component={Search} onEnter={requireActiveUser} />
    


    {/* public routes */}
    <Route path="/login" component={Login} />
    <Route path="/userproblem" component={UserProblem} />
    <Route path="/register" component={Registration} />
    <Route path="/forgot-password" component={ForgotPassword} />
    <Route path="/reset-password" component={ResetPassword} />
    <Route path="/change-password" component={ChangePassword} />
    <Route path="/tos" component={TermsOfService} />
    <Route path="/privacy" component={PrivacyPolicy} />
    <Route path="/about" component={About} />
    <Route path="/faq" component={FAQ} />
    <Route path='/shareglib' component={GlibShare} />    
    <Route path="/glib" component={SingleGlib}  />
    {/* <Route path='/zombocom' component={ZomboCom} /> */}
    <Route path="/404" component={NotFound} />


    {/* dead routes */}
    <Route path="/charities" component={Charities} />
    {/* <Route path="/stripeusersignup" component={StripeUserSignUp} /> */}
    <Route path="/tc" component={TestContainer} />

    <Route path="/*" component={UserHomeContainer}  onEnter={requireActiveUser}  />
    <Route path="*" component={NotFound} />

  </Route>
);

}// end class


export default GlibrRoutes;











